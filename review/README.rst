..
 =============================================================================
 Title          : Utilities for setup tools

 Classification : reST text file

 Author         : Dirk Ullrich

 Date           : 2022-04-04

 Description    : Read me file for the package.
 =============================================================================


Here is an overview of its functionality, grouped by the submodule of the
``dusetup`` package providing it.


High-level standard building blocks
===================================

Let us start with the those modules that provide high-level building blocks
for setup tools.  This building blocks are more or less needed for all setup
tools based on ``dusetup``:

+ :py:mod:`dusetup.cmdline`: building blocks for the commandline and of setup
  tools, and for interaction of configuration and commandline parameters.

+ :py:mod:`dusetup.config`: utilities to work with configuration files of set
  up tools, including functions useful for value interpolation within such
  configuration files. [#fct_intp]_

+ :py:mod:`dusetup.step`: framework to implement the steps needed for setup.
  This provides a rather generic mechanism to set up such setup steps
  controlled by the configuration file of the respective setup tool.

+ :py:mod:`dusetup.error`: framework for error handling by providing a
  dedicated exception, and a context handler to deal with errors once for all.


Building blocks for setup steps
===============================

Now we turn to those ``dusetup`` modules proving building blocks for the setup
steps performed by setup tools.

First there are those ``dusetup`` modules dedicated to handle specific file
types in setup steps:

+ :py:mod:`dusetup.doc`: tools to work with documentation files provided by
  the software to be set up.

+ :py:mod:`dusetup.elisp`: utilities for ELisp files used by the `Emacs`_
  editor.

+ :py:mod:`dusetup.python`: utilities to handle Python packages and modules.

Beside this there some ``dusetup`` utility modules that groups functionality
rather by the tasks they perform with setup steps:

+ :py:mod:`dusetup.command`: helpers to run (external) commands.

+ :py:mod:`dusetup.path`: functionality related to file or directory paths
  including basic building blocks to work for basic filesystem operations.

+ :py:mod:`dusetup.install`: helpers specifically to install files and
  directories.


.. [#fct_intp] Value interpolation is the possibility to refer, within the
               values of configuration files, to other values.  On of its
               forms refers to the value of a function call.  For this
               functionality to be available in the configuration files of
               setup tools, ``dusetup`` depends on package `duconfig`_.

.. _duconfig: https://gitlab.com/conlogic/duconfig

.. _Emacs: https://www.gnu.org/software/emacs


..
 -----------------------------------------------------------------------------
 Local Variables:
 mode: rst
 ispell-local-dictionary: "en"
 End:
