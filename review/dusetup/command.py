# ============================================================================
# Title          : Utilities for setup tools
#
# Classification : Python module
#
# Author         : Dirk Ullrich
#
# Date           : 2022-04-14
#
# Description    : See documentation string below.
# ============================================================================

'''
Utilities for external commands.
'''


import subprocess

from . import error


# *Done*
def run_cmd(cmd, error_rc=None, verbose=True, **kwargs):
    '''
    Run command given by list `cmd`.

    The keyword arguments in dictionary `kwargs` are passed to
    :py:func:`subprocess.run`.

    A not-``None`` number `error_rc` is used as return code in the error
    exception instead of the original one.

    If `verbose` is ``True`` a message with command to be run is printed.

    :return: the result of the command.

    :raise: a :py:exc:`SetupError` if running of the command has failed.
    '''
    error_exc = None
    try:
        if verbose:
            print(f"Run command\n  {cmd}")
        result = subprocess.run(cmd, check=True, **kwargs)
    except subprocess.CalledProcessError as e:
        error_exc = e
        error_rc = e.returncode if error_rc is None else error_rc
    except OSError as e:
        error_exc = e
        error_rc = e.errno if error_rc is None else error_rc
    if error_exc is not None:
        raise error.SetupError(
            f"Command\n  {cmd}\n has failed with error\n  {error_exc}",
            rc=error_rc)

    return result


# *Done*
def run_cmds(cmds, first_error_rc=None, verbose=True, **kwargs):
    '''
    Run commands given by list `cmds`.

    The keyword arguments in dictionary `kwargs` are passed to
    :py:func:`subprocess.run`.

    For a not-``None`` number `first_error_rc` a number `first_error_rc + n`
    is used as as return code in the error exception instead of the original
    one if the command given by ``n``-th element in `cmd` has failed.

    If `verbose` is ``True`` messages with the commands to be run is printed.

    :return: a list with the results of the commands.

    :raise: a :py:exc:`SetupError` if running of a command has failed.
    '''
    error_rc = first_error_rc
    results = []
    for c in cmds:
        result = run_cmd(c, error_rc=error_rc, verbose=verbose, **kwargs)
        results.append(result)
        if error_rc is not None:
            error_rc += 1

    return results


# ----------------------------------------------------------------------------
# Local Variables:
# ispell-local-dictionary: "en"
# End:
