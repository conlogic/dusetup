# ============================================================================
# Title          : Utilities for setup tools
#
# Classification : Python module
#
# Author         : Dirk Ullrich
#
# Date           : 2022-04-14
#
# Description    : See documentation string below.
# ============================================================================

'''
Generic utilities for setup steps.
'''


import time
import pathlib

from . import command
from . import install
from . import path
from . import error


# Parameters
# ==========

# *Done*
# Time (in seconds) before starting to run the setup steps.
RUN_START_TIME = 3.0

# *Done*
# Configuration section containing the setup steps.
STEPS_CFG_SECT = 'steps'

# Prefix for function instructions.
FCT_PREFIX = 'fct:'

# Prefix for command instructions.
CMD_PREFIX = 'cmd:'

# Template for the name if an install function for a file / item of a certain
# kind, with placeholder ``'{kind}'`` for the name of the combined kind.
INST_KIND_NAME_TPL = "install_{kind}"

# Separator for installation parameters.
INST_PARAM_SEP = ':'

# Separator in installation parameters between name and value.
INST_NAME_VAL_SEP = '='


# Functions
# =========


# Framework
# ---------


# *Done*
def run_steps(start_params, step_fcts_mod, steps_cfg_sect=STEPS_CFG_SECT):
    '''
    Run the setup steps as defined by parameter dictionary `start_params`.

    Module `step_fcts_mod` is the name of the module where the step functions
    are expected to be attributes of.

    The name of the steps are given by key `steps` in `start_params`, and the
    configuration by key `cfg` in `start_params`.

    For every `start_params` step named `s`, the function is given for
    parameter `s` in section `steps_cfg_sect` of th `start_params`
    configuration.  Every such function is called with a parameter dictionary,
    and returns itself such a parameter dictionary.  Actually the parameter
    dictionary of the first step is `start_params` without steps list, but the
    parameter dictionary for every later step function is the parameter
    dictionary returned by the previous step function.

    For every of these parameter dictionaries, the following parameters are at
    least expected:
    + key ``'app_name'`` with the name of the application to be set up.
    + key ``'app_vers'`` with the version of the application to be set up.
    + key ``'prefix_dpath'`` with the prefix directory path.

    :raise: a :py:exc:`SetupError` if running of a setup step has failed.
    '''
    # Get the step names and the configuration.
    steps = start_params.pop('steps')
    cfg = start_params['cfg']

    # Show important parameters (that could be modified by commandline
    # options).
    app_name = start_params['app_name']
    app_vers = start_params['app_vers']
    prefix_dpath = start_params['prefix_dpath'].resolve()
    print(f"===Run setup steps for application `{app_name}`===")
    print(f"Setup steps          : {steps}")
    print(f"Application version  : {app_vers}")
    print(f"Prefix directory path:\n  {prefix_dpath}")
    time.sleep(RUN_START_TIME)

    # Run, for every step in `steps`, the instructions for this step.
    params_s = start_params.copy()
    for s in steps:
        print(f"===Run `{s}` step===")
        # Get the step's instructions from the configuration.
        if s not in cfg[steps_cfg_sect]:
            raise error.SetupError(
                f"No function is configured for step `{s}`", rc=1)
        fct_name_s = cfg[f"{steps_cfg_sect}.{s}"]
        # Run the step's function.
        params_s = _run_step_function(fct_name_s, params_s, step_fcts_mod)


# *Done*
def _run_step_function(fct_name, params, step_fct_mod):
    '''
    Run step function given by string `fct_name` using parameter dictionary
    `params` a sole argument.  The function with name `fct_name` is expected
    to be an attribute of module `step_fct_mod`.

    :return: parameter dictionary calculated within the very step.

    :raise: a :py:exc:`SetupError` if running of the step function fails.
    '''
    # Get the step function.
    try:
        fct = getattr(step_fct_mod, fct_name)
    except NameError:
        raise error.SetupError(f"There is no module `{step_fct_mod}`.", rc=1)
    except AttributeError:
        raise error.SetupError(
            f"Module `{step_fct_mod}` has no function `{fct_name}`.", rc=2)

    # Run the step function.
    try:
        params = fct(params)
    except TypeError:
        raise error.SetupError(f"Cannot execute function `{fct_name}`.", rc=3)

    return params


# Concrete step components
# ------------------------


def apply_patches(params, patches_fpaths_key, prefix_strip_nr=1):
    '''
    Apply, to the files in the current working directory, the patch files
    given byas list value for key `patches_fpaths_key` in the configuration
    from parameter dictionary `params`, in the given order.  Relative paths
    this list are relative to the data directory path of `params`.  For a
    description of the parameter dictionary please consult the documentation
    of :py:func:`run_steps`.

    When applying, the a prefix with `prefix_strip_nr` many slashes is
    stripped of the patch.

    :raise: a :py:exc:`duconfig.ConfigError` for a configuration error.
    :raise: a :py:exc:`SetupError` if applying of a patch has failed.
    '''
    data_dpath = pathlib.Path(params['data_dpath'])

    cfg = params['cfg']
    patches_fpaths = cfg[patches_fpaths_key]
    cmd_prefix = f"patch -p{prefix_strip_nr} < "
    for f in patches_fpaths:
        f = pathlib.Path(f)
        print(f"Applying patch with path\n  {f}")
        if f.is_absolute():
            abs_f = f
        else:
            abs_f = data_dpath / f
        cmd_f = f"{cmd_prefix} {abs_f}"
        print(cmd_f)
        command.run_cmd(cmd_f, shell=True)


def rm_items(params, items_key):
    '''
    Remove all existing files or directories whose paths are given as list
    value for key `items_key` in the configuration from parameter dictionary
    `params`.  For a description of the parameter dictionary please consult
    the documentation of :py:func:`run_steps`.

    :raise: a :py:exc:`duconfig.ConfigError` for a configuration error.
    :raise: a :py:exc:`SetupError` if a removal fails.
    '''
    cfg = params['cfg']
    items_paths = cfg[items_key]
    path.rm_items(items_paths)


def autotools_configure(params, add_opts_key):
    '''
    Run Autotools-based ``configure`` with additional options given as list
    value for key `add_opts_key` in the configuration from parameter
    dictionary `params`.  For a description of the parameter dictionary please
    consult the documentation of :py:func:`run_steps`.

    If the ``configure`` does not already exists, it is tried to create it
    first.

    :raise: a :py:exc:`duconfig.ConfigError` for a configuration error.
    :raise: a :py:exc:`SetupError` if configuration has failed.
    '''
    configure_fpath = pathlib.Path.cwd() / 'configure'

    # Try to create ``configure``, if it does not already exist.
    if not configure_fpath.is_file():
        mk_configure_cmd = "autoreconf -i"
        command.run_cmd(mk_configure_cmd, shell=True)

    # Run ``configure``.
    prefix_dpath = params['prefix_dpath']
    configure_cmd = f"{configure_fpath} --prefix={prefix_dpath}"
    cfg = params['cfg']
    add_opts = cfg[add_opts_key]
    add_opts_str = ' '.join(add_opts)
    if len(add_opts_str) > 0:
        configure_cmd += f" {add_opts_str}"
    command.run_cmd(configure_cmd, shell=True)


# *Done*
def run_command(params, cmd_key, **kwargs):
    '''
    Run the command given as value for key `cmd_key` in the configuration from
    parameter dictionary `params`.  The command is called in a shell.

    Additional keyword arguments in `kwargs` are passed to the used function
    :py:mod:`command.run_cmd`.

    For a description of the parameter dictionary please consult the
    documentation of :py:func:`run_steps`.

    :return: the results of the command.

    :raise: a :py:exc:`duconfig.ConfigError` for a configuration error.
    :raise: a :py:exc:`SetupError` if a running of the command fails.
    '''
    params = params.copy()
    cfg = params.pop('cfg')
    # Replace in the command (template) the parameter placeholders by their
    # respective values.
    cmd = cfg[cmd_key].format_map(params)
    result = command.run_cmd(cmd, shell=True, **kwargs)

    return result


# *Done*
def run_commands(params, cmds_key, **kwargs):
    '''
    Run the commands given as list value for key `cmds_key` in the
    configuration from parameter dictionary `params`.  The commands are called
    in a shell.

    Additional keyword arguments in `kwargs` are passed to the used function
    :py:mod:`command.run_cmds`.

    For a description of the parameter dictionary please consult the
    documentation of :py:func:`run_steps`.

    :return: a list with the results of the commands.

    :raise: a :py:exc:`duconfig.ConfigError` for a configuration error.
    :raise: a :py:exc:`SetupError` if a running of a command fails.
    '''
    params = params.copy()
    cfg = params.pop('cfg')
    # Replace in the command (templates) the parameter placeholders by their
    # respective values.
    cmds = [c.format_map(params) for c in cfg[cmds_key]]
    results = command.run_cmds(cmds, shell=True, **kwargs)

    return results


# *Done*
def ensure_clean_destination(params):
    '''
    Ensure a clean directory for the prefix directory path from parameter
    dictionary `params`.  For a description of the parameter dictionary please
    consult the documentation of :py:func:`run_steps`.
    '''
    prefix_dpath = params.get('prefix_dpath')
    if prefix_dpath is not None:
        path.ensure_clean_dir(prefix_dpath)


# *Done*
def install_items(params, items_sect,
                  inst_params_sep=INST_PARAM_SEP,
                  inst_name_val_sep=INST_NAME_VAL_SEP):
    '''
    Install items given in section `items_sect` in the configuration from
    parameter dictionary `params`.  For a description of the parameter
    dictionary please consult the documentation of :py:func:`run_steps`.

    The items in map `items` are grouped by their kind as key, and a single
    installation element or list of installation elements of the respective
    kind as value.  In turn, every installation element starts with source
    path for this installation element, and has optionally appended
    installation parameters for its installation.  These installation
    parameters are separated by separator `inst_params_sep`, and every such
    installation parameter consists of a name and value, separated by
    separator `inst_name_val_sep`.

    :raise: a :py:exc:`duconfig.ConfigError` for a configuration error.
    :raise: a :py:exc:`SetupError` if installation has failed.
    '''
    cfg = params['cfg']
    inst_items = cfg[items_sect]
    for kind in inst_items.keys():
        elem_or_elems = inst_items[kind]
        _install_kind_elem_or_elems(
            kind, elem_or_elems, params, inst_params_sep, inst_name_val_sep)


# *Done*
def _install_kind_elem_or_elems(kind, elem_or_elems, params,
                                inst_params_sep, inst_name_val_sep):
    '''
    Install an element or elements `elem_or_elems`  of kind `kind` using
    parameter dictionary `params`.

    For a description of the parameter dictionary please consult the
    documentation of :py:func:`run_steps`.  For an explanation of arguments
    `inst_params_sep` and `inst_name_val_sep` please consult the documentation
    of :py:func:`install_items`.

    :raise: a :py:exc:`SetupError` if installation has failed.
    '''
    # Distinguish whether there is a single or multiple items.
    if kind.endswith('s'):
        single_kind = kind[:-1]
        is_single_item = False
    else:
        single_kind = kind
        is_single_item = True

    # Get installation function for a single item of the respective kind.
    inst_fct_name = INST_KIND_NAME_TPL.format(kind=single_kind)
    inst_fct = getattr(install, inst_fct_name, None)
    if inst_fct is None:
        raise error.SetupError(
            f"Installation for kind `{single_kind}` is not implemented.")

    # Run the installation function for the respective element(s).
    if is_single_item:
        _run_fct_for_elem(
            inst_fct, elem_or_elems, params,
            inst_params_sep, inst_name_val_sep)
    else:
        for e in elem_or_elems:
            _run_fct_for_elem(inst_fct, e, params,
                              inst_params_sep, inst_name_val_sep)


def _run_fct_for_elem(fct, elem, params, inst_params_sep, inst_name_val_sep):
    '''
    Install an element `elem` using installation function and parameter
    dictionary `params`.

    For a description of the parameter dictionary please consult the
    documentation of :py:func:`run_steps`.  For an explanation of arguments
    `inst_params_sep` and `inst_name_val_sep` please consult the documentation
    of :py:func:`install_items`.

    :raise: a :py:exc:`SetupError` if installation has failed.
    '''
    # Split the installation element into installation source path, and
    # optional installation parameters.
    (src_path, *params_strs) = elem.split(inst_params_sep)

    # Build the positional arguments for the installation function call.
    posargs = (
        src_path,
        params['prefix_dpath'],
        params['app_vers'],
        params['app_name']
    )

    # Build the keyword arguments for the installation function call.
    kwargs = {}
    for p in params_strs:
        comps_p = p.split(inst_name_val_sep)
        # A parameter should exactly consist of 2 components - namely of the
        # parameter name and parameter value.
        if not len(comps_p) == 2:
            raise error.SetupError(
                f"Malformed installation parameter `{p}`")
        (name_p, val_p) = comps_p
        kwargs[name_p] = val_p

    # Try to call the installation function.
    try:
        fct(*posargs, **kwargs)
    except TypeError as e:
        fct_name = fct.__name__
        msg = f"Trying to call install function\n  {fct_name}"
        msg += f"\n has failed with\n  {e}"
        raise error.SetupError(msg)


# ----------------------------------------------------------------------------
# Local Variables:
# ispell-local-dictionary: "en"
# End:

#  LocalWords:  Globals globals
