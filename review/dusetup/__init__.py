# ============================================================================
# Title          : Utilities for setup tools
#
# Classification : Python module
#
# Author         : Dirk Ullrich
#
# Date           : 2022-04-12
#
# Description    : See documentation string below.
# ============================================================================

'''
Init module for package ``dusetup``.
'''


# Version of the very package.
version = '0.18'


from .config import (
    read_config_file, upd_config_sections)
from .cmdline import (
    calc_default_param_vals, mk_cmdline_parser, calc_setup_params)
from .step import (
    run_steps)
from .error import (
    handle_error)

__all__ = [
    read_config_file, upd_config_sections,
    calc_default_param_vals, mk_cmdline_parser, calc_setup_params,
    run_steps,
    handle_error
]


# ----------------------------------------------------------------------------
# Local Variables:
# ispell-local-dictionary: "en"
# End:
