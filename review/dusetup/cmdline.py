# ============================================================================
# Title          : Utilities for setup tools
#
# Classification : Python module
#
# Author         : Dirk Ullrich
#
# Date           : 2022-04-14
#
# Description    : See documentation string below.
# ============================================================================

'''
Commandline utilities.
'''


import pathlib

try:
    from duargparse import DUArgumentParser as ArgumentParser
except ModuleNotFoundError:
    from argparse import ArgumentParser
import duconfig

from . import config


# Parameters
# ==========

# *Done*
# Default name for the section with default values in a configuration file.
CFG_DEFAULT_SECT = 'default'


# Functions
# =========


# *Done*
def calc_default_param_vals(cfg_data, default_cfg_sect=CFG_DEFAULT_SECT):
    '''
    Calculate the default values for standard commandline options with
    parameters from section with name `default_cfg_sect` of a configuration
    file read by function `read_config_file` that has returned configuration
    data `cfg_data`.

    :return: dictionary with the calculated default parameter values.

    :raise: a :py:exc:`duconfig.ConfigError` if something is wrong with the
            configuration used.
    '''
    # Read the default section.
    cfg_defaults = cfg_data['cfg'][default_cfg_sect]

    # Calculate the default parameter values.
    defaults = {}
    # - Application name.
    defaults['app_name'] = cfg_defaults['app_name']
    # - Application version.
    defaults['app_vers'] = cfg_defaults.get('app_vers', '')
    # - Prefix directory path template.
    defaults['prefix_dpath_tpl'] = cfg_defaults['prefix_dpath_tpl']
    # - Setup steps.
    defaults['steps'] = cfg_defaults['steps']
    # - Path of used configuration file.
    cfg_fpath = cfg_data['cfg_fpath']
    defaults['use_cfg_fpath'] = cfg_fpath
    # - Path of the exported configuration file.
    defaults['out_cfg_fpath'] = duconfig.user_config_file(
        cfg_fpath.name)

    return defaults


# *Done*
def mk_cmdline_parser(cmd_fpath, cmd_vers, cmd_descript, defaults):
    '''
    Create the commandline parser for tool called by path `cmd_fpath` that has
    description `cmd_descript` and version `cmd_version`.  Dictionary
    `defaults` contains default command parameter values.

    :return: the created commandline parser.
    '''
    # Create commandline parser.
    cmdl_parser = ArgumentParser(
        prog=pathlib.Path(cmd_fpath).name,
        description=cmd_descript)
    # Add option for version.
    cmdl_parser.add_argument(
        '-V', '--version',
        action='version', version=f"%(prog)s {cmd_vers}"
    )
    # Add option for application version.
    cmdl_parser.add_argument(
        '-v', '--appl-version', metavar='<app_vers>', help=f"""
        Use version <app_vers> for the application
        (default: `{defaults['app_vers']}`).  An empty value is treated as no
        version.""",
        action='store', dest='app_vers', default=None
    )
    # Add option for prefix directory path.
    app_name = defaults['app_name']
    default_app_name_vers_str = f"{app_name}[{config.APP_SEP}<app_vers>]"
    prefix_dpath_default_str = defaults['prefix_dpath_tpl'].format(
        app_name_vers=default_app_name_vers_str)
    cmdl_parser.add_argument(
        '-p', '--prefix', metavar='<prefix_dpath>', help=f"""
        Use prefix with directory path <prefix_dpath> (default:
        `{prefix_dpath_default_str}`, where the suffix is only added for a
        not-`None` application version <appl_vers>).""",
        action='store', dest='prefix_dpath_tpl', default=None
    )
    # Add option for setup steps.
    default_steps_str = ','.join(defaults['steps'])
    cmdl_parser.add_argument(
        '-s', '--steps', metavar='<steps>', help=f"""
        Use steps in comma-separated list <steps> to set up the application
        (default: `{default_steps_str}`).""",
        action='store', dest='steps_str', default=None
    )
    # Add option for the path of the used configuration file.
    cmdl_parser.add_argument(
        '-c', '--use-cfg-file', metavar='<cfg_fpath>', help=f"""
        Use configuration file with path <cfg_fpath> instead of the default
        one at `{defaults['use_cfg_fpath']}`""",
        action='store', dest='use_cfg_fpath', default=None
    )
    # Add option to export the active configuration file.
    cmdl_parser.add_argument(
        '-C', '--export-cfg-file', metavar='<cfg_fpath>', help=f"""
        Export active configuration file as file with path <cfg_fpath>, and
        exit.  If <cfg_fpath> is not given, user configuration file at
        `{defaults['out_cfg_fpath']}` is used.
        """,
        action='store', nargs='?', dest='out_cfg_fpath',
        const=defaults['out_cfg_fpath'], default=None
    )
    # Add option for debugging.
    cmdl_parser.add_argument(
        '-d', '--debug', help='''
        Enable debugging.
        ''',
        action='store_true', default=False
    )

    return cmdl_parser


# *Done*
def calc_setup_params(args, cfg_data, default_cfg_sect=CFG_DEFAULT_SECT):
    '''
    Calculate the setup parameters used from the result `args` of parsing the
    commandline, and configuration data `cfg_data` returned by function
    `read_config_file` from reading the configuration file.

    String `default_cfg_sect` is the name of the section in the configuration
    with the default parameter values.

    If wanted by `args`, export the configuration file, and exit.  Otherwise
    the calculated parameters form a dictionary with the following items:
    + key ``'app_name'`` with the application name as value,
    + key ``'app_vers'`` with the application version as value,
    + key ``'app_name_vers'`` with the combined application name and version
      as value,
    + key ``'step'`` with the list of the names of setup steps as value,
    + key ``'prefix_dpath'`` with prefix directory path as value,
    + key ``'cfg'`` with the read configuration as value.

    :return: dictionary with calculated parameters to be used.

    :raise: a :py:exc:`SystemExit` if the configuration file is exported.
    :raise: a :py:exc:`duconfig.ConfigError` if something is wrong with the
            configuration used.
    '''
    cfg = cfg_data['cfg']
    if args.use_cfg_fpath is not None:
        # An alternative configuration file was given -> read the
        # configuration from this file.
        cfg_fpath = pathlib.Path(args.use_cfg_fpath)
        cfg_fcts = cfg_data['cfg_fcts']
        alt_cfg = duconfig.Config(
            fcts=cfg_fcts, oth_begs=['('], oth_ends=[')'], raw=True)
        alt_cfg.read_file(cfg_fpath)
        # Update the default configuration by the alternative one.
        cfg = config.upd_config_sections(cfg, alt_cfg)
        cfg_data = {'cfg': cfg, 'cfg_fpath': cfg_fpath}
    else:
        # Otherwise reuse original configuration data.
        cfg_fpath = cfg_data['cfg_fpath']
    # Calculate the default parameter values.
    defaults = calc_default_param_vals(cfg_data, default_cfg_sect)

    # Calculate the setup parameters.
    params = {}
    # - Application name.
    params['app_name'] = defaults['app_name']
    # - Application version.
    if args.app_vers is not None:
        app_vers = args.app_vers
    else:
        app_vers = defaults['app_vers']
    params['app_vers'] = app_vers
    # - Setup steps.
    all_steps = defaults['steps']
    if args.steps_str is not None:
        # Run the requested setup steps always in their configured order.
        wanted_steps = args.steps_str.split(',')
        params['steps'] = [s for s in all_steps if s in wanted_steps]
    else:
        params['steps'] = all_steps
    # - Prefix directory path.
    if args.prefix_dpath_tpl is not None:
        prefix_dpath_tpl = args.prefix_dpath_tpl
    else:
        prefix_dpath_tpl = defaults['prefix_dpath_tpl']
    app_name_vers = config.add_app_version(
        params['app_name'], params['app_vers'])
    # For convenience the combined application name and version is stored,
    # too.
    params['app_name_vers'] = app_name_vers
    prefix_dpath_str = prefix_dpath_tpl.format(app_name_vers=app_name_vers)
    params['prefix_dpath'] = pathlib.Path(prefix_dpath_str).absolute()

    # Export the configuration file, if wanted, and exit.
    out_cfg_fpath = args.out_cfg_fpath
    if out_cfg_fpath is not None:
        config.export_config_file(cfg_fpath, out_cfg_fpath)
        raise SystemExit(0)

    # Store the used configuration, too.
    params['cfg'] = cfg

    return params


# ----------------------------------------------------------------------------
# Local Variables:
# ispell-local-dictionary: "en"
# End:
