# ============================================================================
# Title          : Utilities for setup tools
#
# Classification : Python module
#
# Author         : Dirk Ullrich
#
# Date           : 2022-04-14
#
# Description    : See documentation string below.
# ============================================================================

'''
Utilities to handle errors.
'''


import sys


# *Done*
class SetupError(Exception):
    '''
    Specific exception for errors related to setup.  Such an exception has a
    message `msg`, and optional location `loc` and optional return code `rc`.
    '''
    def __init__(self, msg, loc=None, rc=None):
        self.msg = msg
        self.loc = loc
        self.rc = rc


    def __str__(self):
        # Since we want to have full control whether the printable string for
        # the exception ends with a ``'.'`` or not we ensure first that there
        # is no ``'.'`` at the end.
        if self.msg.endswith('.'):
            ex_str = self.msg[:-1]
        else:
            ex_str = self.msg

        # When a return code is given, it is added.
        if self.rc is not None:
            ex_str += f" (rc={self.rc})"

        # When a location is given, we start a new line for it, but do not
        # want a ``'.'`` at the end.  Otherwise we ensure one.
        if self.loc is not None:
            ex_str += f" at\n  {self.loc}"
        else:
            ex_str += '.'

        return ex_str


# *Done*
class handle_error(object):
    '''
    Context manager to handle exceptions caused by errors, to print an
    appropriate error message if such an exception has be raised.  It is
    distinguished between :py:exc:`SetupError`s, and other error exceptions.
    Non-error exceptions (i.e. exceptions not inheriting from
    :py:exc:`Exception`) are not handled at all.

    Beside the attribute :py:attr:`rc` is set to an return code <> 0 iff an
    error has been handled.

    If `raise_exception` has value ``True``, raised error exceptions will
    re-raised, too.
    '''
    def __init__(self, raise_exception=False):
        '''
        Method to initialize this context.
        '''
        self.raise_exc = raise_exception

        # Default return code is 0.
        self.rc = 0


    def __enter__(self):
        '''
        Method to enter this context.
        '''
        # Nothing to do when entering the context.

        return self


    def __exit__(self, exc_type, exc_obj, traceback):
        '''
        Method to exit this context, where for not-``None`` type `exc_type` is
        the type of raised exception, a not-``None`` `exc_obj` is the
        exception raised, and a not-``None`` `traceback` is the associated
        trace back.

        Print an appropriate message to STDERR if an exception is raised.
        '''
        # Known errors should raise a :py:exc:`SetupError` (or an instance of
        # a subclass).
        exc_handled = not self.raise_exc
        if exc_type is None:
            # We cannot pass ``None`` to ``issubclass``.
            pass
        elif issubclass(exc_type, SetupError):
            # A known error has occurred.
            msg = f"The following setup error has occurred:\n  {exc_obj}"
            self.rc = exc_obj.rc if exc_obj.rc is not None else 1
        elif issubclass(exc_type, Exception):
            # An unknown error exception has been raised.
            exc_name = exc_type.__name__
            msg = f"An '{exc_name}' exception has raised:\n  {exc_obj}"
            self.rc = 2
        else:
            # A non-error exception should do its work.
            exc_handled = False

        if not self.rc == 0 and exc_handled:
            # Print an error message if an error has occurred, and an error
            # exception is not raised again.
            print(f"\n{msg}", file=sys.stderr)

        return exc_handled


    def enable_debug(self):
        '''
        Enable debugging: raised error exceptions will raised again.
        '''
        self.raise_exc = True


    def disable_debug(self):
        '''
        Disable debugging: raised error exceptions will not raised again.
        '''
        self.raise_exc = False


# ----------------------------------------------------------------------------
# Local Variables:
# ispell-local-dictionary: "en"
# End:
