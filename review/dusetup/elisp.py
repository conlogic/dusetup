# ============================================================================
# Title          : Utilities for setup tools
#
# Classification : Python module
#
# Author         : Dirk Ullrich
#
# Date           : 2022-03-28
#
# Description    : See documentation string below.
# ============================================================================

'''
Utilities for ELisp files.
'''

from . import path
from . import command


# Functions
# =========


# Path components for different file types
# ----------------------------------------


# *Done*
def is_elisp_file(fpath):
    '''
    :return: ``True`` if `fpath` is the file path for an ELisp file.
    '''

    is_elisp = path.has_ext('el')(fpath) or path.has_ext('elc')(fpath)

    return is_elisp


# Byte-compilation
# ----------------


# *Done*
def mk_elc_file(el_fpath, add_emacs_opts=[], force_recompile=False):
    '''
    Byte-compile ELisp file with path `el_fpath`.

    The elements of list `add_emacs_opts` are passed as additional options to
    the Emacs command.

    For a ``True`` switch `force_recompile` the file is even byte-compiled
    again if it was already done.

    :raise: a :py:exc:`SetupError` if byte-compilation has failed.
    '''
    cmd = ['emacs']
    cmd.extend(add_emacs_opts)
    if force_recompile:
        compile_fct = 'batch-byte-compile'
        compile_mode = 'forced'
    else:
        compile_fct = 'batch-byte-compile-if-not-done'
        compile_mode = 'if needed'
    cmd.extend(['--no-site-file', '--batch', '-f', compile_fct])
    cmd.append(str(el_fpath))

    print("Byte-compile ({0})\n  {1}".format(compile_mode, el_fpath))
    command.run_cmd(cmd)


# *Done*
def mk_elc_dir_files(el_dpath, recursive=False, crit=None,
                     add_emacs_opts=[], force_recompile=False):
    '''
    Byte-compile all ELisp source file in (if `recursive` is ``False``) or
    below (if `recursive` is ``True``) directory with path <el_dpath>.

    If `crit` is a not-``None`` path predicate only those ELisp source files
    are byte-compiled whose path fulfills `crit`.

    The elements of list `add_emacs_opts` are passed as additional options to
    the Emacs command.

    For a ``True`` switch `force_recompile` the files are even byte-compiled
    again if it was already done.
    '''
    # Extend the ELisp file criteria if needed.
    if crit is not None:
        def is_candidate(f):
            return (crit(f) and path.has_ext('el')(f))
    else:
        is_candidate = path.has_ext('el')

    # Since often the directory `el_dpath` contains more than one Elisp file
    # and some of these files may include others from `el_dpath` we add
    # `el_dpath` as additional load directory by default.
    add_emacs_opts.extend(['-L', el_dpath])

    for f in path.ls_dir_files(
            el_dpath, recursive, is_candidate, abspath=True):
        mk_elc_file(f, add_emacs_opts, force_recompile)


# ----------------------------------------------------------------------------
# Local Variables:
# ispell-local-dictionary: "en"
# End:
