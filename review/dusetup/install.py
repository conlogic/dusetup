# ============================================================================
# Title          : Utilities for setup tools
#
# Classification : Python module
#
# Author         : Dirk Ullrich
#
# Date           : 2022-04-09
#
# Description    : See documentation string below.
# ============================================================================

'''
Utilities to install items, i.e. files and directories.
'''


import pathlib

from . import path
from . import python
from . import elisp
from . import config


# Parameters
# ==========


# Default values for standard directories relative to the prefix.
STD_REL_DPATHS = {
    # Binary files.
    'bin': pathlib.Path('bin'),
    # Library files.
    'lib': pathlib.Path('lib'),
    # Shared files.
    'share': pathlib.Path('share'),
    # Documentation files.
    'doc': pathlib.Path('share') / 'doc',
    # Emacs site ELisp files.
    'emacs': pathlib.Path('share') / 'emacs' / 'site-lisp',
    # Vim files.
    'vim': pathlib.Path('share') / 'vim' / 'vimfiles',
    # Bash completion files.
    'bash': pathlib.Path('share') / 'bash-completion' / 'completions',
    # Zsh completion files.
    'zsh': pathlib.Path('share') / 'zsh' / 'site-functions'
}


# Functions
# =========


def bin_dir_path(prefix_dpath):
    '''
    Calculate the path for the binary directory for prefix directory path
    `prefix_dpath`.

    :return: calculated directory path.
    '''
    bin_dpath = _std_noapp_dir('bin', prefix_dpath)

    return bin_dpath


def bin_file_path(rel_fpath, prefix_dpath, vers=None):
    '''
    Calculate the path for the binary file with relative path `rel_fpath` for
    prefix directory path `prefix_dpath`.

    If `vers` is not ``None`` it is added as version to installed file name.

    :return: calculated file path.
    '''
    bin_fpath = _std_noapp_file('bin', rel_fpath, prefix_dpath, vers)

    return bin_fpath


def install_bin_file(bin_fpath, prefix_dpath, vers, *posargs,
                     rel_dst_fpath=None, **kwargs):
    '''
    Install binary file with path `bin_fpath` into the binary directory for
    prefix directory path `prefix_dpath`.

    If `vers` is not ``None`` it is added as version to installed file name.

    If `rel_dst_fpath` is a not-``None`` file path it is used as path of the
    installed file relative to `prefix_dpath` instead of `bin_fpath`'s file
    name.

    In `posargs` / `kwargs` further positional / keyword arguments not handled
    by the function will be collected.

    :return: path of the installed file.
    '''
    dst_fpath = _inst_noapp_std_file(
        'bin', bin_fpath, prefix_dpath, vers, rel_dst_fpath, fmode=0o755)

    return dst_fpath


def lib_dir_path(app_name, prefix_dpath, vers=None):
    '''
    Calculate the path for the library directory for application with name
    `app_name` with prefix directory path `prefix_dpath`.

    If `vers` is not ``None`` it is added as version to the application name.

    :return: calculated directory path.
    '''
    share_dpath = _std_app_dir('lib', app_name, prefix_dpath, vers)

    return share_dpath


def lib_item_path(rel_path, app_name, prefix_dpath, vers=None):
    '''
    Calculate the path for library file or directory with relative path
    `rel_path` for application with name `app_name` with prefix directory path
    `prefix_dpath`.

    If `vers` is not ``None`` it is added as version to the application name.

    :return: calculated item path.
    '''
    item_path = _std_app_item('lib', rel_path, app_name, prefix_dpath, vers)

    return item_path


def install_lib_item(lib_path, prefix_dpath, vers, app_name, *posargs,
                     rel_dst_path=None, **kwargs):
    '''
    Install a library file or directory with path `lib_path` into the library
    directory for application with name `app_name` with prefix directory path
    `prefix_dpath`.

    If `vers` is not ``None`` it is added as version to the installed
    application name.

    If `rel_dst_path` is a not-``None`` path it is used as path of the
    installed item relative to `prefix_dpath` instead of `lib_path`'s name.

    In `posargs` / `kwargs` further positional / keyword arguments not handled
    by the function will be collected.

    :return: path of the installed item.
    '''
    dst_path = _inst_app_std_item(
        'lib', lib_path, app_name, prefix_dpath, vers, rel_dst_path)

    return dst_path


def share_dir_path(app_name, prefix_dpath, vers=None):
    '''
    Calculate the path for the shared directory for application with name
    `app_name` with prefix directory path `prefix_dpath`.

    If `vers` is not ``None`` it is added as version to the application name.

    :return: calculated directory path.
    '''
    share_dpath = _std_app_dir('share', app_name, prefix_dpath, vers)

    return share_dpath


def share_item_path(rel_path, app_name, prefix_dpath, vers=None):
    '''
    Calculate the path for shared file or directory with relative path
    `rel_path` for application with name `app_name` with prefix directory path
    `prefix_dpath`.

    If `vers` is not ``None`` it is added as version to the application name.

    :return: calculated item path.
    '''
    item_path = _std_app_item('share', rel_path, app_name, prefix_dpath, vers)

    return item_path


def install_share_item(share_path, prefix_dpath, vers, app_name, *posargs,
                       rel_dst_path=None, **kwargs):
    '''
    Install a shared file or directory with path `share_path` into the shared
    directory for application with name `app_name` with prefix directory path
    `prefix_dpath`.

    If `vers` is not ``None`` it is added as version to the installed
    application name.

    If `rel_dst_path` is a not-``None`` path it is used as path of the
    installed item relative to `prefix_dpath` instead of `share_path`'s name.

    In `posargs` / `kwargs` further positional / keyword arguments not handled
    by the function will be collected.

    :return: path of the installed item.
    '''
    dst_path = _inst_app_std_item(
        'share', share_path, app_name, prefix_dpath, vers, rel_dst_path)

    return dst_path


def doc_dir_path(app_name, prefix_dpath, vers=None):
    '''
    Calculate the path for the documentation directory for application with
    name `app_name` with prefix directory path `prefix_dpath`.

    If `vers` is not ``None`` it is added as version to the application name.

    :return: calculated directory path.
    '''
    share_dpath = _std_app_dir('doc', app_name, prefix_dpath, vers)

    return share_dpath


def doc_item_path(rel_path, app_name, prefix_dpath, vers=None):
    '''
    Calculate the path for documentation file or directory with relative path
    `rel_path` for application with name `app_name` with prefix directory path
    `prefix_dpath`.

    If `vers` is not ``None`` it is added as version to the application name.

    :return: calculated item path.
    '''
    item_path = _std_app_item('doc', rel_path, app_name, prefix_dpath, vers)

    return item_path


def install_doc_item(doc_path, prefix_dpath, vers, app_name, *posargs,
                     rel_dst_path=None, **kwargs):
    '''
    Install a documentation file or directory with path `doc_path` into the
    documentation directory for application with name `app_name` with prefix
    directory path `prefix_dpath`.

    If `vers` is not ``None`` it is added as version to installed application
    name.

    If `rel_dst_path` is a not-``None`` path it is used as path of the
    installed item relative to `prefix_dpath` instead of `doc_path`'s name.

    In `posargs` / `kwargs` further positional / keyword arguments not handled
    by the function will be collected.

    :return: path of the installed item.
    '''
    dst_path = _inst_app_std_item(
        'doc', doc_path, app_name, prefix_dpath, vers, rel_dst_path)

    return dst_path


def install_python_item(py_path, prefix_dpath, vers, *posargs,
                        rel_dst_path=None, byte_compile='maybe', **kwargs):
    '''
    Install Python module file or package directory with path `py_path` into
    the Python site directory for prefix directory path `prefix_dpath`.

    If `vers` is not ``None`` it is added as version to installed item name.

    If `rel_dst_path` is a not-``None`` path it is used as path of the
    installed item relative to `prefix_dpath` instead of `doc_path`'s name.

    Argument `byte_compile` controls byte-compilation of the installed file:
    + Value ``'no'`` prevents byte-compilation.
    + Value ``'maybe'`` causes byte-compilation if no version is used.
    + Value ``'yes'`` forces byte-compilation.

    In `posargs` / `kwargs` further positional / keyword arguments not handled
    by the function will be collected.

    :return: path of the installed item.
    '''
    py_path = pathlib.Path(py_path)
    if rel_dst_path is None:
        rel_dst_path = py_path.name

    rel_dst_path_vers = config.add_app_version(rel_dst_path, vers)
    site_dpath = python.python_site_dir(prefix_dpath)
    dst_path = site_dpath / rel_dst_path_vers

    # Copy file.
    path.cpmv_item(py_path, dst_path, verbose=True)

    # Byte-compilation, if wanted.
    if byte_compile == 'no':
        do_compile = False
    elif byte_compile == 'maybe':
        do_compile = vers in [None, '']
    else:
        do_compile = True
    if do_compile:
        python.compile_python_item(dst_path)

    return dst_path


def install_emacs_file(emacs_fpath, prefix_dpath, vers, *posargs,
                       rel_dst_fpath=None, byte_compile='maybe', **kwargs):
    '''
    Install Emacs ELisp file with path `emacs_fpath` into the Emacs ELisp site
    directory for prefix directory path `prefix_dpath`.

    If `vers` is not ``None`` it is added as version to destination file name.

    If `rel_dst_fpath` is a not-``None`` file path it is used as path of the
    installed file relative to `prefix_dpath` instead of `emacs_fpath`'s file
    name.

    Argument `byte_compile` controls byte-compilation of the installed file:
    + Value ``'no'`` prevents byte-compilation.
    + Value ``'maybe'`` causes byte-compilation if no version is used.
    + Value ``'yes'`` forces byte-compilation.

    In `posargs` / `kwargs` further positional / keyword arguments not handled
    by the function will be collected.

    :return: path of the installed file.
    '''
    # Copy file.
    dst_fpath = _inst_noapp_std_file(
        'emacs', emacs_fpath, prefix_dpath, vers, rel_dst_fpath)

    # Byte-compilation, if wanted.
    if byte_compile == 'no':
        do_compile = False
    elif byte_compile == 'maybe':
        do_compile = vers in [None, '']
    else:
        do_compile = True
    if do_compile:
        elisp.mk_elc_file(dst_fpath)

    return dst_fpath


def install_vim_file(vim_fpath, prefix_dpath, vers, *posargs,
                     rel_dst_fpath=None, **kwargs):
    '''
    Install Vim file with path `vim_fpath` into the Vim file directory for
    prefix directory path `prefix_dpath`.

    If `vers` is not ``None`` it is added as version to destination file name.

    If `rel_dst_fpath` is a not-``None`` file path it is used as path of the
    installed file relative to `prefix_dpath` instead of `vim_fpath`'s file
    name.

    In `posargs` / `kwargs` further positional / keyword arguments not handled
    by the function will be collected.

    :return: path of the installed file.
    '''
    dst_fpath = _inst_noapp_std_file(
        'vim', vim_fpath, prefix_dpath, vers, rel_dst_fpath)

    return dst_fpath


def install_bash_file(bash_fpath, prefix_dpath, vers, *posargs,
                      rel_dst_fpath=None, **kwargs):
    '''
    Install Bash completion file with path `bash_fpath` into the Bash
    completion files directory for prefix directory path `prefix_dpath`.

    If `vers` is not ``None`` it is added as version to destination file name.

    If `rel_dst_fpath` is a not-``None`` file path it is used as path of the
    installed file relative to `prefix_dpath` instead of `bash_fpath`'s file
    name.

    In `posargs` / `kwargs` further positional / keyword arguments not handled
    by the function will be collected.

    :return: path of the installed file.
    '''
    dst_fpath = _inst_noapp_std_file(
        'bash', bash_fpath, prefix_dpath, vers, rel_dst_fpath)

    return dst_fpath


def install_zsh_file(zsh_fpath, prefix_dpath, vers, *posarge,
                     rel_dst_fpath=None, **kwargs):
    '''
    Install Zsh completion file with path `zsh_fpath` into the Zsh
    completion files directory for prefix directory path `prefix_dpath`.

    If `vers` is not ``None`` it is added as version to destination file name.

    If `rel_dst_fpath` is a not-``None`` file path it is used as path of the
    installed file relative to `prefix_dpath` instead of `zsh_fpath`'s file
    name.

    In `posargs` / `kwargs` further positional / keyword arguments not handled
    by the function will be collected.

    :return: path of the installed file.
    '''
    dst_fpath = _inst_noapp_std_file(
        'zsh', zsh_fpath, prefix_dpath, vers, rel_dst_fpath)

    return dst_fpath


def _inst_noapp_std_file(kind, src_fpath, prefix_dpath, vers=None,
                         rel_dst_fpath=None, fmode=None):
    '''
    Install file with path `src_fpath` into standard application-unspecific
    directory of kind `kind` for prefix directory with path `prefix_dpath`.

    If `rel_dst_fpath` is a not-``None`` file path it is used as path of the
    installed file relative to `prefix_dpath` instead of source file name.

    If `vers` is not ``None`` it is added as version to installed file name.

    A not-`None`` permission specification `fmode` is used to set the
    permission of the installed file.

    :return: path of the installed file.
    '''
    src_fpath = pathlib.Path(src_fpath)
    if rel_dst_fpath is None:
        rel_dst_fpath = src_fpath.name

    # Calculate the destination path.
    dst_fpath = _std_noapp_file(kind, rel_dst_fpath, prefix_dpath, vers)

    # Copy file.
    path.cpmv_file(src_fpath, dst_fpath, fmode=fmode, verbose=True)

    return dst_fpath


def _inst_app_std_item(kind, src_path, app_name, prefix_dpath, vers=None,
                       rel_dst_path=None):
    '''
    Install item with path `src_path` into standard directory of kind `kind`
    specific for application with name `app_name` for prefix directory with
    path `prefix_dpath`.

    If `rel_dst_fpath` is a not-``None`` path it is used as path of the
    installed item relative to `prefix_dpath` instead of source item name.

    If `vers` is not ``None`` it is added as version to installed item name.

    :return: path of the installed item.
    '''
    src_path = pathlib.Path(src_path)
    if rel_dst_path is None:
        rel_dst_path = src_path.name

    # Calculate the destination path.
    dst_path = _std_app_item(
        kind, rel_dst_path, app_name, prefix_dpath, vers)

    # Copy item.
    path.cpmv_item(src_path, dst_path, verbose=True)

    return dst_path


def _std_noapp_dir(kind, prefix_dpath=None, rel_dpaths=STD_REL_DPATHS):
    '''
    Calculate standard application-unspecific directory of kind `kind` for
    prefix directory with path `prefix_dpath`.

    If  `prefix_dpath` is ``None``, the relative directory  path is used
    instead.

    Dictionary `rel_dpaths` specifies the standard directories relative to the
    prefix.

    :return: the calculated directory path.
    '''
    std_dpath = path.add_prefix(rel_dpaths[kind], prefix_dpath)

    return std_dpath


def _std_noapp_file(kind, rel_fpath, prefix_dpath=None, vers=None,
                    rel_dpaths=STD_REL_DPATHS):
    '''
    Calculate, for a file with relative path `rel_fpath` and a standard
    application-unspecific directory of kind `kind` for prefix directory with
    path `prefix_dpath`, its full path.

    If  `prefix_dpath` is ``None``, the relative directory  path is used
    instead.

    If `vers` is not ``None`` it is added as version to installed file name.

    Dictionary `rel_dpaths` specifies the standard directories relative to the
    prefix.

    :return: the calculated file path.
    '''
    # Add version to relative file path.
    rel_fpath_vers = config.add_app_version(rel_fpath, vers)
    # Calculate standard directory path.
    std_dpath = _std_noapp_dir(kind, prefix_dpath)
    # Past full file path together.
    fpath = std_dpath / rel_fpath_vers

    return fpath


def _std_app_dir(kind, app_name, prefix_dpath=None, vers=None,
                 rel_dpaths=STD_REL_DPATHS):
    '''
    Calculate standard application-specific directory of kind `kind` for
    application with name `app_name`, version `vers` and prefix directory with
    path `prefix_dpath`.

    If  `prefix_dpath` is ``None``, the relative directory  path is used
    instead.

    If `vers` is not ``None`` it is added as version to installed file name.

    Dictionary `rel_dpaths` specifies the standard directories relative to the
    prefix.

    :return: the calculated directory path.
    '''
    std_dpath = path.add_prefix(rel_dpaths[kind], prefix_dpath)
    app_vers = config.add_app_version(app_name, vers)

    return std_dpath / app_vers


def _std_app_item(kind, rel_path, app_name, prefix_dpath=None, vers=None,
                  rel_dpaths=STD_REL_DPATHS):
    '''
    Calculate, for an item with relative path `rel_path` and a standard
    application-specific directory of kind `kind` for application with name
    `app_name`, version `vers` and prefix directory with path `prefix_dpath`.
    If  `prefix_dpath` is ``None``, the relative directory  path is used
    instead.

    If `vers` is not ``None`` it is added as version to installed file name.

    Dictionary `rel_dpaths` specifies the standard directories relative to the
    prefix.

    :return: the calculated item path.
    '''
    # Calculate standard directory path for application with version.
    app_dpath = _std_app_dir(kind, app_name, prefix_dpath, vers)
    # Past full item path together.
    path = app_dpath / rel_path

    return path


# ----------------------------------------------------------------------------
# Local Variables:
# ispell-local-dictionary: "en"
# End:
