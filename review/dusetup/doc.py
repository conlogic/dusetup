# ============================================================================
# Title          : Utilities for setup tools
#
# Classification : Python module
#
# Author         : Dirk Ullrich
#
# Date           : 2022-02-09
#
# Description    : See documentation string below.
# ============================================================================

'''
Utilities related to documentation.
'''


import pathlib
import os
import re
import subprocess


from . import path


# Parameters
# ==========

# Regular expression to match a numbered INFO file extension.
INFO_FEXT_RX = re.escape(os.extsep + 'info') + r'(-\d+)?$'

# Default name of the Info directory file.
INFO_DIR_FNAME = 'dir'


# Functions
# =========


# *Done*
def is_texi_file(fpath):
    '''
    :return: ``True`` if `fpath` is the path for a Texinfo file.
    '''
    is_texi = path.has_ext('texi')(fpath) or path.has_ext('texinfo')(fpath)

    return is_texi


# *Done*
def is_info_file(fpath):
    '''
    :return: ``True`` if `fpath` is the file path for an INFO file.
    '''
    fpath = pathlib.Path(fpath)

    # INFO files can have file extension ``.info-N`` where `N` is a number.
    info_matcher = re.search(INFO_FEXT_RX, str(fpath))
    is_info = info_matcher is not None

    return is_info


# *Done*
def texi2any_file(texi_fpath, add_opts=[], out_format='info', out_dpath=None):
    '''
    Transform Texinfo file with path `texi_fpath` to file(s) with format
    `out_format`.

    The options in list `add_opts` are passed to the transformation program.

    If `out_dpath` is a not-``None`` path, the directory with this path is
    used to store the output file(s) instead of the current working directory.

    :raise: a :py:ecs:`subprocess.CalledProcessError` if transformation has
            failed.
    '''
    texi_fpath = pathlib.Path(texi_fpath)
    if out_dpath is None:
        out_dpath = pathlib.Path.cwd()
    else:
        out_dpath = pathlib.Path(out_dpath)

    if out_format in ['info', 'pdf', 'html']:
        cmd = ['texi2any', f"--{out_format}", '--no-split']
        cmd.extend(add_opts)
        texi_fbase = texi_fpath.stem
        out_fname = texi_fbase + os.extsep + out_format
        out_fpath = out_dpath / out_fname
        cmd.extend([f"--output={out_fpath}", texi_fpath])
        out_dpath.mkdir(parents=True, exist_ok=True)
        subprocess.run(cmd, check=True)


# *Done*
def texi2any_dir_files(texi_dpath, crit=None, add_opts=[],
                       out_format='info', out_dpath=None):
    '''
    Transform all Texinfo files in directory with path <texi_dpath> to files
    with format `out_format`.

    If `crit` is a not-``None`` path predicate only those Texinfo files are
    transformed whose path fulfills `crit`.

    The options in list `add_opts` are passed to the transformation program.

    If `out_dpath` is a not-``None`` path, the directory with this path is
    used to store the output file(s) instead of the current working directory.

    :raise: a :py:ecs:`subprocess.CalledProcessError` if transformation has
            failed.
    '''
    # Extend the Texinfo file criteria if needed.
    if crit is not None:
        def is_candidate(f):
            return (crit(f) and is_texi_file(f))
    else:
        is_candidate = is_texi_file

    texi_fpaths = path.ls_dir_files(
        texi_dpath, recursive=False, crit=is_candidate, abspath=True)

    for f in texi_fpaths:
        texi2any_file(f, add_opts, out_format, out_dpath)


# *Done*
def install_info_dir_files(info_dpath, info_dir_fname=INFO_DIR_FNAME,
                           add_opts=[]):
    '''
    Install all Info files in directory with path `info_dpath` in Info
    directory file with name `info_dir_fname`.

    The options in list `add_opts` are additionally used for installation.

    :raise: a :py:ecs:`subprocess.CalledProcessError` if installation has
            failed.
    '''
    info_fpaths = path.ls_dir_files(
        info_dpath, recursive=False, abspath=True)

    for f in info_fpaths:
        cmd_f = ['install-info']
        cmd_f.extend(add_opts)
        cmd_f.extend([str(f), info_dir_fname])
        # We want relative entries for the installed Info files in the Info
        # directory file.
        subprocess.run(cmd_f, check=True, cwd=info_dpath)


# ----------------------------------------------------------------------------
# Local Variables:
# ispell-local-dictionary: "en"
# End:
