# ============================================================================
# Title          : Utilities for setup tools
#
# Classification : Python module
#
# Author         : Dirk Ullrich
#
# Date           : 2022-02-26
#
# Description    : See documentation string below.
# ============================================================================

'''
Utilities for Python packages.
'''


import site
import pathlib
import compileall


from . import path


# *Done*
def python_site_dir(prefix_dpath=None):
    '''
    :return: path for Python package site directory for prefix directory with
             path `prefix_dpath`, or its relative path, in `prefix_dpath` is
             ``None``.
    '''
    # Calculate the relative site directory path.
    # - User's site directory path.
    user_site_dpath = pathlib.Path(site.USER_SITE)
    # - User's base directory path.
    user_base_dpath = site.USER_BASE
    # Remove the user's base directory path
    rel_site_dpath = user_site_dpath.relative_to(user_base_dpath)

    # Add prefix, if wanted.
    site_dpath = path.add_prefix(rel_site_dpath, prefix_dpath)

    return site_dpath


# *Done*
def compile_python_item(path):
    '''
    Byte-compile Python module (i.e. file) or package (i.e. directory) with
    path `path`.
    '''
    path = pathlib.Path(path)

    if path.is_file():
        compileall.compile_file(path)
    elif path.is_dir():
        compileall.compile_dir(path)


# ----------------------------------------------------------------------------
# Local Variables:
# ispell-local-dictionary: "en"
# End:
