# ============================================================================
# Title          : Utilities for setup tools
#
# Classification : Python module
#
# Author         : Dirk Ullrich
#
# Date           : 2022-04-14
#
# Description    : See documentation string below.
# ============================================================================

'''
Utilities for configuration.
'''


import pathlib
import shutil
import os

import duconfig

from . import error


# Parameters
# ==========

# *Done*
# Separator between a name and application version.
APP_SEP = '-'


# Functions
# =========


# For value interpolation
# -----------------------

# This section contains functions to be used for value interpolation in
# configuration files.  Due to current type limitation for value interpolation
# based on the ``duipoldict`` package, such functions either return a string,
# or a dictionary or list with string values.


# *Done*
def calc_app_version(nr_parts=None, dpath=None, part_sep='.', sep=APP_SEP):
    '''
    Calculate version for the application in directory with path `dpath`.

    For a not-``None`` value `nr_parts` that can be converted into a positive
    integer , maximal `nr_parts` many parts of the calculated version is used,
    where the parts of a version are separated by string `part_sep`.

    If `dpath` is ``None``, the path of the current working directory is used.

    String `sep` is the separator between the application's name and its
    version in the name of `dpath`.  If the name of `dpath` does not contain a
    separator, the whole name is used as application version.

    :return: calculated application version.
    '''
    if dpath is None:
        dpath = pathlib.Path.cwd()
    else:
        dpath = pathlib.Path(dpath)
    dname = dpath.name

    sep_idx = dname.rfind(sep)
    if sep_idx < 0:
        # Name version separator was not found -> take the whole directory
        # name as application version.
        app_vers_str = dname
    else:
        # Name version separator was found -> application version is rightmost
        # part after the separator.
        app_vers_str = dname[sep_idx + 1:]

    if nr_parts is not None:
        app_vers_parts = app_vers_str.split(part_sep)
        app_vers_used_parts = app_vers_parts[:int(nr_parts)]
        app_vers = part_sep.join(app_vers_used_parts)
    else:
        app_vers = app_vers_str

    return app_vers


# *Done*
def calc_app_name(dpath=None, lowercase_only=True, sep=APP_SEP):
    '''
    Calculate name for the application in directory with path `dpath`.

    If `dpath` is ``None``, the path of the current working directory is used.

    For a ``True`` switch `lowercase_only` the calculated application names
    uses lowercase letters only.

    String `sep` is the separator between the application's name and its
    version in the name of `dpath`.  If the name of `dpath` does not contain a
    separator, the whole name is used as application name.

    :return: calculated application name.
    '''
    if dpath is None:
        dpath = pathlib.Path.cwd()
    else:
        dpath = pathlib.Path(dpath)
    dname = dpath.name

    sep_idx = dname.rfind(sep)
    if sep_idx < 0:
        # Name version separator was not found -> take name of the parent's
        # parent as application name.
        app_dpath = dpath.parent.parent
        app_name_str = app_dpath.name
    else:
        # Name version separator was found -> application name is the part
        # before the separator.
        app_name_str = dname[:sep_idx]

    if lowercase_only:
        app_name = app_name_str.lower()
    else:
        app_name = app_name_str

    return app_name


# *Done*
def add_app_version(name, vers=None, sep=APP_SEP):
    '''
    Append a version suffix for version `vers` to name `name`, if `vers` is
    neither ``None`` nor empty, and `name` itself otherwise.

    :return: combined name and version.
    '''
    if vers is None or vers == '':
        name_vers = name
    else:
        # We add the version to the basename, i.e. before all file extensions.
        name = pathlib.Path(name)
        base_vers = f"{name.stem}{sep}{vers}"
        name_vers = name.with_stem(base_vers)

    return name_vers


# *Done*
def count_usable_cpus():
    '''
    :return: number of CPUs the current process can use, represented as
             string.
    '''
    # Set of CPUs the current process is restricted to.
    cpu_set = os.sched_getaffinity(0)
    # Number of CPUs the current process is restricted to.
    nr_cpus = len(cpu_set)

    return str(nr_cpus)


# *Done*
# Collect the functions for interpolation in a list.
DEFAULT_CFG_FCTS = [
    calc_app_name, calc_app_version, add_app_version, count_usable_cpus]


# Remaining ones
# --------------


# *Done*
def read_config_file(cfg_fname, data_dpath, cfg_fcts=DEFAULT_CFG_FCTS):
    '''
    Read the configuration file with name `cfg_fname` - either from the user's
    configuration directory or from the data directory  with path
    `data_dpath`.  List `cfg_fcts` contains the functions available for value
    interpolation in the configuration file.

    The data for configuration is the following dictionary:

     + key ``'cfg'`` with the read configuration,

     + key ``'cfg_fpath'`` with the path of the used configuration file,

     + key ``'cfg_fcts'`` with list `cfg_fcts`.

    :return: the configuration data described above, if the respective
             configuration file exists and could be read.

    :raise: a :py:exc:`SetupError` if no configuration file does not exist.
    :raise: a :py:exc:`duconfig.ConfigError` if something is wrong with the
            configuration used.
    '''
    cfg = duconfig.Config(
        fcts=cfg_fcts, oth_begs=['('], oth_ends=[')'], raw=False)
    cfg_fpath = duconfig.calc_config_file(
        cfg_fname, fallback_dpath=data_dpath, check_fallback=True)

    if cfg_fpath is not None:
        # A configuration file exists -> read it.
        cfg.read_file(cfg_fpath)
    else:
        # No configuration exists -> error.
        raise error.SetupError(f"Missing configuration file\n  {cfg_fname}")

    cfg_data = {'cfg': cfg, 'cfg_fpath': cfg_fpath, 'cfg_fcts': cfg_fcts}

    return cfg_data


# *Done*
def export_config_file(cfg_fpath, out_fpath):
    '''
    Export currently active configuration file according to result `args` of
    parsing the commandline.
    '''
    cfg_fpath = pathlib.Path(cfg_fpath)
    out_fpath = pathlib.Path(out_fpath)
    if out_fpath.is_file() and cfg_fpath.samefile(out_fpath):
        print('Paths for active and exported configuration files are equal.')
    else:
        print(f"Active configuration file exported to\n  {out_fpath}")
        shutil.copy(cfg_fpath, out_fpath)


# *Done*
def upd_config_sections(old_cfg, new_cfg):
    '''
    Update the sections in configuration `old_cfg` with sections in
    configuration `new_cfg`.

    :return: updated configuration.
    '''
    # To work around a ``duipoldict`` / ``duconfig`` bug, we work with a
    # dictionary for `old_cfg`.
    old_data = old_cfg.write_dict()
    new_data = new_cfg.write_dict()
    # Update sections.
    data = old_data.copy()
    for s in new_data.keys():
        new_sect_s = new_data[s]
        if s in data.keys():
            data[s].update(new_sect_s)
        else:
            data[s] = new_sect_s
    # Create the updated configuration.
    # - We also merge the interpolation functions.
    old_fcts = set(old_cfg.fcts)
    new_fcts = set(new_cfg.fcts)
    fcts = list(old_fcts.union(new_fcts))
    # - Create the configuration object.
    cfg = duconfig.Config(fcts, raw=old_cfg.raw)
    cfg.read_dict(data)

    return cfg


# *Done*
def add_path_envvar(var_name, add_path, place='end', keep_empty=True):
    '''
    Extend search path value of environment variable with name `var_name` by
    additional path `add_path`.

    String `place` controls where the additional path is added:
    + at the begin, if `place` has value `'beg'`,
    + at the end, if `place` has value `'end'`.

    For a ``True`` switch `keep_empty` even an empty original value of the
    `var_name` variable is preserved.  Please note that a path separator at
    the `place`'s end of the old value of the `var_name` variable is always
    preserved.
    '''
    # Get the old paths.
    old_val = os.getenv(var_name)
    if old_val is None:
        old_paths = ''
    else:
        old_paths = old_val

    # What path separators are needed?
    # - Store whether the old paths have at the place we are about to add the
    #   additional path a path separator.
    old_has_sep = False
    # - A maybe path separator between old paths and additional path.
    maybe_sep = ''
    if len(old_paths) > 0:
        if place == 'end' and old_paths.endswith(os.pathsep):
            old_has_sep = True
        elif place == 'beg' and old_paths.startswith(os.pathsep):
            old_has_sep = True
        else:
            maybe_sep = os.pathsep
    elif keep_empty:
        maybe_sep = os.pathsep

    # Add the additional path at the proper place.
    if place == 'end':
        new_paths = old_paths + maybe_sep + str(add_path)
        if old_has_sep and not new_paths.endswith(os.pathsep):
            new_paths += os.pathsep
    elif place == 'beg':
        new_paths = str(add_path) + maybe_sep + old_paths
        if old_has_sep and not new_paths.startswith(os.pathsep):
            new_paths = os.pathsep + new_paths

    # Set the environment variable anew.
    os.environ[var_name] = new_paths


# ----------------------------------------------------------------------------
# Local Variables:
# ispell-local-dictionary: "en"
# End:

#  LocalWords:  CPUs
