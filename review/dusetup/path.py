# ============================================================================
# Title          : Utilities for setup tools
#
# Classification : Python module
#
# Author         : Dirk Ullrich
#
# Date           : 2022-04-14
#
# Description    : See documentation string below.
# ============================================================================

'''
Utilities related to file and directory paths.
'''


import pathlib
import os
import shutil


# Functions
# =========


# Components of paths
# -------------------


# *Done*
def has_ext(fext):
    '''
    Create a predicate to test file paths for file extension `fext`.

    :return: a function ``pred`` such that ``pred(fpath)`` is ``True`` iff
             file path ``fpath`` has file extension `ext`.
    '''

    def pred(fpath):
        fpath = pathlib.Path(fpath)
        return (fpath.suffix == os.extsep + fext)

    return pred


# *Done*
def add_prefix(path, prefix_dpath=None):
    '''
    :return: path `path` prefixed with prefix directory with path
             `prefix_dpath` if `prefix_dpath` is not ``None``, and `path`
             itself otherwise.
    '''
    if prefix_dpath is None:
        prefix_dpath = pathlib.Path('')
    else:
        prefix_dpath = pathlib.Path(prefix_dpath)

    prefixed_path = prefix_dpath / path

    return prefixed_path


# List directories
# ----------------


# *Done*
def ls_dir(dpath, recursive=False, crit=None, abspath=True):
    '''
    List the entries in (if `recursive` is ``False``) or below (if `recursive`
    is ``True``) directory with path `dpath'.

    If `crit` is a not-``None`` path predicate only those entries are listed
    whose path fulfills `crit`.

    If `abspath` is ``True`` the absolute paths of the entries are collected,
    and paths of the entries relative to `dpath` otherwise.

    :return: list of the collected entries.
    '''
    dpath = pathlib.Path(dpath)

    if crit is None:
        def crit(p):
            return True

    if recursive:
        entries = _ls_dir_recursive(dpath, crit, abspath)
    else:
        entries = _ls_dir_flat(dpath, crit, abspath)

    return entries


# *Done*
def _ls_dir_flat(dpath, crit, abspath=True):
    '''
    Helper function for :py:func:`ls_dir` for the flat case. i.e. that its
    `recursive` argument has value ``False``.
    '''
    entries_paths = [p for p in dpath.iterdir() if crit(p)]

    if abspath:
        entries = [p.absolute() for p in entries_paths]
    else:
        entries = [p.name for p in entries_paths]

    return entries


# *Done*
def _ls_dir_recursive(dpath, crit, abspath=True):
    '''
    Helper function for :py:func:`ls_dir` for the recursive case. i.e. that
    its `recursive` argument has value ``True``.
    '''
    entries_paths = []
    for (d, dnames_d, fnames_d) in os.walk(dpath):
        d = pathlib.Path(d)
        all_entries_names_d = dnames_d + fnames_d
        for p in all_entries_names_d:
            path_d_p = d / p
            if crit(path_d_p):
                if abspath:
                    entry_path_d_p = path_d_p.absolute()
                else:
                    entry_path_d_p = path_d_p.relative_to(dpath)
                entries_paths.append(entry_path_d_p)

    return entries_paths


def ls_dir_files(dpath, recursive=False, crit=None, abspath=True):
    '''
    List the file entries in (if `recursive` is ``False``) or below (if
    `recursive` is ``True``) directory with path `dpath'.

    If `crit` is a not-``None`` path predicate only those file entries are
    listed whose path fulfills `crit`.

    If `abspath` is ``True`` the absolute paths of the file entries are
    collected, and paths of the file entries relative to `dpath` otherwise.

    :return: list of the collected file entries.
    '''

    if crit is not None:
        def is_candidate(p):
            return (crit(p) and p.is_file())
    else:
        def is_candidate(p):
            return p.is_file()

    files_entries = ls_dir(dpath, recursive, is_candidate, abspath)

    return files_entries


# Removing
# --------


# *Done*
def rm_item(path, verbose=True):
    '''
    Remove an existing file or directory with path `path`.  For a directory
    its content is removed, too.

    If `verbose` is ``True`` a message about copying / moving is printed.
    '''
    path = pathlib.Path(path)

    if path.is_file():
        if verbose:
            print(f"Remove file\n  {path}")
        path.unlink()
    elif path.is_dir():
        if verbose:
            print(f"Remove  directory\n  {path}")
        shutil.rmtree(path)


def rm_items(paths, crit=None, verbose=True):
    '''
    Remove all existing files or directories whose paths are elements of list
    `paths`.

    If `crit` is a not-``None`` path predicate only those files and
    directories are removed whose path fulfills `crit`.

    If `verbose` is ``True`` a message about copying / moving is printed.
    '''
    if crit is None:
        def crit(p):
            return True

    for p in [pathlib.Path(p) for p in paths]:
        if crit(p):
            rm_item(p, verbose)


def rm_files(fpaths, crit=None, verbose=True):
    '''
    Remove all existing files whose paths are elements of list `fpaths`.

    If `crit` is a not-``None`` path predicate only those files are removed
    whose path fulfills `crit`.

    If `verbose` is ``True`` a message about copying / moving is printed.
    '''
    if crit is None:
        def crit(p):
            return True

    for f in [pathlib.Path(f) for f in fpaths]:
        if crit(f) and f.is_file():
            if verbose:
                print(f"Remove file\n  {f}")
            f.unlink()


# *Done*
def rm_dir_files(dpath, recursive=False, crit=None, verbose=True):
    '''
    Remove all existing files in (if `recursive` is ``False``) or below (if
    `recursive` is ``True``) directory with path `dpath'.

    If `crit` is a not-``None`` path predicate only those files are removed
    whose path fulfills `crit`.

    If `verbose` is ``True`` a message about copying / moving is printed.
    '''
    fpaths = ls_dir_files(dpath, recursive, crit)
    rm_files(fpaths, verbose=verbose)


# *Done*
def ensure_clean_dir(dpath, verbose=False):
    '''
    Ensure that directory with path `dpath` exists and is empty.

    If `verbose` is ``True`` a message about copying / moving is printed.
    '''
    dpath = pathlib.Path(dpath)
    if dpath.is_dir():
        for p in dpath.iterdir():
            rm_item(p, verbose)
    else:
        if verbose:
            print(f"Create directory\n  {dpath}")
        dpath.mkdir(parents=True)


# Copying and moving
# ------------------


# *Done*
def cpmv_file(src_fpath, dst_fpath, rm_src=False, fmode=None, dmode=None,
              cp_fct=None, verbose=True):
    '''
    Copy or move file with path `src_fpath` to path `dst_fpath`.  The
    directory for the destination file will be created if it does not already
    exist.

    If `rm_src` is ``True`` the source file is removed, too.

    A not-`None`` permission specification `mode` is used to set the
    permissions of the destination file.

    A not-`None`` permission specification `dmode` is used to set the
    permissions of all destination directories.

    If `cp_fct` is a not-``None`` function this function is used to copy the
    file instead of :py:func:`shutil.copy2`.

    If `verbose` is ``True`` a message about copying / moving is printed.
    '''
    src_fpath = pathlib.Path(src_fpath)
    dst_fpath = pathlib.Path(dst_fpath)

    if verbose:
        if rm_src:
            op_name = 'Move'
        else:
            op_name = 'Copy'
        print(f"{op_name} file\n  {src_fpath}\n to\n  {dst_fpath}")

    # We ensure that the destination directory exists.
    dst_dpath = dst_fpath.parent
    dst_dpath.mkdir(parents=True, exist_ok=True)
    if dmode is not None:
        os.chmod(dst_dpath, dmode)

    # Copy the source file to the destination.
    if cp_fct is None:
        cp_fct = shutil.copy2
    cp_fct(src_fpath, dst_fpath)

    # Set the permission of the destination, if wanted.
    if fmode is not None:
        dst_fpath.chmod(fmode)

    # Remove the source file if wanted.
    if rm_src:
        src_fpath.unlink()


# *Done*
def cpmv_dir(src_dpath, dst_dpath, rm_src=False, fmode=None, dmode=None,
             cp_fct=None, verbose=True):
    '''
    Copy recursively or move directory with path `src_dpath` to path
    `dst_dpath`.  The parent directories for the destination directory will be
    created if they do not already exist.

    If `rm_src` is ``True`` the source directory is removed, too.

    A not-`None`` permission specification `fmode` is used to set the
    permissions of all destination files.

    A not-`None`` permission specification `dmode` is used to set the
    permissions of all destination directories.

    If `cp_fct` is a not-``None`` function this function is used to copy the
    files instead of :py:func:`shutil.copy2`.

    If `verbose` is ``True`` a message about copying / moving is printed.
    '''
    src_dpath = pathlib.Path(src_dpath)
    dst_dpath = pathlib.Path(dst_dpath)

    if verbose:
        if rm_src:
            op_name = 'Move'
        else:
            op_name = 'Copy'
        print(f"{op_name} directory\n  {src_dpath}\n to\n  {dst_dpath}")

    # Copy the source directory to the destination.
    cpmv_dir_items(
        src_dpath, dst_dpath, recursive=True, rm_src=rm_src,
        fmode=fmode, dmode=dmode, verbose=False)


# *Done*
def cpmv_item(src_path, dst_path, rm_src=False, fmode=None, dmode=None,
              cp_fct=None, verbose=True):
    '''
    Copy (recursively) or move existing file or directory with path `src_path`
    to path `dst_path`.  The destination directory (if the source item is a
    file) or the parent directories for the destination directory (if the
    source item is a directory) will be created if they do not already exist.

    If `rm_src` is ``True`` the source item is removed, too.

    A not-`None`` permission specification `fmode` is used to set the
    permissions of all destination files.

    A not-`None`` permission specification `dmode` is used to set the
    permissions of all destination directories.

    If `cp_fct` is a not-``None`` function this function is used to copy the
    files instead of :py:func:`shutil.copy2`.

    If `verbose` is ``True`` a message about copying / moving is printed.
    '''
    src_path = pathlib.Path(src_path)
    if src_path.is_file():
        cpmv_file(src_path, dst_path, rm_src, fmode, dmode, cp_fct, verbose)
    elif src_path.is_dir():
        cpmv_dir(src_path, dst_path, rm_src, fmode, dmode, cp_fct, verbose)


# *Done*
def cpmv_items(src_paths, dst_dpath, src_crit=None, relative_to=None,
               rm_src=False, fmode=None, dmode=None,
               cp_fct=None, verbose=True):
    '''
    Copy (recursively) or move all files or directories with paths in list
    `src_paths` to an item below or in directory with path `dst_dpath`.  The
    destination directory (if the source item is a file) or the parent
    directories for the destination directory (if the source item is a
    directory) will be created if they do not already exist.

    If `src_crit` is a not-``None`` predicate for paths, only those items are
    copied whose path meets `src_crit`.

    For a not-``None`` directory path `relative_to` the path of each copied
    item relative to `dst_dpath` is its source path relative to `relative_to`.

    If `rm_src` is ``True`` the source will be removed, too.

    A not-`None`` permission specification `fmode` is used to set the
    permissions of all destination files.

    A not-`None`` permission specification `dmode` is used to set the
    permissions of all destination directories.

    If `cp_fct` is a not-``None`` function this function is used to copy the
    files instead of :py:func:`shutil.copy2`.

    If `verbose` is ``True`` a message about copying / moving is printed.
    '''
    src_paths = [pathlib.Path(p) for p in src_paths]
    dst_dpath = pathlib.Path(dst_dpath)

    if src_crit is None:
        src_paths_todo = src_paths
    else:
        src_paths_todo = [p for p in src_paths if src_crit(p)]

    if relative_to is not None:
        abs_rel_to_dpath = pathlib.Path(relative_to).absolute()

    for p in src_paths_todo:
        if relative_to is not None:
            rel_dst_dpath_p = p.relative_to(abs_rel_to_dpath)
        else:
            rel_dst_dpath_p = p.name
        dst_path_p = dst_dpath / rel_dst_dpath_p
        cpmv_item(p, dst_path_p, rm_src, fmode, dmode, cp_fct, verbose)


def cpmv_files(src_fpaths, dst_dpath, src_crit=None, relative_to=None,
               rm_src=False, fmode=None, dmode=None,
               cp_fct=None, verbose=True):
    '''
    Copy or move all files with paths in list `src_fpaths` to a file below or
    in directory with path `dst_dpath`.  The destination directory will be
    created if it does not already exist.

    If `src_crit` is a not-``None`` predicate for paths, only those files are
    copied whose path meets `src_crit`.

    For a not-``None`` directory path `relative_to` the path of each copied
    file relative to `dst_dpath` is its source path relative to `relative_to`.

    If `rm_src` is ``True`` the source will be removed, too.

    A not-`None`` permission specification `fmode` is used to set the
    permissions of all destination files.

    A not-`None`` permission specification `dmode` is used to set the
    permissions of all destination directories.

    If `cp_fct` is a not-``None`` function this function is used to copy the
    files instead of :py:func:`shutil.copy2`.

    If `verbose` is ``True`` a message about copying / moving is printed.
    '''
    if src_crit is not None:
        def is_candidate(p):
            return (src_crit(p) and p.is_file())
    else:
        def is_candidate(p):
            return p.is_file()

    cpmv_items(
        src_fpaths, dst_dpath, is_candidate,
        relative_to, rm_src, fmode, dmode, cp_fct, verbose)


# *Done*
def cpmv_dir_items(src_dpath, dst_dpath, src_crit=None, recursive=False,
                   rm_src=False, fmode=None, dmode=None,
                   cp_fct=None, verbose=True):
    '''
    Copy or move all files or directories below directory with path
    `src_dpath` to an item in or below directory with path `dst_dpath`.
    Actually, any copied item either uses
    + the same name like the respective source item and directory with path
      `dst_dpath`, if `recursive` is ``False``, or
    + the same path relative to `dst_dpath` like the respective source
      item relative to `src_dpath`, if `recursive` is ``True``.

    If `src_crit` is a not-``None`` predicate for paths, only those items are
    copied whose path meets `src_crit`.

    If `rm_src` is ``True`` the source will be removed, too.

    A not-`None`` permission specification `fmode` is used to set the
    permissions of all destination files.

    A not-`None`` permission specification `dmode` is used to set the
    permissions of all destination directories.

    If `cp_fct` is a not-``None`` function this function is used to copy the
    files instead of :py:func:`shutil.copy2`.

    If `verbose` is ``True`` a message about copying / moving is printed.
    '''
    src_paths_todo = ls_dir(src_dpath, recursive, src_crit, abspath=True)

    rel_to_dpath = src_dpath if recursive else None
    cpmv_items(
        src_paths_todo, dst_dpath, relative_to=rel_to_dpath, rm_src=rm_src,
        fmode=fmode, dmode=dmode, cp_fct=cp_fct, verbose=verbose)


def cpmv_dir_files(src_dpath, dst_dpath, src_crit=None, recursive=False,
                   rm_src=False, fmode=None, dmode=None,
                   cp_fct=None, verbose=True):
    '''
    Copy or move all files below directory with path `src_dpath` to a file
    in or below directory with path `dst_dpath`.  Actually, any copied file
    either uses
    + the same filename like the respective source file and directory with path
      `dst_dpath`, if `recursive` is ``False``, or
    + the same filepath relative to `dst_dpath` like the respective source
      file relative to `src_dpath`, if `recursive` is ``True``.

    If `src_crit` is a not-``None`` predicate for paths, only those files are
    copied whose path meets `src_crit`.

    If `rm_src` is ``True`` the source will be removed, too.

    A not-`None`` permission specification `fmode` is used to set the
    permissions of all destination files.

    A not-`None`` permission specification `dmode` is used to set the
    permissions of all destination directories.

    If `cp_fct` is a not-``None`` function this function is used to copy the
    files instead of :py:func:`shutil.copy2`.

    If `verbose` is ``True`` a message about copying / moving is printed.
    '''
    if src_crit is not None:
        def is_candidate(p):
            return (src_crit(p) and p.is_file())
    else:
        def is_candidate(p):
            return p.is_file()

    cpmv_dir_items(
        src_dpath, dst_dpath, is_candidate, recursive, rm_src,
        fmode, dmode, cp_fct, verbose)


# Linking
# -------


def symlink_item(src_path, dst_path, dst_as_dir=False, mk_relative=True,
                 verbose=True):
    '''
    Symlink file or directory with path `src_path` to path `dst_path`.  For a
    ``True`` switch `dst_as_dir` path `dst_path` is supposed to be the path of
    a directory, and used as directory path of the link instead of the
    directory path of `dst_path`.  The link directory will be created if they
    do not already exist.

    If switch `mk_relative` is ``True``, the created link is a relative
    instead of an absolute one.

    If `verbose` is ``True`` a message about copying / moving is printed.
    '''
    src_path = pathlib.Path(src_path)
    dst_path = pathlib.Path(dst_path)

    # Calculate the proper link path and its directory path.
    if dst_as_dir:
        src_name = src_path.name
        lnk_path = dst_path / src_name
        lnk_dpath = dst_path
    else:
        lnk_path = dst_path
        lnk_dpath = dst_path.parent

    # We ensure that the link's directory exists.
    lnk_dpath.mkdir(parents=True, exist_ok=True)

    # Use the right variant for the link target path.
    if mk_relative:
        tgt_path = os.path.relpath(src_path, start=lnk_dpath)
    else:
        tgt_path = lnk_path.absolute()

    # Create the symlink.
    if verbose:
        print(f"Symlink\n  {tgt_path}\n to\n  {lnk_path}")
    lnk_path.symlink_to(tgt_path)


def symlink_items(src_paths, dst_dpath, src_crit=None, mk_relative=True,
                  verbose=True):
    '''
    Symlink files or directories with paths in list `src_paths` to links with
    same name in directory with path `dst_dpath`.  This directory will be
    created if they do not already exist.

    If `src_crit` is a not-``None`` predicate for paths, only those items are
    linked whose path meets `src_crit`.

    If switch `mk_relative` is ``True``, the created link is a relative
    instead of an absolute one.

    If `verbose` is ``True`` a message about copying / moving is printed.
    '''
    for p in src_paths:
        if src_crit is None or src_crit(p):
            symlink_item(
                p, dst_dpath, dst_as_dir=True,
                mk_relative=mk_relative, verbose=verbose)


def symlink_dir_items(src_dpath, dst_dpath, src_crit=None, mk_relative=True,
                      verbose=True):
    '''
    Symlink all files or directories in directory with path `src_dpath` to
    links with same name in directory with path `dst_dpath`.  This directory
    will be created if they do not already exist.

    If `src_crit` is a not-``None`` predicate for paths, only those items are
    linked whose path meets `src_crit`.

    If switch `mk_relative` is ``True``, the created link is a relative
    instead of an absolute one.

    If `verbose` is ``True`` a message about copying / moving is printed.
    '''
    src_paths = ls_dir(src_dpath, recursive=False, crit=src_crit)
    symlink_items(
        src_paths, dst_dpath, mk_relative=mk_relative, verbose=verbose)


# ----------------------------------------------------------------------------
# Local Variables:
# ispell-local-dictionary: "en"
# End:
