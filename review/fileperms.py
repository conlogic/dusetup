def is_rdir(path):
    path = pathlib.Path(path)
    try:
        is_dir = path.is_dir()
        if is_dir:
            list(path.iterdir())
            res = True
        else:
            res = False
    except PermissionError:
        res = False

    return res


def is_rfile(path):
    path = pathlib.Path(path)
    try:
        is_file = path.is_file()
        if is_file:
            with open(path, 'rb'):
                pass
            res = True
        else:
            res = False
    except PermissionError:
        res = False

    return res
