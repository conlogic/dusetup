'''
Module :py:mod:`cmdline` for package ``idris2Setup``.
'''

import pathlib
import dusetup
from . import version
from . import steps

# Parameters
# ==========

# Path for the data directory.
DATA_DPATH = pathlib.Path(__file__).parent / 'data'

# Name of the configuration file for this very package.
CFG_FNAME = 'idris2-setup.conf'

# Functions
# =========

def main(argv):
    '''
    Main function using the command line argument list `argv`.
    '''
    with dusetup.handle_error(raise_exception=True) as err_hdl:

        # 1. Read the default configuration file.
        cfg_data = dusetup.read_config_file(CFG_FNAME, DATA_DPATH)

        # 2. Get the default commandline parameters.
        default_params = dusetup.calc_default_param_vals(cfg_data)

        # 3. Create the commandline parser.
        cmdl_parser = dusetup.mk_cmdline_parser(
            argv[0], version, "Set up Idris 2 application from source.",
            default_params)

        # 4. Parse the commandline.
        args = cmdl_parser.parse_args(argv[1:])

        # 5. If debugging is enabled, error exceptions are re-raised and their
        #    full stack trace printed.
        if args.debug:
            err_hdl.enable_debug()
        else:
            err_hdl.disable_debug()

        # 6. Calculate setup parameters, and export configuration file and
        #    exit, if wanted.
        params = calc_setup_params(args, cfg_data)

        # 7. Run setup steps.
        dusetup.run_steps(params, steps)

    return err_hdl.rc

def calc_setup_params(args, cfg_data):
    '''
    Variant of :py:func:`dusetup.calc_setup_params` for Idris 2 setup
    parameters.  For details see the documentation of original function
    :py:func:`dusetup.calc_setup_params`
    '''
    # Calculate the standard setup parameters.
    params = dusetup.calc_setup_params(args, cfg_data)

    # Add additional setup parameters for Idris 2.
    app_name = params['app_name']
    app_vers = params['app_vers']
    prefix_dpath = params['prefix_dpath']
    # - Binary directory path.
    params['bin_dpath'] = dusetup.install.bin_dir_path(prefix_dpath)
    # - Library directory path.
    params['lib_dpath'] = dusetup.install.lib_dir_path(
        app_name, prefix_dpath, app_vers)
    # - Documentation directory path.
    params['doc_dpath'] = dusetup.install.doc_dir_path(
        app_name, prefix_dpath, app_vers)
    # - Bootstrap directory path.
    app_name_vers = params['app_name_vers']
    rel_boot_dpath_tpl = params['cfg']['params.rel_boot_dpath_tpl']
    rel_boot_dpath = rel_boot_dpath_tpl.format(app_name_vers=app_name_vers)
    params['boot_dpath'] = pathlib.Path(rel_boot_dpath).absolute()
    # - Command file path.
    params['cmd_fpath'] = dusetup.install.bin_file_path(
        app_name, prefix_dpath, app_vers)

    # Add bootstrapped bin directory path to the value of `PATH`.
    boot_bin_dpath = params['boot_dpath'] / 'bin'
    dusetup.config.add_path_envvar('PATH', boot_bin_dpath, place='beg')
    # Add bootstrapped lib directory path to the value of `LD_LIBRARY_PATH`.
    boot_lib_dpath = params['boot_dpath'] / 'lib'
    dusetup.config.add_path_envvar(
        'LD_LIBRARY_PATH', boot_lib_dpath, place='beg')

    return params
