#!/bin/bash

# =========================
# Set up Idris2 from source
# =========================


# Parameters
# ==========

# Application name.
APP_NAME='idris2'
# Application version.
APP_VERS="${APP_VERS:-devel}"
# Combined application namen and version.
APP_NAME_VERS="${APP_NAME}-$APP_VERS"
# Path for Idris2 installation prefix directory.
PREFIX_DPATH="${PREFIX_DPATH:-/tmp/$APP_NAME_VERS}"

# Binary directory path.
BIN_DPATH="$PREFIX_DPATH/bin"
# Library directory path.
LIB_DPATH="$PREFIX_DPATH/lib/$APP_NAME_VERS"
# Documentation directory path.
DOC_DPATH="$PREFIX_DPATH/share/doc/$APP_NAME_VERS"
# Bootstrap directory path.
BOOT_DPATH="$(pwd)/bootstrap/$APP_NAME_VERS"
# Command file path.
CMD_FPATH="$BIN_DPATH/$APP_NAME_VERS"

# Add bootstrapped bin directory path to the value of `PATH`.
boot_bin_dpath="${BOOT_DPATH}/bin"
export PATH="$boot_bin_dpath:$PATH"
# Add bootstrapped lib directory path to the value of `LD_LIBRARY_PATH`.
boot_lib_dpath="${BOOT_DPATH}/lib"
export LD_LIBRARY_PATH="$boot_lib_dpath:$LD_LIBRARY_PATH"

# Functions
# =========

bootstrap_step() {
    echo '======Bootstrap======'
    echo '------Cleanup-------'
    make distclean && rm -rf "$BOOT_DPATH" || return 1

    echo '------Bootstrap application and libraries------'
    make bootstrap SCHEME=chez PREFIX="$BOOT_DPATH" || return 2

    echo '------Install bootstrapped application and libraries------'
    make install PREFIX="$BOOT_DPATH" || return 3

    return 0
}

build_step() {
    echo '======Build======'
    echo '------Cleanup-------'
    make clean || return 1

    echo '------Build application and libraries------'
    make all PREFIX="$LIB_DPATH" || return 2

    echo '------Build application documentation------'
    mkdir -p docs/source/_static && make -C docs html || return 3

    return 0
}

install_app_step() {
    echo '======Install application======'
    echo '------Cleanup-------'
    rm -rf "$PREFIX_DPATH"/* || return 1

    echo '------Install application files------'
    make install-idris2 \
         PREFIX="$LIB_DPATH" IDRIS2_PREFIX="$LIB_DPATH" || return 2
    make install-support \
         PREFIX="$LIB_DPATH" IDRIS2_PREFIX="$LIB_DPATH" || return 3

    echo '------Create `bin` link------'
    local rel_lib_bin_fpath="../lib/$APP_NAME_VERS/bin/$APP_NAME"
    local cw_dpath=$(pwd)
    mkdir -pv "$BIN_DPATH" || return 4
    cd "$BIN_DPATH" \
        && ln -sfv "$rel_lib_bin_fpath" "$APP_NAME_VERS" || return 5
    cd "$cw_dpath"

    return 0
}

install_lib_step() {
    echo '======Install library======'
    echo '------Install library files------'
    make install-with-src-libs \
         PREFIX="$LIB_DPATH" IDRIS2_PREFIX="$LIB_DPATH" || return 1
    echo '------Clean up old shared library stuff------'
    rm -rfv "$LIB_DPATH/lib" || return 2

    echo '------Install application API------'
    make install-with-src-api \
         PREFIX="$LIB_DPATH" IDRIS2_PREFIX="$LIB_DPATH" || return 3

    echo '------Link application libraries------'
    local inst_lib_dpath=$($CMD_FPATH --libdir)
    local rel_inst_lib_dpath=$(basename "$inst_lib_dpath")/lib
    local cw_dpath=$(pwd)
    cd "$LIB_DPATH" && \
        for l in $(ls -1 "$rel_inst_lib_dpath"); do
            ln -sfv "$rel_inst_lib_dpath/$l" || return 4
        done
    cd "$cw_dpath"

    return 0
}

install_doc_step() {
    echo '======Install documentation======'
    mkdir -pv "$DOC_DPATH" || return 1
    echo '------Install application documentation------'
    cp -rv docs/build/html "$DOC_DPATH/application" || return 2

    echo '------Install library documentation------'
    make install-libdocs PREFIX="$LIB_DPATH" || return 3
    local inst_lib_dpath=$($CMD_FPATH --libdir)
    mv -v "$inst_lib_dpath/docs" "$DOC_DPATH/libs" || return 4

    return 0
}

# Doing
# =====

case "$1" in
    bootstrap)
        bootstrap_step
        ;;
    build)
        build_step
        ;;
    install-app)
        install_app_step
        ;;
    install-lib)
        install_lib_step
        ;;
    install-doc)
        install_doc_step
        ;;
    all)
        bootstrap_step && build_step && \
            install_app_step && install_lib_step && install_doc_step
        ;;
esac
