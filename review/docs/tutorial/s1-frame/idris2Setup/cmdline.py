'''
Module :py:mod:`cmdline` for package ``idris2Setup``.
'''

import pathlib
import dusetup
from . import version
from . import steps

# Parameters
# ==========

# Path for the data directory.
DATA_DPATH = pathlib.Path(__file__).parent / 'data'

# Name of the configuration file for this very package.
CFG_FNAME = 'idris2-setup.conf'

# Functions
# =========

def main(argv):
    '''
    Main function using the command line argument list `argv`.
    '''
    with dusetup.handle_error(raise_exception=True) as err_hdl:

        # 1. Read the default configuration file.
        cfg_data = dusetup.read_config_file(CFG_FNAME, DATA_DPATH)

        # 2. Get the default commandline parameters.
        default_params = dusetup.calc_default_param_vals(cfg_data)

        # 3. Create the commandline parser.
        cmdl_parser = dusetup.mk_cmdline_parser(
            argv[0], version, "Set up Idris 2 application from source.",
            default_params)

        # 4. Parse the commandline.
        args = cmdl_parser.parse_args(argv[1:])

        # 5. If debugging is enabled, error exceptions are re-raised and their
        #    full stack trace printed.
        if args.debug:
            err_hdl.enable_debug()
        else:
            err_hdl.disable_debug()

        # 6. Calculate setup parameters, and export configuration file and
        #    exit, if wanted.
        params = dusetup.calc_setup_params(args, cfg_data)

        # 7. Run setup steps.
        dusetup.run_steps(params, steps)

    return err_hdl.rc
