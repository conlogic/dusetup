'''
Setup step functions.
'''

from dusetup import step

def bootstrap_step(params):
    '''
    Function for ``bootstrap`` setup step using setup parameters `params`.

    :return: setup parameters after this step.

    :raise: a :py:exc:`duconfig.ConfigError` if there is something wrong with
            the configuration.
    :raise: a :py:exc:`SetupError` if running of a step command has failed.
    '''
    step.run_commands(params, 'bootstrap.cmds_tpls')

    return params

def build_step(params):
    '''
    Function for ``build`` setup step using setup parameters `params`.

    :return: setup parameters after this step.

    :raise: a :py:exc:`duconfig.ConfigError` if there is something wrong with
            the configuration.
    :raise: a :py:exc:`SetupError` if running of a step command has failed.
    '''
    step.run_commands(params, 'build.cmds_tpls')

    return params
