'''
Setup step functions.
'''

from dusetup import (step, path)

def bootstrap_step(params):
    '''
    Function for ``bootstrap`` setup step using setup parameters `params`.

    :return: setup parameters after this step.

    :raise: a :py:exc:`duconfig.ConfigError` if there is something wrong with
            the configuration.
    :raise: a :py:exc:`SetupError` if running of a step command has failed.
    '''
    step.run_commands(params, 'bootstrap.cmds_tpls')

    return params

def build_step(params):
    '''
    Function for ``build`` setup step using setup parameters `params`.

    :return: setup parameters after this step.

    :raise: a :py:exc:`duconfig.ConfigError` if there is something wrong with
            the configuration.
    :raise: a :py:exc:`SetupError` if running of a step command has failed.
    '''
    step.run_commands(params, 'build.cmds_tpls')

    return params

def install_app_step(params):
    '''
    Function for ``install-app`` setup step using setup parameters `params`.

    :return: setup parameters after this step.

    :raise: a :py:exc:`duconfig.ConfigError` if there is something wrong with
            the configuration.
    :raise: a :py:exc:`SetupError` if running of a step command has failed.
    '''
    step.ensure_clean_destination(params)
    step.run_commands(params, 'install-app.cmds_tpls')
    cfg = params['cfg']
    cmd_src_fpath_tpl = cfg['install-app.cmd_src_fpath_tpl']
    cmd_src_fpath = cmd_src_fpath_tpl.format_map(params)
    cmd_dst_fpath = params['cmd_fpath']
    path.symlink_item(cmd_src_fpath, cmd_dst_fpath, mk_relative=True)

    return params
