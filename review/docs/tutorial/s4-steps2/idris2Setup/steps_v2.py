'''
Setup step functions.
'''

import pathlib
import subprocess

from dusetup import (step, path)

def bootstrap_step(params):
    '''
    Function for ``bootstrap`` setup step using setup parameters `params`.

    :return: setup parameters after this step.

    :raise: a :py:exc:`duconfig.ConfigError` if there is something wrong with
            the configuration.
    :raise: a :py:exc:`SetupError` if running of a step command has failed.
    '''
    step.run_commands(params, 'bootstrap.cmds_tpls')

    return params

def build_step(params):
    '''
    Function for ``build`` setup step using setup parameters `params`.

    :return: setup parameters after this step.

    :raise: a :py:exc:`duconfig.ConfigError` if there is something wrong with
            the configuration.
    :raise: a :py:exc:`SetupError` if running of a step command has failed.
    '''
    step.run_commands(params, 'build.cmds_tpls')

    return params

def install_app_step(params):
    '''
    Function for ``install-app`` setup step using setup parameters `params`.

    :return: setup parameters after this step.

    :raise: a :py:exc:`duconfig.ConfigError` if there is something wrong with
            the configuration.
    :raise: a :py:exc:`SetupError` if running of a step command has failed.
    '''
    step.ensure_clean_destination(params)
    step.run_commands(params, 'install-app.cmds_tpls')
    cfg = params['cfg']
    cmd_src_fpath_tpl = cfg['install-app.cmd_src_fpath_tpl']
    cmd_src_fpath = cmd_src_fpath_tpl.format_map(params)
    cmd_dst_fpath = params['cmd_fpath']
    path.symlink_item(cmd_src_fpath, cmd_dst_fpath, mk_relative=True)

    return params

def calc_idris2_lib_dpath(params):
    '''
    If parameter ``inst_lib_dpath`` is not already stored in the setup
    parameter dictionary `params`, calculate it and store it there.

    :return: the new setup parameter dictionary.

    :raise: a :py:exc:`duconfig.ConfigError` if there is something wrong with
            the configuration.
    :raise: a :py:exc:`SetupError` if running of the command has failed.
    '''
    if params.get('inst_lib_dpath') is None:
        # We have to catch STDOUT of the command as string.
        result = step.run_command(
            params, 'install-lib.libdir_cmd_tpl',
            stdout=subprocess.PIPE, text=True)
        # Remove a newline from the output's end.
        result_stdout = result.stdout.rstrip()
        params['inst_lib_dpath'] = pathlib.Path(result_stdout).resolve()

    return params

def install_lib_step(params):
    '''
    Function for ``install-lib`` setup step using setup parameters `params`.

    :return: setup parameters after this step.

    :raise: a :py:exc:`duconfig.ConfigError` if there is something wrong with
            the configuration.
    :raise: a :py:exc:`SetupError` if running of a step command has failed.
    '''
    step.run_command(params, 'install-lib.inst_lib_cmd_tpl')
    step.rm_items(params, 'install-lib.del_paths')
    step.run_command(params, 'install-lib.inst_api_cmd_tpl')
    params = calc_idris2_lib_dpath(params)
    cfg = params['cfg']
    lib_src_dpath_tpl = cfg['install-lib.lib_src_dpath_tpl']
    lib_src_dpath = lib_src_dpath_tpl.format_map(params)
    lib_dst_dpath = params['lib_dpath']
    path.symlink_dir_items(lib_src_dpath, lib_dst_dpath, mk_relative=True)

    return params

def install_doc_step(params):
    '''
    Function for ``install-doc`` setup step using setup parameters `params`.

    :return: setup parameters after this step.

    :raise: a :py:exc:`duconfig.ConfigError` if there is something wrong with
            the configuration.
    :raise: a :py:exc:`SetupError` if running of a step command has failed.
    '''
    doc_dpath = pathlib.Path(params['doc_dpath'])
    doc_dpath.mkdir(parents=True, exist_ok=True)
    cfg = params['cfg']
    app_doc_src_dpath = cfg['install-doc.app_doc_src_dpath']
    app_doc_dst_dpath_tpl = cfg['install-doc.app_doc_dst_dpath_tpl']
    app_doc_dst_dpath = app_doc_dst_dpath_tpl.format_map(params)
    path.cpmv_dir(app_doc_src_dpath, app_doc_dst_dpath)
    step.run_command(params, 'install-doc.inst_lib_doc_cmd_tpl')
    params = calc_idris2_lib_dpath(params)
    lib_doc_src_dpath_tpl = cfg['install-doc.lib_doc_src_dpath_tpl']
    lib_doc_src_dpath = lib_doc_src_dpath_tpl.format_map(params)
    lib_doc_dst_dpath_tpl = cfg['install-doc.lib_doc_dst_dpath_tpl']
    lib_doc_dst_dpath = lib_doc_dst_dpath_tpl.format_map(params)
    path.cpmv_dir(lib_doc_src_dpath, lib_doc_dst_dpath, rm_src=True)

    return params
