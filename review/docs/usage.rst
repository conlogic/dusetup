..
 =============================================================================
 Title          : Utilities for setup tools

 Classification : reST text file

 Author         : Dirk Ullrich

 Date           : 2022-04-12

 Description    : Description of the user interface.
 =============================================================================


Tutorial: let us write a ``dusetup``-based setup tool
=====================================================


What we want to do
------------------

To get an impression how ``dusetup`` can be used to create setup tools, let us
implement one, step by step.  For the application to be set up we chose the
dependently typed functional programming language `Idris`_, actually its 2nd
version *Idris 2*. [#idris2]_  We start from the following simple Bash script
that implements the core functionality of *Idris 2* setup: [#st0]_

.. literalinclude:: tutorial/s0-bash/idris2_setup.bash
   :language: bash
   :linenos:

Let us re-implement the functionality of this script as a ``dusetup``-based
setup commandline tool for *Idris 2*.  We will put the code for this setup
tool into a Python distribution package ``idris2-setup`` that installs a
command wrapper script ``idris2_setup``.  The remaining code enters a package
``idris2Setup`` with subdirectory ``data`` for data files.  Actually,
``idris2Setup`` will contain at least: [#idris2Setup]_

+ the configuration file ``data/idris2-setup.conf`` for the setup tool;

+ a module :py:mod:`cmdline` doing commandline processing, together with
  handling the configuration and running of the setup steps.


Stage 1: a framework
--------------------

At stage 1 we provide a framework to run the setup scripts. [#st1]_

Before we start the real implementation of this stuff let us, for the sake of
completeness, just show the content configuration file ``setup.cfg`` for the
distribution package ``idris2-setup``.

Here comes ``setup.cfg`` for ``idris2-setup``:

.. literalinclude:: tutorial/s1-frame/setup.cfg
   :language: ini
   :linenos:

Please note that ``idris2-setup`` depends, of course, from this very package
``dusetup``, and that the data files like ``data/idris2-setup.conf`` are
included into the package.

And this is unspectacular content of the command wrapper script
``idris2_setup``:

.. literalinclude:: tutorial/s1-frame/idris2_setup
   :language: python
   :linenos:

Thus we expect that module :py:mod:`idris2Setup.cmdline` implements main
function :py:func:`main` to be called.  So let us now implement module
:py:mod:`idris2Setup.cmdline`, together with a configuration file
``idris2-setup.conf`` for the setup tool.  At stage 1 we just provide
skeletons for both of them.

We start with the configuration file:

.. literalinclude:: tutorial/s1-frame/idris2Setup/data/idris2-setup.conf
   :language: ini
   :linenos:

An this is the content of :py:mod:`idris2Setup.cmdline` for this stage:

.. literalinclude:: tutorial/s1-frame/idris2Setup/cmdline.py
   :language: python
   :linenos:

Even this skeletons provide some basic functionality of
``idris2-setup``. [#skel]_  For instance, the ``-h`` / ``--help`` option for
``idris2_setup`` should already work: [#help]_

.. code-block:: console

   >>> basename $(pwd)
   Idris2-0.5.1

   >>> /path/to/idris2-setup -h
   usage: idris2_setup [-h] [-V] [-v <app_vers>] [-p <prefix_dpath>]
                       [-s <steps>] [-c <cfg_fpath>] [-C [<cfg_fpath>]] [-d]

   Set up Idris 2 application from source.

   options:
     -h, --help            show this help message and exit
     -V, --version         show program's version number and exit
     -v <app_vers>, --appl-version <app_vers>
                           Use version <app_vers> for the application (default:
                           `0.5.1`). An empty value is treated as no version.
     -p <prefix_dpath>, --prefix <prefix_dpath>
                           Use prefix with directory path <prefix_dpath>
                           (default: `/usr/local/idris2[-<app_vers>]`). The
                           suffix is only added for a not-`None` application
                           version <appl_vers>.
     -s <steps>, --steps <steps>
                           Use steps in comma-separated list <steps> to set up
                           the application (default: ``).
     -c <cfg_fpath>, --use-cfg-file <cfg_fpath>
                           Use configuration file with path <cfg_fpath> instead
                           of the default one at
                           `[...]/idris2Setup/data/idris2-setup.conf`
     -C [<cfg_fpath>], --export-cfg-file [<cfg_fpath>]
                           Export active configuration file as file with path
                           <cfg_fpath>, and exit. If <cfg_fpath> is not given,
                           user configuration file at
                           `/home/dirk/.config/idris2-setup.conf` is used.
     -d, --debug           Enable debugging.

Please note that ``idris2_setup`` should always be called from within a
directory containing the original *Idris 2* source code we want to use for
setup.  In our example we use version 0.5.1 of *Idris 2*.

Let us have a look at the building blocks in :py:mod:`idris2Setup.cmdline` and
the configuration in `idris2-setup.conf`` we have so far.

0. In the :py:func:`main` function in :py:mod:`idris2Setup.cmdline` (lines
   22-56) a context handler :py:class:`dusetup.handle_error` (line 26) is used
   for calling all the remaining code in :py:func:`main`.  This eases the
   exception-based error handling for the setup tool: if an exception is
   raised, and appropriate error message is printed, and the :py:attr:`rc`
   attribute of the context handler is set to an return code <> 0 (line 55).

1. By using function :py:func:`dusetup.read_config_file`, the active
   configuration file is read and stored in a special dictionary ``cfg_data``
   (lines 28-29).  The name and the default location of the configuration file
   are defined as the values of :py:const:`CFG_FNAME` and
   :py:const:`DATA_DPATH` in the parameter section (lines 10-16).  If the user
   has a configuration file with filename from :py:const:`CFG_FNAME` in the
   XDG configuration directory (this is ``~/.config`` by default), this
   configuration file is the active one.  Otherwise we fall back to the
   default on in the data directory with path from :py:const:`DATA_DPATH`.

2. The default parameters for the commandline options are calculated using
   function :py:func:`dusetup.calc_default_setup_param_vals` (lines 31-32).
   These are read off section  ``default`` from the active configuration file
   (lines 5-19).  Concretely, the following have fixed meanings:

   + Parameter ``app_name`` for the application name - used as part of the
     installation prefix directory path;

   + Parameter ``steps`` for the list of setup steps - used for the default
     value of option ``-s`` / ``--steps``.

   + Parameter ``prefix_dpath_tpl`` for (the template of) the installation
     prefix directory path - used for the default value of option ``-p`` /
     ``--prefix``;

   + Parameter ``app_vers`` for the application version - default value for
     option ``-v`` / ``--app-version`` and as part of the installation prefix
     directory path.

   The calculated default values are stored in special dictionary
   ``default_params``.

3. Based on this dictionary ``default_params``, next an appropriate call of
   function :py:func:`dusetup.mk_cmdline_parser` commandline parser is created
   and returned as ``cmdl_parser`` (lines 34-37).  This uses module
   :py:mod:`argparse` from the Standard Library in the background.

4. The created commandline parser in ``cmdl_parser`` parses the commandline
   arguments and returns the result as ``args`` (lines 39-40).

5. If debugging is enabled (by commandline option ``-d`` / ``--debug``) the
   error handler will re-raise error exceptions and print their stack trace
   instead of only printing an error message (lines 42-47).  Please note that
   before this step debugging is always enabled to be able to analyze errors
   the occur early (for this the context handler
   :py:class:`dusetup.handle_error` is created in line 25 with argument
   ``raise_exception=True``).

6. Function :py:func:`dusetup.calc_setup_params` is used to calculate, from
   parsed commandline arguments ``args`` and configuration data ``cfg_data``,
   the used setup parameters.  The latter are stored in special dictionary
   ``params`` (lines 49-51).

7. Finally, this very ``params`` is used to run the setup steps by calling
   function :py:func:`dusetup.run_steps` (lines 53-54).  These setup steps
   will be implemented in module :py:mod:`idris2Setup.steps`.  For now this
   module exists, but do not contain any code.


Stage 2: start with setup parameters
------------------------------------

Before we start to configure and implement setup steps function we have to be
say more about the parameters used during setup.  We can distinguish the
following variants of parameters:

a. *Configuration parameters*: these are parameters that are directly read off
   from the configuration file(s) of the setup tool.  Such parameters are
   rather static, i.e. after they read from the configuration their value
   usually do not change anymore.

b. *Calculated parameters*: such parameters are calculated *outside of the
   configuration* before and during running the setup steps.  For instance, a
   bunch of initial setup parameters are calculated for the default
   configuration parameters and parameters given by commandline options (this
   is what function :py:func:`dusetup.calc_setup_params` is for).  Starting
   with this initial setup parameters, every setup step function can calculate
   further setup parameters that are passed to the following setup step
   function.  Such parameters are, by their very nature, rather dynamic.

Let us give some examples, from the stuff done at stage 1, for both variants
of parameters.

First of all, all parameters defined in section ``default`` of the
``idris2-setup.conf`` configuration file, are configuration parameters.  Let
us have a more detailed look at some of them:

+ Parameter ``steps`` (lines 11-12) is the list of the all possible setup
  steps in their proper order.  (This list is empty yet, but will be filled at
  stage 3 when we define the setup steps.).  In particular, the value of this
  parameter is a literal value, and therefore definitely a static one.

+ Parameter ``app_name`` (lines 8-9) gets its value by calling function
  :py:func:`calc_app_name` to calculate the name of the application we are
  about to set up from source.  By the way, this is a specific form of the
  so-called *value implementation*, i.e. to have special placeholders within
  parameter values in configuration files, that are replaced when the value of
  the respective parameter is accessed. [#intpol]_.  Parameter ``app_vers``
  (lines 18-19) is similar, but gets the application's version as value.
  Although both value are calculated (by the respective function calls), they
  are already calculated *inside of the configuration*. [#default]_.

+ Parameter ``prefix_dpath_tpl`` provides the *template* for the prefix
  directory path.  It is not the prefix directory path yet, since it contains
  a placeholder ``{app_name_vers}``.  To get the concrete prefix directory
  this placeholder has to be replaced by the combined application name and
  version.  Therefore ``prefix_dpath_tpl`` is a static configuration parameter
  (actually a literal one), but the prefix directory path will be a calculated
  parameter.  (We turn to it in a moment).

Next we turn to the calculated parameters we have yet, i.e. after stage 1.
These parameters are calculated by various functions called with the
:py:func:`idris2Setup.cmdline.main` function:

+ Parameters used for default value of commandline options.  They are
  calculated by calling function :py:func:`dusetup.calc_default_param_vals`
  from the read configuration values in ``cfg_data`` and they are stored in
  dictionary ``default_params`` (lines 31-32).

+ Initial setup step parameters.  These ones are calculated by function both
  from the read configuration values in ``cfg_data`` and the parsed
  commandline arguments in ``args`` by :py:func:`dusetup.calc_setup_params`
  and they are stored in dictionary ``params`` (lines 49-51).  One of these
  calculated parameters is the path for the prefix directory mentioned above.
  It is stored as ``params['prefix_dpath']``.

Next we take care of the remaining parameters from the ``Parameters`` section
of the Bash script we started from.  To relate our ``dusetup``-based
implementation to that script more easily, we will use the same parameter
names, but in small case letters.  First of all, function
:py:func:`dusetup.calc_setup_params` already takes care of the parameters
``app_name``, ``app_vers``, ``app_name_vers`` and ``prefix_dpath`` and stores
them in the returned dictionary ``params`` of the initial setup step
parameters.  Since the remaining parameters all depend on calculated
parameters, they become itself calculated parameters.  We could handle them
similar to the prefix directory path: put, for every parameter, an appropriate
template parameter in the configuration file.  Then calculate the real
parameter value in an enhanced version of
:py:func:`dusetup.calc_setup_params`, and store the result in setup step
parameter dictionary, too.  But for all remaining parameters but
``boot_dpath`` such a template is necessary because module
:py:mod:`dusetup.install` has already functions to calculate these parameter
values.  We are free to choose a section for the ``boot_dpath`` parameter
template ``rel_boot_dpath_tpl`` - let us choose a new section ``params`` for
this. [#rel_boot_dpath]_ Thus the configuration file ``idris2-setup.conf``
from stage 1 is enhanced in the following way [#st2]_:

.. literalinclude:: tutorial/s2-params/idris2Setup/data/idris2-setup.conf
   :language: ini
   :lines: 18-
   :linenos:
   :lineno-start: 18
   :emphasize-lines: 4-9

To complete stage 2 we have to enhance :py:func:`dusetup.calc_setup_params`.
We put our version of this function in the :py:mod:`idris2Setup.cmdline`
module.  Please note that we let our py:func:`calc_setup_params` also take
care to extend the values of the environment variables ``PATH`` and
``LD_LIBRARY_PATH``, similar to the original Bash script (lines 33-38).  This
is what function :py:func:`add_path_envvar` from module
:py:mod:`dusetup.config` is for.  Therefore we add to
:py:mod:`idris2Setup.cmdline`:

.. literalinclude:: tutorial/s2-params/idris2Setup/cmdline.py
   :language: python
   :lines: 53-
   :linenos:
   :lineno-start: 53
   :emphasize-lines: 6-

Finally we have to modify the :py:func:`main` function to use our variant of
:py:func:`calc_setup_params`:

.. literalinclude:: tutorial/s2-params/idris2Setup/cmdline.py
   :language: python
   :lines: 49-54
   :linenos:
   :lineno-start: 49
   :emphasize-lines: 3


Stage 3: set up setup steps 1 (``bootstrap`` and ``build``)
-----------------------------------------------------------

Now we turn to implement the setup steps needed for *Idris 2*.  At this stage
we will take care of the first two, ``bootstrap`` and ``build``. [#st3]_

Do implement a setup step, one have to at least the following things to do:

+ Add the name of the step, say ``<step>``, as element to the list value for
  parameter ``steps`` in section ``default`` of the configuration file of the
  setup tool.

+ Add the name for the function that should called to do step ``<step>`` as
  value for parameter ``<step>`` in section ``steps`` of the configuration
  file.

+ Implement the step function for ``<step>`` as named above in the module
  :py:mod:`idris2Setup.steps`.

Additionally to this required parameters, we use a special section ``<step>``
to hold additional parameters needed by the setup function for ``<step>``.

To ease comparison with the original Bash script from stage 0, we will use
exactly the same setup steps like the Bash script.  Moreover we will name our
step functions like the corresponding functions in the Bash script.

Let us start to implement the ``bootstrap`` step.  According to the first 2
steps to implement a setup step, we therefore enhance the configuration file
from stage 1 by adding ``bootstrap`` as element to parameter ``steps`` in
section ``defaults``, and add parameter ``bootstrap`` with value
``bootstrap_step`` to section ``steps``.  Since the configuration parameter
``rel_boot_dpath_tpl`` (put in section ``params`` during stage 2) is specific
for the ``bootstrap`` step, we migrate it to the special parameter section for
``bootstrap``.  After this we can trash section ``params``.  These changes
produces the following new version of the ``idris2-setup.conf`` configuration
file:

.. literalinclude:: tutorial/s3-steps1/idris2Setup/data/idris2-setup_v1.conf
   :language: ini
   :lines: -34
   :linenos:
   :emphasize-lines: 12-14, 26-27, 29-

We should not forget to adapt our :py:func:`calc_setup_params` in
:py:mod:`idris2Setup.cmdline` to use the new location of
``rel_boot_dpath_tpl``:

.. literalinclude:: tutorial/s3-steps1/idris2Setup/cmdline.py
   :language: python
   :lines: 79-83
   :linenos:
   :lineno-start: 79
   :emphasize-lines: 3

It remains to implement the setup step function :py:func:`bootstrap_step` for
the ``bootstrap`` step.  Please remember that we have to put its definition in
the :py:mod:`idris2Setup.steps` module.  So let us reproduce the instructions
from the ``bootstrap_step`` function in the Bash scrip but ignore producing
the messages using ``echo``.  So it remains to issue 3 commands.  This can
easily be done by using function :py:func:`dusetup.step.run_commands`: it
takes the setup parameter dictionary and a configuration parameter name
(qualified by its configuration section) as arguments, expecting that the
value of the latter parameter is a list of commands to run in a Shell.
Actually, each of this commands can be a template containing placeholders for
setup parameters, and these placeholders will be automatically replaced by the
values of the respective setup parameters before running the respective
command.

Therefore configuration section gets a new parameter ``cmds_tpls`` whose value
is the list of the (shell) command templates needed for the ``bootstrap``
step.  Compared to the ``bootstrap_step`` function in the Bash script, we add
a small improvement: we make the value of the ``SCHEME`` parameter in the 2nd
``make`` command a configuration parameter, too - say ``scheme`` in the
``bootstrap`` section.  Then we use value interpolation within the respective
``make`` command template in ``cmds_tpls`` to refer to this configuration
parameter. [#item_intp]_ Putting all this together, the ``bootstrap`` section
of ``idris2-setup.conf`` now looks like:

.. literalinclude:: tutorial/s3-steps1/idris2Setup/data/idris2-setup_v1.conf
   :language: ini
   :lines: 29-
   :linenos:
   :lineno-start: 29
   :emphasize-lines: 8-

To complete the implementation for the ``bootstrap`` step, it remains to put
the definition of the :py:func:`bootstrap_step` function in the
:py:mod:`idris2Setup.steps` module.  This is pretty simple, thanks to function
:py:func:`dusetup.step.run_commands` [#ret_params]_:

.. literalinclude:: tutorial/s3-steps1/idris2Setup/steps.py
   :language: python
   :lines: -19
   :linenos:

Now the implementation of the ``build`` setup step does not provide extra
challenges, since the corresponding function ``build_step`` in the original
Bash script has, beside some messaging, again only some straightforward
instructions to be done in the shell.  Therefore we can use the same strategy
like for ``bootstrap``.

First we register the ``build`` step in the ``defaults`` section of
``idris2-setup.conf``:

.. literalinclude:: tutorial/s3-steps1/idris2Setup/data/idris2-setup_v2.conf
   :language: ini
   :lines: 5-15
   :linenos:
   :lineno-start: 5
   :emphasize-lines: 10

Next we register a new setup step function :py:func:`build_step` in the
``steps`` section:

.. literalinclude:: tutorial/s3-steps1/idris2Setup/data/idris2-setup_v2.conf
   :language: ini
   :lines: 24-32
   :linenos:
   :lineno-start: 24
   :emphasize-lines: 7-8

We again will run the required Shell commands by function
:py:func:`dusetup.step.run_commands`.  Therefore we define list of command
(templates) within a dedicated ``build`` section in the configuration:

.. literalinclude:: tutorial/s3-steps1/idris2Setup/data/idris2-setup_v2.conf
   :language: ini
   :lines: 51-60
   :linenos:
   :lineno-start: 51

Finally, the setup step function :py:func:`idris2Setup.steps.build_step` for
``build`` is similar to the ``bootstrap`` step function - we have only to
choose the right configuration parameter for the list of command templates:

.. literalinclude:: tutorial/s3-steps1/idris2Setup/steps.py
   :language: python
   :lines: 21-
   :linenos:


Stage 4: set up setup steps 2 (``install``, three times)
--------------------------------------------------------

For a complete ``dusetup``-based setup tool for `Idris 2` it remains to
reproduce the ``install-*`` setup steps, namely ``install-app``,
``install-lib`` and ``install-doc``. [#st4]_

We start with the ``install-app`` step.  First the usual changes in the
``idris2-setup.conf`` configuration file to register the ``install-app`` setup
step:

.. literalinclude:: tutorial/s4-steps2/idris2Setup/data/idris2-setup_v1.conf
   :language: ini
   :lines: 5-36
   :linenos:
   :lineno-start: 5
   :emphasize-lines: 11,30-31

This time we do, for the ``install-app`` setup step function
:py:func:`idris2Setup.steps.install_app_step`, not reproduce any essential
(i.e. non-``echo ...``) instruction as a shell command template, but use
``dusetup`` functions for some of them, since it is more powerful or easier
this way.  For instance, module :py:mod:`dusetup.step` has a function
:py:func:`ensure_empty_destination` that does not only empty an existing
prefix directory but also creates it if it does not already exist.  Therefore
a call of this function is a better alternative to, say, ``rm -rf
{prefix_dpath}/*``.  And the creation of the relative symlink for ``idris2``
command in the binary directory (lines 80-86 of the original Bash script) can
much more simple be done by using function :py:func:`symlink_item` from the
:py:mod:`dusetup.path` module.  For the source path of this symlink let us use
a path template in the ``idris2-setup.conf`` configuration file [#abs_path]_:

.. literalinclude:: tutorial/s4-steps2/idris2Setup/data/idris2-setup_v1.conf
   :language: ini
   :lines: 66-71
   :linenos:
   :lineno-start: 66
   :emphasize-lines: 4-

The two ``make ...`` instructions (lines 75-78 in the Bash scrip) we port,
like for ``bootstrap`` and ``build``, with the help of function
:py:func:`dusetup.step.run_commands`.  For this use, as usual, a list
parameter ``cmds_tpls`` in the ``install-app`` section:

.. literalinclude:: tutorial/s4-steps2/idris2Setup/data/idris2-setup_v1.conf
   :language: ini
   :lines: 66-78
   :linenos:
   :lineno-start: 66
   :emphasize-lines: 8-

Following the heuristics sketched above the setup step function
:py:func:`idris2Setup.steps.install_app_step` looks like:

.. literalinclude:: tutorial/s4-steps2/idris2Setup/steps_v1.py
   :language: python
   :lines: 35-53
   :linenos:
   :lineno-start: 35

By the way, the part of :py:func:`idris2Setup.steps.install_app_step`'s
definition that deals with creating the ``idris2`` command link (lines
47-51) demonstrates how to deal with configuration parameters whose values are
templates, i.e. have placeholders for setup parameter values: one has to
extract the configuration from the setup parameter dictionary (line 47), get
the parameter value from configuration (line 48), and replace the setup
parameter placeholder by the respective parameter values (line 49).  You
usually do not have to do this if, for the needed functionality, an
appropriate function in :py:mod:`dusetup.step` exists since most of these
functions do such stuff itself.

Since function :py:func:`symlink_item` comes from the :py:mod:`dusetup.path`
module we should not forget to import this module in :py:mod:`dusetup.step`:

.. literalinclude:: tutorial/s4-steps2/idris2Setup/steps_v1.py
   :language: python
   :lines: -5
   :linenos:
   :emphasize-lines: 5

For ``install-lib``, i.e. the next setup step, we can reuse many of our
previous ideas.  First, the business as usual to register the new setup step
in the ``idris2-setup.conf`` configuration file:

.. literalinclude:: tutorial/s4-steps2/idris2Setup/data/idris2-setup_v2.conf
   :language: ini
   :lines: 11-39
   :linenos:
   :lineno-start: 11
   :emphasize-lines: 6,28-29

But there is a new aspect not covered before: in the original
Bash script a command is run, and is output (to STDOUT) is used as a parameter
``inst_lib_dpath`` later:

.. literalinclude:: tutorial/s0-bash/idris2_setup.bash
   :language: bash
   :lines: 91-114
   :linenos:
   :lineno-start: 91
   :emphasize-lines: 14

Exactly the same happens again, in the ``install_doc_step`` function:

.. literalinclude:: tutorial/s0-bash/idris2_setup.bash
   :language: bash
   :lines: 116-128
   :linenos:
   :lineno-start: 116
   :emphasize-lines: 9

A way to pass a parameter value produced by a command to later components of a
setup step is to store it, like the initial setup parameters, in the setup
parameter dictionary. [#late_param]_  Since the calculated parameter is needed
twice, namely both for the ``install-lib`` and the ``install-doc`` step, let
us create a function, say :py:func:`calc_idris2_lib_dpath` that checks whether
the parameter is already stored in the parameter dictionary, and, if not, run
the respective command and store it then.  For flexibility, we configure the
command to run to get the parameter as command template in the
``idris2-setup.conf`` configuration file:

.. literalinclude:: tutorial/s4-steps2/idris2Setup/data/idris2-setup_v2.conf
   :language: ini
   :lines: 84-88
   :linenos:
   :lineno-start: 84
   :emphasize-lines: 4-5

We put the definition of :py:func:`calc_idris2_lib_dpath` in the
:py:mod:`dusetup.step` module:

.. literalinclude:: tutorial/s4-steps2/idris2Setup/steps_v2.py
   :language: python
   :lines: 58-78
   :linenos:
   :lineno-start: 58

and, of course, import the additional modules we need for the definition of
that function from the Standard Library:

.. literalinclude:: tutorial/s4-steps2/idris2Setup/steps_v2.py
   :language: python
   :lines: -8
   :linenos:
   :emphasize-lines: 5-6

Based on this the implementation of setup step function
:py:func:`install_lib_step` is rather straightforward.  For the first ``make
...`` instruction (lines 94-95 in the Bash script) we configure an command
template, and feed it to :py:func:`run_command`: [#line_cont]_

.. literalinclude:: tutorial/s4-steps2/idris2Setup/data/idris2-setup_v2.conf
   :language: ini
   :lines: 84-92
   :linenos:
   :lineno-start: 84
   :emphasize-lines: 7-9

.. literalinclude:: tutorial/s4-steps2/idris2Setup/steps_v2.py
   :language: python
   :lines: 80-90
   :linenos:
   :lineno-start: 80
   :emphasize-lines: 11

Then the directory removal (line 97 in the Bash script) is done by function
:py:func:`rm_items` from :py:mod:`dusetup.step`:

.. literalinclude:: tutorial/s4-steps2/idris2Setup/data/idris2-setup_v2.conf
   :language: ini
   :lines: 84-95
   :linenos:
   :lineno-start: 84
   :emphasize-lines: 11-12

.. literalinclude:: tutorial/s4-steps2/idris2Setup/steps_v2.py
   :language: python
   :lines: 80-91
   :linenos:
   :lineno-start: 80
   :emphasize-lines: 12

The second ``make ...`` instruction (lines 100-101 in the Bash script) is
similarly ported like the first one above:

Then the directory removal (line 97 in the Bash script) is done by function
:py:func:`rm_items` from :py:mod:`dusetup.step`:

.. literalinclude:: tutorial/s4-steps2/idris2Setup/data/idris2-setup_v2.conf
   :language: ini
   :lines: 84-99
   :linenos:
   :lineno-start: 84
   :emphasize-lines: 14-16

.. literalinclude:: tutorial/s4-steps2/idris2Setup/steps_v2.py
   :language: python
   :lines: 80-92
   :linenos:
   :lineno-start: 80
   :emphasize-lines: 13

Finally, to link the libraries (lines 104-111 in the Bash script) we first
calculate the new setup parameter ``inst_lib_dpath`` first as described
above.  Then we use this parameter a with configured path template to
determine path ``lib_src_dpath`` for the directory, where the libraries live
that we want to symlink.  With this directory, function
:py:func:`symlink_dir_items` can create the links:

.. literalinclude:: tutorial/s4-steps2/idris2Setup/data/idris2-setup_v2.conf
   :language: ini
   :lines: 84-102
   :linenos:
   :lineno-start: 84
   :emphasize-lines: 18-19

.. literalinclude:: tutorial/s4-steps2/idris2Setup/steps_v2.py
   :language: python
   :lines: 80-100
   :linenos:
   :lineno-start: 80
   :emphasize-lines: 14-19

The implementation of the last setup step ``install-doc`` is start, as usual,
by adding it tot the ``idris2-setup.conf`` configuration file:

.. literalinclude:: tutorial/s4-steps2/idris2Setup/data/idris2-setup_v3.conf
   :language: ini
   :lines: 11-43
   :linenos:
   :lineno-start: 11
   :emphasize-lines: 7,32-33

The implementation of the setup step function :py:func:`install_doc_step` for
step ``install-doc`` in module :py:mod:`idris2Setup.steps` offers no
surprises.  First, the creation of the documentation directory (line 118 in
the Bash script) can be done, for instance, by good old
:py:class:`pathlib.Path` method :py:meth:`mkdir`: [#alt_impl]_

.. literalinclude:: tutorial/s4-steps2/idris2Setup/steps_v2.py
   :language: python
   :lines: 102-113
   :linenos:
   :lineno-start: 102
   :emphasize-lines: 11-12

To install the application documentation (line 120 in the Bash script) we
define configuration parameters for source and destination paths, and let
function :py:func:`cpmv_dir` from :py:mod:`dusetup.path` do the work:

.. literalinclude:: tutorial/s4-steps2/idris2Setup/data/idris2-setup_v3.conf
   :language: ini
   :lines: 108-116
   :linenos:
   :lineno-start: 108
   :emphasize-lines: 4-5,7-9

.. literalinclude:: tutorial/s4-steps2/idris2Setup/steps_v2.py
   :language: python
   :lines: 102-118
   :linenos:
   :lineno-start: 102
   :emphasize-lines: 13-17

For the ``make ...`` instruction to install the library documentation (line
123 in the Bash script) a Bash command defined by a template in the
configuration file is used:

.. literalinclude:: tutorial/s4-steps2/idris2Setup/data/idris2-setup_v3.conf
   :language: ini
   :lines: 108-119
   :linenos:
   :lineno-start: 108
   :emphasize-lines: 11-12

.. literalinclude:: tutorial/s4-steps2/idris2Setup/steps_v2.py
   :language: python
   :lines: 102-119
   :linenos:
   :lineno-start: 102
   :emphasize-lines: 18

For the last part of ``install-doc`` (lines 124-125 in the Bash script) the
calculated setup parameter ``inst_lib_dpath`` is (re-)used to fill the path
template for the library documentation source directory path, while the
template for the its destination directory path only needs initial setup
parameters.  Moving the source directory to the destination directory is again
delegated to function :py:func:`cpmv_dir`:

.. literalinclude:: tutorial/s4-steps2/idris2Setup/data/idris2-setup_v3.conf
   :language: ini
   :lines: 108-125
   :linenos:
   :lineno-start: 108
   :emphasize-lines: 14-15,17-18

.. literalinclude:: tutorial/s4-steps2/idris2Setup/steps_v2.py
   :language: python
   :lines: 102-125
   :linenos:
   :lineno-start: 102
   :emphasize-lines: 19-24

Now we have re-implemented the original Bash script as a ``dusetup``-based
setup tool for *Idris 2*.


.. [#idris2] Idris 2 is interesting for several reasons.

             Firstly, there is its expressive power that results from being a
             dependently typed functional programming language.  Typical for
             modern statically typed functional programming languages, like
             `Haskell`_, is their powerful type system that allows to express
             many expected properties of the code and to check them already at
             compile time.  The availability of dependent types takes this a
             big step further to allow to encode even whole formal
             specifications solely by appropriate types.

             Secondly, another interesting features of *Idris 2* is that it is
             self-hosting, i.e. completely implemented in itself.  For details
             you can consult the `Idris 2 GitHub`_ repository and the
             corresponding `Idris 2 Wiki`_.

.. [#st0] This very Bash script lives in subdirectory ``tutorial/s0-bash`` of
          this documentation.

.. [#idris2Setup] Beside the stuff mentioned above, ``idris2Setup`` contains,
                  like every Python package, an initialization module
                  :py:mod:`__init__`.  For our purpose this module can remain
                  empty.

.. [#st1] The content of the whole ``idris2-setup`` distribution package as
          resulting from this stage 1 you can find in subdirectory
          ``tutorial/s1-frame`` of this documentation.

.. [#skel] Please note that the content of the ``tutorial/s1-frame``
           subdirectory can be used as template for other ``dusetup``-based
           setup tools.  For this you have only to make some minor changes:
           adapt some package and module names, and the description string for
           the commandline parser in :py:mod:`cmdline`, and, maybe, change
           some default parameter values in the configuration file to fit your
           taste.

.. [#help] For simplicity, the path for the default configuration file is
           shortened since its full path is irrelevant here.

.. [#st2] Subdirectory ``tutorial/st2-params`` contains the content of the
          whole ``idris2-setup`` distribution package for stage 2.

.. [#intpol] The capability for value interpolation with configuration values
             comes from package ``duconfig``.  Here we a specif form of value
             interpolation - the so-called *function call interpolation*.  It
             uses placeholders for (a form of) function calls that are
             delimited by ``%(...)%``.  For details please have a look into
             the `duconfig repository`_ or `duconfig documentation`_.

.. [#default] Actually, the calculation of these values is done when they are
              accessed in function :py:func:`dusetup.read_config_file` used to
              read the ``idris2-setup.conf`` configuration file.

.. [#rel_boot_dpath] We use a template for the relative boot directory path,
                     and make the path absolute during calculation.

.. [#st3] Code for this very stage lives in subdirectory
          ``tutorial/st3-steps1``.

.. [#item_intp] Actually, this is another variant of value interpolation
                inherited from ``duconfig``, namely the so-called *item
                interpolation*.  It uses placeholders delimited by
                ``%{...}%``, and refers to the value of the parameter whose
                full name is contained in such a placeholder.

.. [#ret_params] Please remember that every setup step function must return
                 the setup parameter dictionary for the next setup step - even
                 if the function, like :py:func:`bootstrap_step`, does not
                 change these setup parameters.

.. [#st4] The code for this stage 4 can be found in subdirectory
          ``tutorial/st4-steps2``.

.. [#abs_path] Other than in the Shell script, the source path of a symlink to
               be created by :py:func:`symlink_item` can be absolute, even if
               we want a relative link.  Function :py:func:`symlink_item`
               automatically calculates the correct variant of the target path
               used by the symlink.

.. [#late_param] Please note that we cannot already calculate this setup
                 parameter before running any setup step, i.e. in our function
                 :py:func:`calc_setup_params` in the
                 :py:mod:`idris2Setup.cmdline` module because the ``idris2``
                 binary needed is available not until setup step
                 ``install-app`` is completed.

.. [#line_cont] The ``\`` is used here continue a line on the next line.
                Please note that all leading whitespace of the continuing line
                is ignored.

.. [#alt_impl] For this very step - and the next one - one could also run
               appropriate shell commands that are similar to their
               counterparts in the Bash script.  But for variety reasons, we
               have chosen alternatives not yet used in the tutorial before.


.. _Idris: https://www.idris-lang.org

.. _Haskell: https://www.haskell.org

.. _Idris 2 Github: https://github.com/idris-lang/Idris2

.. _Idris 2 Wiki: https://github.com/idris-lang/Idris2/wiki

.. _duconfig repository: https://gitlab.com/conlogic/duconfig

.. _duconfig documentation: https://duconfig.readthedocs.io


..
 -----------------------------------------------------------------------------
 Local Variables:
 mode: rst
 ispell-local-dictionary: "en"
 End:
