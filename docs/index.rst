..
 =============================================================================
 Title          : Setup utilities

 Classification : reST text file

 Author         : Dirk Ullrich

 Date           : 2022-05-30

 Description    : Master file for the documentation of distribution package.
 =============================================================================


=============================
Documentation for ``dusetup``
=============================

.. toctree::
   :maxdepth: 2

   overview
   usage
   api
   old_info


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`


..
 -----------------------------------------------------------------------------
 Local Variables:
 mode: rst
 ispell-local-dictionary: "en"
 End:
