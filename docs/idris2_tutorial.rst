..
 =============================================================================
 Title          : Setup utilities

 Classification : reST text file

 Author         : Dirk Ullrich

 Date           : 2023-05-04

 Description    : How to write a ``dusetup``-based Idris 2 setup.
 =============================================================================


============================================================================
Tutorial: let us configure and enhance ``dusetup`` as setup tool (*Idris 2*)
============================================================================


What we want to do
==================

This tutorial tries to show how ``dusetup`` itself can be used as commandline
setup tool for an application by writing an appropriate setup configuration
file *and* additional Python code to be used by ``dusetup`` to set up that
application.  Thus we have again to create an appropriate setup configuration
file to perform the setup stages needed for the application we want to set up.
This aspect we are already familiar with since it was described in
:ref:`git_tut`, and we will reuse its wisdom here as much as possible.  But
there is also a new aspect: we have to provide (at least) a Python module or
package that contains our additional code needed for the application's setup.
For this tutorial we choose to reproduce the two files of our *Idris 2* setup
example, step by step, from scratch.  Actually, this example sets up *Idris 2*
in a way that satisfies the *Filesystem Hierarchy Standard* [#FHS]_ for UNIX
systems (*FHS* for short).

We start from the following simple Bash script, say :file:`idris2_setup.bash`,
that implements the core functionality of *Idris 2* setup such that it is
compliant to the *FHS* [#idr2_bash]_:

.. literalinclude:: idris2/idris2_setup.bash
   :language: bash
   :linenos:


.. _idr2-why_own_code:

Why do we want own code for the *Idris 2* setup?
================================================

One could ask why a tailored setup configuration file does not suffice for an
*Idris 2* setup that uses ``dusetup`` as commandline tool.  To put it
differently: whose features of the *Idris 2* setup are to such an extend
different from the *Git* setup explained in section :ref:`git_tut` that we
should need own Python code beside an appropriate setup configuration file?
A first attempt could try to reuse the strategy from :ref:`git_tut`: port, for
every setup stage, its shell commands in :file:`idris2_setup.bash`, and then
use either setup step function :py:func:`dusetup.stage.cmd.run_cmd_from_cfg`
or :py:func:`dusetup.stage.cmd.run_cmds_from_cfg` to run them.  But then we
face at least the following task: how to deal with many of the parameters
represented as environment or shell variables in the :file:`idris2_setup.bash`
script?

First, some of them, like :envvar:`REL_DOCS_DPATH` or
:envvar:`REL_DOCS_STATIC_DPATH`, are static parameters (in the sense of point
a in section :ref:`git-write_conf`):

.. literalinclude:: idris2/idris2_setup.bash
   :language: bash
   :linenos:
   :lineno-match:
   :start-at: # Relative path for Sphinx documentation directory.
   :end-before: # Binary directory path of installed Idris 2.
   :emphasize-lines: 2,5

Hence they could easily implemented as setup configuration parameters.

Second, many others depend on calculated parameters (in the sense of point b
from  section :ref:`git-write_conf`).  For instance, :envvar:`FHS_CMD_FPATH`
depends on the values of :envvar:`BIN_DPATH` and :envvar:`APP_NAME_VERS`:

.. literalinclude:: idris2/idris2_setup.bash
   :language: bash
   :linenos:
   :lineno-match:
   :start-at: # FHS command file path of installed Idris 2.
   :end-before: # Raw command file path of installed Idris 2.
   :emphasize-lines: 2

Both :envvar:`BIN_DPATH` and :envvar:`APP_NAME_VERS` correspond to standard
initial setup parameters.  Thus their calculation poses no new problem, and
can handled like described in :ref:`git_tut` for the *Git* setup example: add
an appropriate template for the respective parameter, and let setup step
functions calculating the value of the parameter from its template when needed.

Thirdly, there is a new aspect related to the environment or shell variables
that is *no* problem.  The value of some standard environment variables must
be enhanced by some of our calculated setup parameters, namely :envvar:`PATH`
and :envvar:`LD_LIBRARY_PATH`:

.. literalinclude:: idris2/idris2_setup.bash
   :language: bash
   :linenos:
   :lineno-match:
   :start-at: # Add bootstrapped bin directory path to the value of `PATH`.
   :end-before: # Scheme backend to be used.
   :emphasize-lines: 3,6

For this functionality ``dusetup`` provides setup step function
:py:func:`dusetup.step.path.add_path_envvar`.  Please consult the API
documentation in :ref:`API-ref` for details.

Last but not least there is a aspect related to the environment or shell
variables in :file:`idris2_setup.bash` that *is* a problem:  There remain
calculated parameters -- like ``raw_lib_dpath`` within the functions
``install_lib_step`` and ``linstall_doc_step``:

.. literalinclude:: idris2/idris2_setup.bash
   :language: bash
   :linenos:
   :lineno-match:
   :start-at: install_lib_step() {
   :end-before: # Doing
   :emphasize-lines: 16,38

Please notice that the value of this setup parameter depends on a call of the
*Idris 2* binary.  Moreover, that very *Idris 2* binary hat to be built and
installed in setup stages performed before.  Thus this setup parameter cannot
be calculated by standard ``dusetup`` helpers or setup step functions.
Therefore we have at least one good reason for the need of own Python code for
the *Idris 2* setup by ``dusetup``.


Write ``dusetup`` supplements for *Idris 2*
===========================================

So it is our task now to write a setup configuration file together with own
Python code addons to be used with ``dusetup`` as commandline tool for a
*Idris 2* setup.  Since we are about to reproduce the *Idris 2* example, we
choose the same file names: the ``dusetup`` configuration file will become
:file:`idris2-setup.conf`, and our own Python code is put in Python module
file :file:`idris2_setup.py`.  To ease the import of the latter by
``dusetup``, we store both files in the same directory.

To ease comparison with the Bash script :file:`idris2_setup.bash` we start
from we choose the same setup stage names.  Furthermore as far as useful, the
same names for parameters that represent environment or other shell variables
are used - but always in lowercase.  Other than in :file:`idris2_setup.bash`
we do not collect parameters that replace environment variables of that script
in a ``Parameters`` section.  Instead we usually put such parameters, like for
:file:`git-setup.conf` in the *Git* setup tutorial above, in the section for
that setup stage section that needs the respective parameter for the first
time.

Last but not least we do not intend to repeat much of the information already
given in the *Git* setup tutorial in section :ref:`git_tut`.  Whenever
possible pointers to its information will be used instead.


-----------------------------
Parameters for default values
-----------------------------

Like for *Git* setup in section :ref:`git-default_params` we use the
parameters in a section named ``default`` to define default values for some
initial *Idris 2* setup parameters.  Actually, these parameters are ``stages``
with the list of the names of all setup stages as its value, and
``prefix_dpath_tpl`` with a template for the prefix directory's path as value.
Inspired by the :file:`idris2_setup.bash` script we use the following setup
stages for *Idris 2*:

.. literalinclude:: ../examples/idris2/idris2-setup.conf
   :language: ini
   :linenos:
   :lineno-match:
   :start-at: # Default values some initial setup parameters.
   :end-before: # Path template for default installation prefix directory.
   :emphasize-lines: 4-12

As prefix installation directory we use one similar to the *Git* setup that is
suitable for experimentation:

.. literalinclude:: ../examples/idris2/idris2-setup.conf
   :language: ini
   :linenos:
   :lineno-match:
   :start-at: # Default values some initial setup parameters.
   :end-before: # Definitions of the setup stages.
   :emphasize-lines: 14-17

Please remember that the ``{app_name_vers}`` part of the ``prefix_dpath_tpl``
value is a placeholder for a setup parameter - for ``app_name_vers``, and
that's why parameter ``prefix_dpath_tpl`` is called *a template* for the
installation prefix directory path.  Most setup step functions get the real
value of such a template by replacing all these placeholders for setup
parameters by their values.


-----------------------------------------------------------
Enhance some search paths (using dummy setup stage ``pre``)
-----------------------------------------------------------

As mentioned in section :ref:`idr2-why_own_code` we have to add paths to the
values of the environment variables :envvar:`PATH` and
:envvar:`LD_LIBRARY_PATH`.  Actually this has to be done before any setup
stage but ``bootstrap`` is performed.  This is perfect use case for the dummy
setup stage ``pre``.  Setup stage ``pre`` is optional, i.e. it does not to be
mentioned in the ``stages`` parameter in setup configuration section
``default``; but if there is a definition of setup steps for it in setup
configuration section ``stages``, these steps are performed before any other
setup stage. [#post_stage]_  Using ``pre`` we can realize the way to enhance
:envvar:`PATH` and :envvar:`LD_LIBRARY_PATH` as sketched in section
:ref:`idr2-why_own_code`.

Let us begin with configuring the setup stage function calls for ``pre``,
i.e. defining parameter ``pre`` in setup configuration section ``stages``.
Section :ref:`idr2-why_own_code` has mentioned that setup step function
:py:func:`dusetup.step.path.add_path_envvar` is meant to enlarge the value a
search path variable.  We need two calls of this function - one to enlarge the
value of :envvar:`PATH`, and one for :envvar:`LD_LIBRARY_PATH`.  Each of these
calls requires the directory path to be added as positional parameter.  Let us
note that the paths we have to add to the values of :envvar:`PATH` and
:envvar:`LD_LIBRARY_PATH` has to be calculated since both depend on parameter
``boot_dpath`` that in turn depends on standard setup parameter
``app_name_vers``.  Only the latter one is a standard setup parameter while
the other three calculated parameters are not.  So we will use the same
strategy like in :ref:`git_tut` for this kind of parameters:  for every of
these parameters we define a corresponding setup configuration parameter that
is a template for this parameter - using a dedicated setup configuration
section named ``pre`` for it.  Let us start with the template parameter for
``boot_dpath`` [#func_intp2]_:

.. literalinclude:: ../examples/idris2/idris2-setup.conf
   :language: ini
   :linenos:
   :lineno-match:
   :start-at: # Parameters for the `pre` stage.
   :end-before: # Template for the path of the bootstrap `bin` directory.
   :emphasize-lines: 4-5

Based on this the parameter templates for the two paths that will be added can
be configured [#item_intp2]_:

.. literalinclude:: ../examples/idris2/idris2-setup.conf
   :language: ini
   :linenos:
   :lineno-match:
   :start-at: # Parameters for the `pre` stage.
   :end-before: # Parameters for the `bootstrap` stage.
   :emphasize-lines: 7-8,10-11

The configuration of the ``pre`` setup stage is completed by defining setup
stage ``pre`` doing the two calls of setup step function
:py:func:`dusetup.step.path.add_path_envvar` as planned above [#beg_path]_:

.. literalinclude:: ../examples/idris2/idris2-setup.conf
   :language: ini
   :linenos:
   :lineno-match:
   :start-at: # Definitions of the setup stages.
   :end-before: # Definition of the `bootstrap` stage.
   :emphasize-lines: 4-9


.. _idr2-clean_stage:

-----------------------------
Configure the ``clean`` stage
-----------------------------

Let us turn to our first 'real' stage, namely the ``clean`` stage.  It
provides no new challenges: we could more or less reproduce, like for most
setup stages of the *Git* example, the relevant shell commands.  For the
``clean`` stage these are commands in the ``clean_step`` shell function in the
:file:`idris2_setup.bash` script.  We do exactly this for the ``make
distclean`` command in this shell function, but instead of the ``rm -rf ...``
command we use setup step function :py:func:`dusetup.step.path.rm_item`.

For the ``make clean`` substitute we need a parameter whose value holds the
corresponding command.  We will configure it in a section named ``clean`` in a
moment; but let us add the call to setup step function
:py:func:`dusetup.steps.run_cmd_from_cfg` first that will run this command:

.. literalinclude:: ../examples/idris2/idris2-setup.conf
   :language: ini
   :linenos:
   :lineno-match:
   :start-at: # Definitions of the setup stages.
   :end-before: # Definition of the `bootstrap` stage.
   :emphasize-lines: 11,13

The translation of ``rm -rf ...`` is easy.  The path (template, actually) for
the directory to be removed was already defined for ``pre``; therefore our
:py:func:`dusetup.step.path.rm_item` does not need new parameters:

.. literalinclude:: ../examples/idris2/idris2-setup.conf
   :language: ini
   :linenos:
   :lineno-match:
   :start-at: # Definitions of the setup stages.
   :end-before: # Definition of the `bootstrap` stage.
   :emphasize-lines: 11,14

This completes the definition of setup stage ``clean`` in the ``stages``
section.  It remains to give the command for the ``make clean`` as value of a
parameter in a ``clean`` section.  This is trivial and amounts to:

.. literalinclude:: ../examples/idris2/idris2-setup.conf
   :language: ini
   :linenos:
   :lineno-match:
   :start-at: # Parameters for the `clean` stage.
   :end-before: # Parameters for the `bootstrap` stage.
   :emphasize-lines: 4-5


.. _idr2-bootstrap_stage:

---------------------------------
Configure the ``bootstrap`` stage
---------------------------------

Configuring the ``bootstrap`` setup stage for the *Idris 2* example is rather
straightforward, too.  Actually, our task is very similar to the definition of
the ``clean setup`` stage in section :ref:`idr2-clean_stage`.  A look at the
``bootstrap`` shell function teaches us that we need a ``rm -rf ...`` and two
``make ...`` substitutes.  For the former we utilize, again, the setup step
function :py:func:`dusetup.steps.run_cmd_from_cfg`, and for the latter the
:py:func:`dusetup.steps.run_cmds_from_cfg` setup step function that can run
multiple commands.  Thus we get the following definition for the setup steps
in the ``bootstrap`` setup stage definition:

.. literalinclude:: ../examples/idris2/idris2-setup.conf
   :language: ini
   :linenos:
   :lineno-match:
   :start-at: # Definition of the `clean` stage.
   :end-before: # Definition of the `build` stage.
   :emphasize-lines: 7-12

To complete the definition of the ``bootstrap`` setup stage we have yet to
take care of some parameters for these setup step function calls.  First let
us note that the path (template, actually) for the directory to be removed was
already defined for ``pre``; hence our :py:func:`dusetup.step.path.rm_item`
does not need new parameters.  But we have to give the list of command
templates for the ``make ...`` commands as value of parameter ``cmds_tpls`` in
a ``bootstrap`` section.  Again this is a rather straightforward translation
of the original shell commands.  We only need a new ``scheme`` setup
parameter - a static one - for the ``make bootstrap ...`` command.  So let us
define this one first:

.. literalinclude:: ../examples/idris2/idris2-setup.conf
   :language: ini
   :linenos:
   :lineno-match:
   :start-at: # Parameters for the `bootstrap` stage.
   :end-before: # Templates for commands to bootstrap Idris 2.
   :emphasize-lines: 4-5

Now the value for the command templates parameter ``cmds_tpls`` is easy going,
and completes the configuration of the ``bootstrap`` setup stage:

.. literalinclude:: ../examples/idris2/idris2-setup.conf
   :language: ini
   :linenos:
   :lineno-match:
   :start-at: # Parameters for the `bootstrap` stage.
   :end-before: # Parameters for the `build` stage.
   :emphasize-lines: 7-11


.. _idr2-build_stage:

-----------------------------
Configure the ``build`` stage
-----------------------------

The configuration of the *Idris 2* example's ``build`` stage need no building
blocks that where not already used in section :ref:`idr2-bootstrap_stage`.
The only notable difference from a direct porting of shell commands in the
:file:`idris2_setup.bash` function ``build_stage_step`` is the handling of the
``mkdir -p ...`` command:  First we do this command first because we can then
collect all three translated ``make ...`` commands in one list.  Second we use
again setup step function :py:func:`dusetup.step.path.ensure_clean_dir` for
the ``mkdir -p ...`` command [#ensure_clean_dir_mk]_.  Let us implement this
strategy.

Like just told we start with the ``mkdir -p ...`` command, and translate it by
using the :py:func:`dusetup.step.path.ensure_clean_dir` setup step function.
Please note that the path for the directory to be created is not defined as
setup parameter yet.  Since it is static we can give it directly as setup
configuration parameter ``rel_static_doc_dpath`` and need no template for it.
Like in the ``build_stage_step`` shell function, ``rel_static_doc_dpath`` uses
another new static setup parameter ``rel_docs_dpath`` that is also used for a
new command in a moment.  So let us start new setup configuration section
``build`` for new setup parameters for the ``build`` setup stage like this:

.. literalinclude:: ../examples/idris2/idris2-setup.conf
   :language: ini
   :linenos:
   :lineno-match:
   :start-at: # Parameters for the `build` stage.
   :end-before: # Templates for commands to build Idris 2.
   :emphasize-lines: 4-5,7-9

This gives us all we need to start the specification of the ``build`` setup
stage as value of setup configuration parameter ``build`` in setup
configuration section ``stages`` using the
:py:func:`dusetup.step.path.ensure_clean_dir` setup step function:

.. literalinclude:: ../examples/idris2/idris2-setup.conf
   :language: ini
   :linenos:
   :lineno-match:
   :start-at: # Definitions of the setup stages.
   :end-before: # Definition of the `install-app` stage.
   :emphasize-lines: 23,25

The remaining relevant commands in ``build_stage_step`` shell function to
translate are three ``make ...`` commands.  We first define new setup
parameter, say ``cmds_tpls`` whose list value give templates for the
translations of these commands:

.. literalinclude:: ../examples/idris2/idris2-setup.conf
   :language: ini
   :linenos:
   :lineno-match:
   :start-at: # Parameters for the `build` stage.
   :end-before: # Parameters for the `install-app` stage.
   :emphasize-lines: 11-16

Based on this we can complete the ``build`` setup stage specification by just
running the respective commands:

.. literalinclude:: ../examples/idris2/idris2-setup.conf
   :language: ini
   :linenos:
   :lineno-match:
   :start-at: # Definitions of the setup stages.
   :end-before: # Definition of the `install-app` stage.
   :emphasize-lines: 23,26


.. _idr2-install-app_stage:

-----------------------------------
Configure the ``install-app`` stage
-----------------------------------

Next we have to configure the ``install-app`` setup stage for our *Idris 2*
example.  As for the other setup stages, we start from the corresponding
function in the :file:`idris2_setup.bash` script that is ``install_app_step``
for ``install-app``.

The first command ``rm -rf ...`` in the ``install_app_step`` is translated
similar like for the ``install`` stage for Git in section
:ref:`git-install_stage`: we use setup step function
:py:func:`dusetup.step.path.ensure_clean_dir` for it.  Please note that the
needed setup parameter ``prefix_dpath`` is one of the calculated standard
setup parameters.  Hence we can start the list value for the ``install-app``
parameter in the ``stages`` section like this:

.. literalinclude:: ../examples/idris2/idris2-setup.conf
   :language: ini
   :linenos:
   :lineno-match:
   :start-at: # Definitions of the setup stages.
   :end-before: # Definition of the `install-lib` stage.
   :emphasize-lines: 29,31

The next two commands in the ``install_app_step`` shell function are ``make
...`` commands.  Again, we handle them like similar commands for the
``bootstrap`` and ``build`` setup stages:  First we define an appropriate list
of command templates in a section ``install-app`` for new setup parameters
needed by the ``install-app`` setup stage:

.. literalinclude:: ../examples/idris2/idris2-setup.conf
   :language: ini
   :linenos:
   :lineno-match:
   :start-at: # Parameters for the `install-app` stage.
   :end-before: # Template for the FHS command file path of installed Idris 2.
   :emphasize-lines: 4-8

Second we add a call of the :py:func:`dusetup.steps.run_cmds_from_cfg` setup
step function to the value of ``install-app`` parameter in the ``stages``
section:

.. literalinclude:: ../examples/idris2/idris2-setup.conf
   :language: ini
   :linenos:
   :lineno-match:
   :start-at: # Definitions of the setup stages.
   :end-before: # Definition of the `install-lib` stage.
   :emphasize-lines: 29,31

The remaining stuff in the ``install_app_step`` shell function creates a
relative symlink for the command of *Idris 2*.  For this purpose we can use
setup step function :py:func:`dusetup.step.path.symlink_item`.  For its call
we have to pass two additional positional arguments - one for the source and
one for the destination path of the symlink.  Both result from setup
parameters we have not encountered yet.  Actually, these setup parameters are
calculated ones whose calculation only needs standard calculated setup
parameters.  Hence we add for both appropriate parameter templates to the
``install-app`` setup configuration section:

.. literalinclude:: ../examples/idris2/idris2-setup.conf
   :language: ini
   :linenos:
   :lineno-match:
   :start-at: # Parameters for the `install-app` stage.
   :end-before: # Parameters for the `install-lib` stage.
   :emphasize-lines: 10-11,13-14

Based on this we can complete the value of ``install-app`` parameter in the
``stages`` setup configuration section:

.. literalinclude:: ../examples/idris2/idris2-setup.conf
   :language: ini
   :linenos:
   :lineno-match:
   :start-at: # Definitions of the setup stages.
   :end-before: # Definition of the `install-lib` stage.
   :emphasize-lines: 29,32-33

Please note the additional keyword arguments used in the
:py:func:`dusetup.step.path.symlink_item` call: ``mk_relative=true`` ensures
that the created symlink is a relative one, and ``verbose=true`` causes a
message for the symlink creation.


.. _idr2-install-lib_stage:

--------------------------------
Set up the ``install-lib`` stage
--------------------------------

The next setup stage we want to set up is the ``install-app`` one of our
*Idris 2* example meaning to translate the relevant parts of the corresponding
function ``install_app_step`` in the :file:`idris2_setup.bash` script.

The start of this process is business as usual: the first relevant commands in
the ``install_app_step`` function are two ``make ...`` commands and a ``rm -rf
...`` one.  Let us start with the two ``make ...`` commands.  The setup
parameter ``lib_dpath`` used by both of them is already calculated.  Hence we
only have to configure appropriate command templates for them in setup
configuration section ``install-lib`` that is dedicated to new setup
parameters for the ``install-lib`` setup step:

.. literalinclude:: ../examples/idris2/idris2-setup.conf
   :language: ini
   :linenos:
   :lineno-match:
   :start-at: # Parameters for the `install-lib` stage.
   :end-before: # Template for an extra directory path of installed Idris 2.
   :emphasize-lines: 4-8

Using this template we can begin the value of parameter ``install-lib`` in the
``stages`` setup configuration section.  Its first element defines the setup
step function call that runs the commands given by this template:

.. literalinclude:: ../examples/idris2/idris2-setup.conf
   :language: ini
   :linenos:
   :lineno-match:
   :start-at: # Definitions of the setup stages.
   :end-before: # Definition of the `install-doc` stage.
   :emphasize-lines: 36,38

To port the ``rm -rf ...`` command in the ``install_app_step`` function the
setup step function :py:func:`dusetup.step.path.rm_item` is utilized one more
time.  For its call we need a new calculated setup parameter.  Thus we define
first a template for th latter:

.. literalinclude:: ../examples/idris2/idris2-setup.conf
   :language: ini
   :linenos:
   :lineno-match:
   :start-at: # Parameters for the `install-lib` stage.
   :end-at: lib_del_dpath_tpl = {lib_dpath}/lib
   :emphasize-lines: 10-11

Then we can enhance the value of parameter ``install-lib`` in the ``stages``
setup configuration section with the definition for the respective setup
function call:

.. literalinclude:: ../examples/idris2/idris2-setup.conf
   :language: ini
   :linenos:
   :lineno-match:
   :start-at: # Definitions of the setup stages.
   :end-before: # Definition of the `install-doc` stage.
   :emphasize-lines: 36,39

Next we meet the problem for the first time that was described in section
:ref:`idr2-why_own_code`.  Like already mentioned there this problem motivates
us to write own code for the ``dusetup``-based setup of *Idris 2*.  Let us
recap: we have a setup parameter, namely ``raw_lib_dpath`` that has to be
calculated, but its calculation needs the *Idris 2* binary just built and
installed at setup stages ``build`` and ``install-app`` before.  Hence we can
calculate ``raw_lib_dpath`` not until these setup stages are completed.

Our solution for this problem is to write a setup step function to just
calculate the ``raw_lib_dpath`` setup parameter, and to add this parameter to
the setup parameter dictionary [#return_params]_.  We will need
``raw_lib_dpath`` again for the ``install-doc`` setup stage but want to do the
``raw_lib_dpath`` calculation only once; hence we check first whether the
setup parameter dictionary already contains the ``raw_lib_dpath`` setup
parameter [#reuse_calc]_.

As already mentioned in section :ref:`user_interface` at point 2, we put our
own code for the *Idris 2* setup in a Python module
:py:mod:`examples.idris2.idris2_setup` in the same directory like the setup
configuration file :file:`idris2-setup.conf` for the *Idris 2* setup example
to let ``dusetup`` automatically find this Python module.  Its documentation
reads like:

.. automodule:: examples.idris2.idris2_setup

Let us begin to implement our setup step function, say
:py:func:`examples.idris2.idris2_setup.add_idris2_raw_lib_dpath`, that takes
care of the ``raw_lib_dpath`` setup parameter calculation.  Being the good
Python programmers we are we start the function's definition by a proper
documentation:

.. autofunction:: examples.idris2.idris2_setup.add_idris2_raw_lib_dpath

Before turning to the real code of our setup step function
:py:func:`examples.idris2.idris2_setup.add_idris2_raw_lib_dpath` let us first
note that we will use a generic helper function from ``dusetup``, namely
:py:func:`dusetup.util.run.run_cmd`, to run the respective *Idris 2* command
to determine the value of the ``raw_lib_dpath`` setup parameter.  For this we
need an appropriate command template for this very command.  As usual we
define this command template within the ``install-lib`` setup configuration
section:

.. literalinclude:: ../examples/idris2/idris2-setup.conf
   :language: ini
   :linenos:
   :lineno-match:
   :start-at: # Parameters for the `install-lib` stage.
   :end-at: libdir_cmd_tpl = %{install-app.fhs_cmd_fpath_tpl}% --libdir
   :emphasize-lines: 13-15

Now we have all we need for the whole definition of function
:py:func:`examples.idris2.idris2_setup.add_idris2_raw_lib_dpath`.  First we
ensure that the ``raw_lib_dpath`` calculation is only done if it was not
already done before:

.. literalinclude:: ../examples/idris2/idris2_setup.py
   :language: python
   :linenos:
   :lineno-match:
   :start-at: def add_idris2_raw_lib_dpath(params):
   :end-at: if params.get('raw_lib_dpath') is None:
   :emphasize-lines: 18-19

Next we get the setup configuration, extract our *Idris 2* command template
from it, and fill this template to get the real *Idris 2* command:

.. literalinclude:: ../examples/idris2/idris2_setup.py
   :language: python
   :linenos:
   :lineno-match:
   :start-at: def add_idris2_raw_lib_dpath(params):
   :end-at: raw_libdir_cmd = raw_libdir_cmd_tpl.format_map(params)
   :emphasize-lines: 20-26

Then we run the respective *Idris 2* command, and catch its output to STDOUT
as string.  To get ``raw_lib_dpath``'s proper value we have to remove a
trailing newline from this output, too.  These two things are implemented
next:

.. literalinclude:: ../examples/idris2/idris2_setup.py
   :language: python
   :linenos:
   :lineno-match:
   :start-at: def add_idris2_raw_lib_dpath(params):
   :end-at: result_stdout = result.stdout.rstrip()
   :emphasize-lines: 27-32

Finally we store the calculated ``raw_lib_dpath`` setup parameter in the
parameter dictionary:

.. literalinclude:: ../examples/idris2/idris2_setup.py
   :language: python
   :linenos:
   :lineno-match:
   :start-at: def add_idris2_raw_lib_dpath(params):
   :end-before: return params
   :emphasize-lines: 33-35

Last but not least our setup step function has to return, like every setup
function, the current parameter dictionary:

.. literalinclude:: ../examples/idris2/idris2_setup.py
   :language: python
   :linenos:
   :lineno-match:
   :start-at: def add_idris2_raw_lib_dpath(params):
   :end-at: return params
   :emphasize-lines: 37

This makes :py:func:`examples.idris2.idris2_setup.add_idris2_raw_lib_dpath`'s
definition complete.  Of course the :py:mod:`examples.idris2.idris2_setup`
Python module has to import all Python modules needed for the function's
definition before this definition:

.. literalinclude:: ../examples/idris2/idris2_setup.py
   :language: python
   :linenos:
   :lineno-match:
   :start-at: import pathlib
   :end-at: def add_idris2_raw_lib_dpath(params):
   :emphasize-lines: 1-3

After adding these imports at :py:mod:`examples.idris2.idris2_setup`'s begin
our own supplemental Python code for the *Idris 2* setup is complete.

Having :py:func:`examples.idris2.idris2_setup.add_idris2_raw_lib_dpath`'s
definition available, we can define the call of this setup step function as
new element in the list value of setup configuration parameter ``install-lib``
in the ``stages`` setup configuration section:

.. literalinclude:: ../examples/idris2/idris2-setup.conf
   :language: ini
   :linenos:
   :lineno-match:
   :start-at: # Definitions of the setup stages.
   :end-before: # Definition of the `install-doc` stage.
   :emphasize-lines: 36,40

After the calculation of the setup parameter ``install-lib`` is ensured we can
use it.  In the ``install-lib`` setup stage it is used to calculate another
setup parameter - namely the path ``os_lib_dpath`` of a directory we want to
create symlinks for all items into it.  For the creation of these links a
setup step function comes to our rescue that is a sibling of setup step
function :py:func:`dusetup.step.path.symlink_item` we have used in the
``install-app`` setup stage.  Concretely, the setup step function
:py:func:`dusetup.step.path.symlink_items_of_dir` create symlinks for all
items in a directory whose path is passed as mandatory argument.  Please note
that this argument allows a path template - like the other path-related setup
step functions for their path arguments.

So let us define the path template for the ``os_lib_dpath`` directory first -
of course in the ``install-lib`` setup configuration section:

.. literalinclude:: ../examples/idris2/idris2-setup.conf
   :language: ini
   :linenos:
   :lineno-match:
   :start-at: # Parameters for the `install-lib` stage.
   :end-at: raw_os_lib_dpath_tpl = {raw_lib_dpath}/lib
   :emphasize-lines: 17-18

Based on this we specify the call for the setup step function
:py:func:`dusetup.step.path.symlink_items_of_dir` to create the wanted links.
Like for the :py:func:`dusetup.step.path.symlink_item` call in section
:ref:`idr2-install-app_stage` we want relative symlinks and messages for their
creation.  For this we can use the same additional keyword arguments:

.. literalinclude:: ../examples/idris2/idris2-setup.conf
   :language: ini
   :linenos:
   :lineno-match:
   :start-at: # Definitions of the setup stages.
   :end-before: # Definition of the `install-doc` stage.
   :emphasize-lines: 36,41-42

This completes the definition of the ``install-lib`` setup stage.


-----------------------------------
Configure the ``install-doc`` stage
-----------------------------------

To complete the ``dusetup``-based setup for our *Idris 2* example we have
a last setup stage left to configure - namely the ``install-doc`` stage.  Like
for the *Idris 2* setup stages before we use the corresponding shell function
``install_doc_step`` from Bash script :file:`idris2_setup.bash` as departing
point.

For the first relevant command ``mkdir -pv ...`` in the ``install_doc_step``
shell function we could again employ setup step function
:py:func:`dusetup.step.path.ensure_clean_dir` - like for a similar shell
command in section :ref:`idr2-build_stage`.  Since the needed directory path
is among the calculated standard setup parameters we start the definition of
the ``install-doc`` setup stage in the ``stages`` setup configuration section
right away.  Please note that we add ``verbose=true`` as keyword argument to
the function call because we want a message signaling directory creation:

.. literalinclude:: ../examples/idris2/idris2-setup.conf
   :language: ini
   :linenos:
   :lineno-match:
   :start-at: # Definitions of the setup stages.
   :end-before: # Parameters for the `pre` stage.
   :emphasize-lines: 45,47

The next interesting command in the ``install_doc_step`` shell function
amounts to copying a directory including its content.  This is covered by the
:py:func:`dusetup.step.path.cpmv_item` setup step function.  For its concrete
call in our current situation we need two new setup parameters giving the
source and destination directory paths for the copying.  The first of these
setup parameters is a static one; hence we can give it directly in the setup
configuration file.  The second setup parameter is a calculated one; so we
give it, like usual, by an appropriate template in the setup configuration
file.
Of course these setup configuration parameters for the ``install-doc`` setup
stage enter a setup configuration section ``install-doc``.  Putting this stuff
together we get:

.. literalinclude:: ../examples/idris2/idris2-setup.conf
   :language: ini
   :linenos:
   :lineno-match:
   :start-at: # Parameters for the `install-doc` stage.
   :end-at: fhs_app_doc_dpath_tpl = {doc_dpath}/application
   :emphasize-lines: 4-6,8-10

After this preparation we can specify the needed call of setup step function
:py:func:`dusetup.step.path.cpmv_item`:

.. literalinclude:: ../examples/idris2/idris2-setup.conf
   :language: ini
   :linenos:
   :lineno-match:
   :start-at: # Definitions of the setup stages.
   :end-before: # Parameters for the `pre` stage.
   :emphasize-lines: 45,48-49

The next command within the ``install_doc_step`` shell function to handle is a
``make ..`` command using a setup parameter that is already calculated.
Therefore we first add an appropriate command template to the ``install-doc``
setup configuration section:

.. literalinclude:: ../examples/idris2/idris2-setup.conf
   :language: ini
   :linenos:
   :lineno-match:
   :start-at: # Parameters for the `install-doc` stage.
   :end-at: cmd_tpl =  make install-libdocs PREFIX={lib_dpath}
   :emphasize-lines: 12-13

Second we specify an appropriate call of setup step function
:py:func:`dusetup.step.run.run_cmd_from_cfg` to run this single command
[#run_a_cmd]_:

.. literalinclude:: ../examples/idris2/idris2-setup.conf
   :language: ini
   :linenos:
   :lineno-match:
   :start-at: # Definitions of the setup stages.
   :end-before: # Parameters for the `pre` stage.
   :emphasize-lines: 45,50

The structure of the remaining commands to handle in the ``install_doc_step``
shell function has some resemblance to last commands in the
``install_lib_step`` shell function:  First the peculiar setup parameter
``raw_lib_dpath`` is calculated.  Then second ``raw_lib_dpath`` is used for
another calculated setup parameter.  And thirdly the latter setup parameter is
finally used as an argument within the last command in the respective setup
stage shell function. Therefore we can use parts of section
:ref:`idr2-install-lib_stage` as inspiration.

First, to calculate the ``raw_lib_dpath`` setup parameter our own setup step
function :py:func:`examples.idris2.idris2_setup.add_idris2_raw_lib_dpath` is
called within the ``install-doc`` setup stage:

.. literalinclude:: ../examples/idris2/idris2-setup.conf
   :language: ini
   :linenos:
   :lineno-match:
   :start-at: # Definitions of the setup stages.
   :end-before: # Parameters for the `pre` stage.
   :emphasize-lines: 45,51

Second, a template for the calculated setup parameter based on
``raw_lib_dpath`` is then added to the ``install-doc`` setup configuration
section:

.. literalinclude:: ../examples/idris2/idris2-setup.conf
   :language: ini
   :linenos:
   :lineno-match:
   :start-at: # Parameters for the `install-doc` stage.
   :end-at: raw_lib_doc_dpath_tpl = {raw_lib_dpath}/docs
   :emphasize-lines: 15-17

Thirdly, we can turn to translate the final command within the
``install_doc_step`` function that moves a directory.  This can be achieved by
using setup step function :py:func:`dusetup.step.path.cpmv_item` again - but
this time with optional argument ``rm_src`` set to a value representing
``True``.  The template to calculate the source directory path was just
defined.  For the destination directory path we need another setup parameter;
actually it is a calculated one.  Therefore we add a template for it to the
``install-doc`` setup configuration section:

.. literalinclude:: ../examples/idris2/idris2-setup.conf
   :language: ini
   :linenos:
   :lineno-match:
   :start-at: # Parameters for the `install-doc` stage.
   :end-at: fhs_lib_doc_dpath_tpl = {doc_dpath}/libs
   :emphasize-lines: 19-21

Now we have everything that is needed to define an appropriate call of setup
step function :py:func:`dusetup.step.path.cpmv_item` as last element of the
``install-doc`` value in the ``stages`` setup configuration section:

.. literalinclude:: ../examples/idris2/idris2-setup.conf
   :language: ini
   :linenos:
   :lineno-match:
   :start-at: # Definitions of the setup stages.
   :end-before: # Parameters for the `pre` stage.
   :emphasize-lines: 45,52-53

This completes the configuration for the ``install-doc`` setup stage for
*Idris 2*.  Since ``install-doc`` is the last setup stage in the *Idris 2*
setup example we have this example completely redone now.


What we have achieved
=====================

First of all we have reproduced the complete ``dusetup`` setup configuration
file :file:`idris2-setup.conf` for our example *Idris 2* setup:

.. literalinclude:: ../examples/idris2/idris2-setup.conf
   :language: ini
   :linenos:

together with file :file:`idris2_setup.py` that is a Python module containing
supplemental code needed for our ``dusetup``-based *Idris 2* setup example:

.. literalinclude:: ../examples/idris2/idris2_setup.py
   :language: python
   :linenos:

Utilizing these two ingredients ``dusetup`` can be used as commandline tool to
set up *Idris 2* as already written above in various places.  The arguments
offered in section :ref:`git-achieved` for using ``dusetup`` as commandline
setup tool for *Git* are valid without modifications for the *Idris 2*
example, too.  Hence there is no need to repeat them here again.

So let us finish this *Idris 2* setup tutorial for ``dusetup`` by playing a
bit with its stuff, i.e. by setting up *Idris 2* from source.  For this we use
a similar workflow like for *Git* in section :ref:`git-achieved`.

Again we start with a fresh copy of the *Idris 2* source code from its Git
repository:

.. code-block:: bash
   :caption: Get a copy of the Git repository for *Idris 2*.

   >>> mkdir ~/playtime
   >>> cd ~/playtime
   >>> git clone https://github.com/idris-lang/Idris2.git idris2-devel
   >>> cd idris2-devel

Following the path for *Git* in section :ref:`git-achieved` further we prepare
a virtual environment with ``dusetup`` including its dependencies:

.. code-block:: bash
   :caption: Installation of ``dusetup`` and its dependencies

   >>> python3 -m venv ~/playtime/venv
   >>> source ~/playtime/venv/bin/activate
   >>> export GITLAB=git+https://gitlab.com/conlogic
   >>> pip install $GITLAB/dubasiclog.git
   >>> pip install $GITLAB/duconfigparser.git
   >>> pip install $GITLAB/duipoldict.git
   >>> pip install $GITLAB/duconfig.git
   >>> pip install $GITLAB/dusetup.git

Next we need the two files :download:`idris2-setup.conf
<../examples/idris2/idris2-setup.conf>` and :download:`idris2_setup.py
<../examples/idris2/idris2_setup.py>`.  To let ``dusetup`` automatically find
them we copy both into the :file:`~/playtime/idris2-devel` directory.

Please note that *Idris 2* relies on a suitable Scheme backend.  Actually both
the original Bash setup script and our ``dusetup``-based setup for *Idris 2*
assume to use `Chez Scheme`_ for this purpose.  Therefore we need a working
installation of *Chez Scheme*, too.  To keep things simple let us assume we
have one.

For a first smoke test we could ask our nicely prepared ``dusetup`` for help:

.. code-block:: console
   :caption: Commandline options for ``dusetup``

   >>> cd ~/playtime/idris2-devel
   >>> dusetup -h
   ...

Since the output of this command is similar to the call of ``dusetup -h`` for
*Git* in section :ref:`git-achieved` we do not show it here.

Now a complete setup for *Idris 2* using the defaults can be issued by

.. code-block:: console

   >>> dusetup

Next we could play, like for the *Git* setup in section :ref:`git-achieved`,
with various commandline options for ``dusetup``.  Let us show this only for
one, namely ``-a`` / ``--add-import-dpath``.  Please recall that this option
can give paths for directories to be used by Python to find its modules.  Like
already described in section :ref:`user_interface` this can be useful to let
``dusetup`` find additional code - like our :py:mod:`idris2_setup` module.

To show this let us first move file :file:`idris2_setup.py` to a directory,
say :file:`/tmp`, where ``dusetup`` does not find it by default:

.. code-block:: bash
   :caption: Hide :file:`idris2_setup.py` from ``dusetup``

   >>> mv ~/playtime/idris2-devel/idris2_setup.py /tmp

Then we can use option ``-a`` / ``--add-import-dpath`` to let ``dusetup`` find
:file:`idris2_setup.py` again:

.. code-block:: console

   >>> dusetup --add-import-path /tmp


.. [#FHS] The *Filesystem Hierarchy Standard* for UNIX systems "consists of a
          set of requirements and guidelines for file and directory placement
          under UNIX-like operating systems" - as stated in the abstract of
          the *FHS* document.  It can be found at the `FHS page`_.

.. [#idr2_bash] This Bash script :download:`idris2_setup.bash
                <idris2/idris2_setup.bash>` lives in subdirectory with path
                :file:`idris2` of this documentation.

.. [#post_stage] By the way, there is complementary dummy setup stage
                 ``post``.  It is similar to ``pre`` but it is performed
                 *after* every other setup stage.

.. [#func_intp2] The value of this parameter is another instance of
                 ``duconfig``'s function call interpolation: function
                 :py:func:`dusetup.conf.interpol.abspath` is used to get the
                 absolute variant of the given path.  Please note that it does
                 not matter that the argument that is fed to this function is
                 not a path but a path template.  Section ref:`git_tut` has
                 mentioned some more details for function call interpolation.

.. [#item_intp2] Let us recall that for the values of these parameters the
                 item interpolation from ``duconfig`` is used.   Section
                 ref:`git_tut` has mentioned some details for item
                 interpolation.

.. [#beg_path] A look at :py:func:`dusetup.step.path.add_path_envvar`'s
               documentation teaches us that by default the respective path is
               added at the begin of the respective environment variable -
               just like it's needed.

.. [#ensure_clean_dir_mk] Please note that setup step function
                          :py:func:`dusetup.step.path.ensure_clean_dir` does
                          not only ensure that the respective directory
                          exists but also that it is empty.  Since this
                          directory does not exist before, the clearing up of
                          its content is automatically skipped.

.. [#return_params] Here we really profit for the first time that a setup step
                    function always return the current setup parameter
                    dictionary.

.. [#reuse_calc] This check allows us to call our setup step function that
                 calculates ``raw_lib_dpath`` again during the ``install-doc``
                 setup stage without the overhead to calculate
                 ``raw_lib_dpath`` again.  By calling the respective setup
                 step function again during the ``install-doc`` we can, for
                 instance, only do the ``install-doc`` setup stage in a
                 ``dusetup`` run - provided that the other setup step were
                 already performed in former ``dusetup`` runs.

.. [#run_a_cmd] You may recall that this setup step function
                :py:func:`dusetup.step.run.run_cmd_from_cfg` running a single
                command was already used in the *Git* setup example above in
                section :ref:`git-clean_stage`.


.. _FHS page: https://refspecs.linuxfoundation.org/fhs.shtml

.. _Chez Scheme: https://cisco.github.io/ChezScheme


..
 -----------------------------------------------------------------------------
 Local Variables:
 mode: rst
 ispell-local-dictionary: "en"
 End:

..  LocalWords:  Chez
