..
 =============================================================================
 Title          : Setup utilities

 Classification : reST text file

 Author         : Dirk Ullrich

 Date           : 2022-04-27

 Description    : License for the distribution package.
 =============================================================================


License
=======

This package is licensed by the LGPL 3.  Fur the full text of this license see
file :download:`LICENSE.txt <../LICENSE.txt>` within this package.


..
 -----------------------------------------------------------------------------
 Local Variables:
 mode: rst
 ispell-local-dictionary: "en"
 End:
