..
 =============================================================================
 Title          : Setup utilities

 Classification : reST text file

 Author         : Dirk Ullrich

 Date           : 2022-08-21

 Description    : How to write a ``dusetup``-based Git setup.
 =============================================================================


.. _git_tut:

============================================================
Tutorial: let us configure ``dusetup`` as setup tool (*Git*)
============================================================


.. _git-what_to_do:

What we want to do
==================

This tutorial tries to show how ``dusetup`` itself can be used as commandline
setup tool for an application by solely writing an appropriate configuration
file.  This amounts to the creation of an appropriate setup configuration file
to perform the setup stages needed for the application we want to set up.  For
this tutorial we choose a simple setup for *Git* and its documentation from
source.  Actually, we will construct the *Git* example setup configuration
file from scratch.

For a simple *Git* setup from source a bunch of shell commands issued in a
terminal (running a Bash) suffice.  We group them into setup stages:

+ ``clean`` stage (i.e. clean up the *Git* source):

  .. code-block:: bash

     make distclean

+ ``configure`` stage (i.e. configure the *Git* build), where ``<vers>`` is
  the *Git* version:

  .. code-block:: bash

     export APP_VERS=<vers>
     export PREFIX_DPATH=/tmp/git-${APP_VERS}
     test -f configure || autoreconf -i
     ./configure --libpcre --prefix=$PREFIX_DPATH

+ ``build`` stage (i.e. build *Git* and its documentation from source), where
  <nr_cpus> is the number of CPUs (CPU cores) that can be used in parallel:

  .. code-block:: bash

     make -j <nr_cpus>
     make -j <nr_cpus> doc

+ ``install`` stage (i.e. install built *Git* and its documentation):

  .. code-block:: bash

     mkdir -p $PREFIX_DPATH && rm -rf $PREFIX_DPATH/*
     make install prefix=$PREFIX_DPATH
     make install-doc prefix=$PREFIX_DPATH


.. _git-write_conf:

Write a ``dusetup`` configuration file for *Git*
================================================

A setup done by ``dusetup`` or by a ``dusetup``-based tool is organized in
stages.  Every such setup stage consists, in turn, of a sequence / list of
setup steps.  To put it differently: setup steps are the building blocks of
the setup stages.  Every setup step will implemented in ``dusetup`` by
configuring an appropriate call of one of the so-called *setup step functions*
living in package :py:mod:`dusetup.step`.

A look at section :ref:`git-what_to_do` shows that we have already used the
term "setup step".  In turn, one could take the shell commands that cause
changes in the filesystem as setup steps.  (The commands changing only the
shell's state by setting environment variables deserve special consideration.)
Since ``dusetup`` already provides setup step functions to run external
commands we can hope to implement all setup stages for *Git* by only writing
an appropriate configuration file for ``dusetup``.

Before we start to configure the setup stages for *Git* setup we get a bit
deeper into the parameters used during setup, the so-called *setup
parameters*.  Before performing the setup stages is started, a bunch of
initial setup parameters is calculated and stored in a dictionary.  During
performing of the setup stages, any setup step function is called with a
dictionary of the setup parameters and is expected to return itself such a
dictionary.  Here the first setup step function gets the initial setup
parameter dictionary just mentioned, and every other setup step function get
the setup parameter dictionary returned by the call of the setup step function
for the setup step before.

We can distinguish the following variants of parameters:

a. *Configuration parameters*: these are parameters that are directly come
   from the configuration file(s) for the setup tool.  Such parameters are
   rather static, i.e. after they are got from the setup configuration their
   value usually do not change anymore.

b. *Calculated parameters*: such parameters are calculated *outside of the
   setup configuration* before and during performing the setup stages.  For
   instance, a bunch of initial setup parameters are calculated from the setup
   configuration parameters and from parameters given by commandline options.
   Starting with this initial setup parameters, every setup step function can
   calculate further setup parameters that are then passed to the following
   setup step function(s).  Such parameters are, by their very nature, rather
   dynamic.


.. _git-default_params:

-----------------------------
Parameters for default values
-----------------------------

Let us start with :file:`git-setup.conf`.  First of all, we need a section
``default``.  Its parameters are used to calculate default values for some
initial setup parameters whose default values cannot be calculated otherwise.
By the way, the value of the respective initial setup parameter can be changed
by an appropriate commandline option.  There are some mandatory parameters in
this setup configuration section that have fixed meanings:

+ Parameter ``stages`` for the list of name of all setup stages in their
  proper order.  This value also gives the default list of setup stages to be
  performed.  To perform only a subset of them option ``-s`` / ``--stages``
  can be used.

+ Parameter ``prefix_dpath_tpl`` for the default (template of the)
  installation prefix directory path.  This value can be changed by option
  ``-p`` / ``--prefix``.

The default values for other initial setup parameters are automatically
calculated.  For instance, the name and version of the application that should
be set up is determined as it was already mentioned in section
:ref:`user_interface`.  Please note that the application's version to be used
can also be modified by a commandline option - namely ``-v`` /
``--app-version``.

So let us configure the ``default`` parameters for our *Git* setup.  The names
for all possible *Git* setup stages is simply stolen from section
ref:`git-what_to_do`.  In particular, the value of this parameter is a literal
value, and therefore parameter ``stages`` is definitely a static one
[#list_val]_:

.. literalinclude:: ../examples/git/git-setup.conf
   :language: ini
   :linenos:
   :lineno-match:
   :start-at: # Default values some initial setup parameters.
   :end-before: # Path template for default installation prefix directory.
   :emphasize-lines: 4-10

Next we give the *template* for the installation prefix directory path.  It is
not the prefix directory path yet, since it contains a placeholder
``{app_name_vers}``.  To get the concrete prefix directory this placeholder
has to be replaced by the combined application name and version.  Therefore
``prefix_dpath_tpl`` is a static setup configuration parameter (actually a
literal one), but the prefix directory path will be a calculated parameter
[#tmp_prefix]_:

.. literalinclude:: ../examples/git/git-setup.conf
   :language: ini
   :linenos:
   :lineno-match:
   :start-at: # Default values some initial setup parameters.
   :end-before: # Definitions of the setup stages.
   :emphasize-lines: 12-15


.. _git-clean_stage:

-----------------------------
Configure the ``clean`` stage
-----------------------------

Our next task is to configure ``clean``, the first *Git* setup stage.  But
before starting this let us describe what one has to do to configure a setup
stage for ``dusetup``.

To configure a setup stage, say (named as) ``<stage>``, we have to ensure:

1. The name ``<stage>`` of the setup stage must be an element of the list
   value for parameter ``stages`` in setup configuration section ``default``.

2. Setup configuration section ``stages`` has to have a parameter ``<stage>``
   whose value defines the list of setup steps the ``<stage>`` stage consists
   of.

Let us explain point 2 a bit more.  As already said at the beginning of
section :ref:`git-write_conf`, a setup stage like ``<stage>`` consists of a
sequence of setup steps where every such setup step is represented by a
specific call of a setup step function.   Defining these calls for the
``<stage>`` setup stage is the very purpose of parameter ``<stage>`` in
section ``stages``: every element of its list value defines exactly one setup
step for ``<stage>`` by specifying the setup step function call for this setup
step.

How does such a setup step definition, i.e. definition of a setup step
function call, has to look like?  For this please recall, again from section
:ref:`git-write_conf`, how a setup step is performed, i.e. its corresponding
setup step function call is issued: the respective setup step function always
get the dictionary with the current setup parameters as 1st argument, and is
expected to return a dictionary with the setup parameters for the next setup
step (if there is any).  One can add, if needed, additional positional or
keyword arguments to the setup step function call.  Accordingly, a setup step
function call definition in the vale of the ``<stage>`` parameter starts with
the mandatory fully-qualified name of the setup step function to be called.
For additional positional or keyword arguments for the setup step function
call additional parts can be used in its definition [#step_fct_call]_.

Let us turn back to the ``clean`` setup stage.  Point 1 from above is already
done: in section :ref:`git-default_params`, the setup stage name ``clean`` has
been already added  as element to the value of parameter ``stages`` in setup
configuration section ``default``.  (Actually, this was done for all setup
stages planned for our *Git* setup example.)

For point 2 we note that this stage requires to run exactly one shell command.
This is what setup step function
:py:func:`dusetup.step.run.run_cmd_from_cfg` is for.  Beside the compulsory
first argument for the setup parameter dictionary, it needs an additional
positional argument for the fully qualified configuration parameter (name)
whose value is the command to perform.  We are free to chose the name for this
parameter and its section.  For our example we will adhere to the following
scheme: parameters first needed for a setup stage named ``<stage>`` are
grouped into a section of the same name ``<stage>``.  So, let us use a
parameter ``cmd`` in section ``clean`` to define the shell command for the
``clean`` setup stage:

.. literalinclude:: ../examples/git/git-setup.conf
   :language: ini
   :linenos:
   :lineno-match:
   :start-at: # Parameters for the `clean` stage.
   :end-before: # Parameters for the `configure` stage.
   :emphasize-lines: 4-5

The parameter defining the ``clean`` setup stage now becomes [#clean_fct]_:

.. literalinclude:: ../examples/git/git-setup.conf
   :language: ini
   :linenos:
   :lineno-match:
   :start-at: # Definitions of the setup stages.
   :end-before: # Definition the `configure` stage.
   :emphasize-lines: 4-5

This completes the configuration of the ``clean`` setup stage for our *Git*
setup example.


.. _git-configure_stage:

---------------------------------
Configure the ``configure`` stage
---------------------------------

For ``configure``, the next setup stage, we have to run not one but a list of
shell commands.  We could use, as in section :ref:`git-clean_stage`, use setup
step function :py:func:`dusetup.step.run.run_cmd_from_cfg` again - but this
time multiple times.  Instead, we chose
:py:func:`dusetup.step.run.run_cmds_from_cfg`.  Using this setup step function
we can bundle the shell commands to run for the ``configure`` stage.  But
there further new aspects.

Firstly, the first two commands for ``configure`` in section
:ref:`git-what_to_do` set some shell variables to be used in later commands.
Actually, these are the :envvar:`APP_VERS` with the version of Git as value,
and :envvar:`PREFIX_DPATH` whose value is the path of the installation prefix
directory.  For our ``dusetup`` configuration we do not need to do this
because parameters with this meaning are already calculated as setup
parameters, and they are stored in every setup parameter dictionary with the
keys ``'app_vers'`` and ``'prefix_dpath'``, respectively [#calc_params]_.
Since both setup parameters can be modified by commandline options, they are
calculated parameters in the sense of point b in section
:ref:`git-write_conf`.

Secondly, we need the values of those calculated parameters in shell commands
for ``configure`` - actually the path of the installation prefix directory for
the :samp:`./configure --prefix=...` command.  For this, i.e. to fill in
values of setup parameters, we use appropriate placeholders in the respective
commands.  In other words: we do not put parameters with the concrete values
into the setup configuration file, but parameters whose values are templates
for that concrete values that use placeholders for values of setup parameters
[#plh_params]_.

Thirdly, we will use another placeholder mechanism: the command template for
the :samp:`./configure --prefix=...` command is not give as single monolithic
value, but its options part :samp:`--prefix=...` is outsourced, for the sake
of modularity, to another parameter, namely ``conf_opts_tpl`` in the
``configure`` section.  Then the command template for :samp:`./configure ...`
refers to the value of the ``conf_opts_tpl`` by a specific placeholder with
the fully-qualified name of the parameter whose value should be used
[#item_intp]_.

With this preparation, we can complete our setup configuration file
:file:`git-setup.conf` for the ``configure`` setup stage.  Like for ``clean``,
we use a dedicated setup configuration section ``configure`` for the
parameters of the ``configure`` stage.  One, this section gets the list of
shell command *templates* for the ``configure`` setup stage:

.. literalinclude:: ../examples/git/git-setup.conf
   :language: ini
   :linenos:
   :lineno-match:
   :start-at: # Parameters for the `configure` stage.
   :end-before: # Options template for `configure`.
   :emphasize-lines: 4-8

Two, we add the parameter ``conf_opts_tpl`` with the *template* (please notice
the ``{prefix_dpath}`` placeholder) of options used in the :samp:`./configure
...` command template and referred to by placeholder
``%{configure.conf_opts_tpl}%``:

.. literalinclude:: ../examples/git/git-setup.conf
   :language: ini
   :linenos:
   :lineno-match:
   :start-at: # Parameters for the `configure` stage.
   :end-before: # Parameters for the `build` stage.
   :emphasize-lines: 10-11

Having the parameter in place with the templates for the shell commands needed
for the ``configure`` setup stage we can configure the definition for this
stage as value of parameter ``configure`` in the ``stages`` setup
configuration section.  According to our design from the beginning of this
section we use a call of setup step function
:py:func:`dusetup.step.run.run_cmds_from_cfg` to run all shell commands
needed.  Similar to :py:func:`dusetup.step.run.run_cmd_from_cfg` one
additional mandatory positional argument refers the fully qualified name of
the parameter holding the list of shell command templates:

.. literalinclude:: ../examples/git/git-setup.conf
   :language: ini
   :linenos:
   :lineno-match:
   :start-at: # Definitions of the setup stages.
   :end-before: # Definition of the `build` stage.
   :emphasize-lines: 7-8

And this is all we need to configure setup stage ``configure``!


-----------------------------
Configure the ``build`` stage
-----------------------------

For the ``build`` setup stage we have again to run a list of shell commands.
Hence we use as setup step function again good old friend
:py:func:`dusetup.step.run.run_cmds_from_cfg` from section
:ref:`git-configure_stage`.  The list specifying the shell commands to
run for the ``build`` stage we analogously configured as a parameter in a
section ``build`` dedicated to ``build`` stage parameters.  Since this
time we have no need for command *templates* but literal commands suffice, we
name the respective parameter ``cmds``.  This is all we need to know to
configure the ``build`` setup stage definition in section ``stages``:

.. literalinclude:: ../examples/git/git-setup.conf
   :language: ini
   :linenos:
   :lineno-match:
   :start-at: # Definitions of the setup stages.
   :end-before: # Definition of the `install` stage.
   :emphasize-lines: 10-11

Let us turn to the configuration of the list of the two shell commands to run
for ``build`` by the setup step function call just configured - as value of
parameter ``cmds`` in section ``build``.  Both shell commands have the form
:samp:`make -j <nr_cpus>` where ``<nr_cpus>`` stands for the number of CPU
cores available in parallel for the build(s).  We split this common option
part away again and move it into an ``build`` parameter of its own, say
``make_opts``.  This way we get for ``build`` parameter ``cmds``:

.. literalinclude:: ../examples/git/git-setup.conf
   :language: ini
   :linenos:
   :lineno-match:
   :start-at: # Parameters for the `build` stage.
   :end-before: # Options for the `make` commands.
   :emphasize-lines: 4-8

When specifying the value of the ``build`` parameter ``make_opts`` we meet a
new twist, since we do not want to hard-wire the umber of CPU cores available
in parallel for the build(s) into its value.  Instead, we let helper function
:py:func:`dusetup.conf.interpol.count_usable_cpus` determine this number.
Then we use another specific placeholder in the value of the ``make_opts`` to
use the value of that function call [#func_intp]_:

.. literalinclude:: ../examples/git/git-setup.conf
   :language: ini
   :linenos:
   :lineno-match:
   :start-at: # Parameters for the `build` stage.
   :end-before: # Parameters for the `install` stage.
   :emphasize-lines: 10-11

Voila - setup stage ``build`` is configured, too!


.. _git-install_stage:

-------------------------------------
Configure the ``install`` setup stage
-------------------------------------

To finish our example setup configuration for a *Git* setup, the final setup
stage ``install`` has to be configured.  To run its shell commands we utilize
setup step function :py:func:`dusetup.step.run.run_cmds_from_cfg` one more
time.  To keep things interesting we do not port command :samp:`mkdir -p
$PREFIX_DPATH && rm -rf $PREFIX_DPATH/*` into a shell command template, but
use :py:func:`dusetup.step.path.ensure_clean_dir` - a setup step function
exactly made for such a task.  It needs an additional positional argument - a
path (template) for the directory that should be ensured and empty.  Function
:py:func:`dusetup.step.path.ensure_clean_dir` has an additional keyword
argument ``verbose`` to control whether the actions done by its call are
shown.  We use this argument to force verbosity.  The remaining two shell
commands are again configured in a section named ``install`` - as list value
of parameter, say ``cmds_tpls``.

Let us implement this strategy.  We start with the definition of the two setup
step function calls to define the ``install`` stage in section ``stages``:

.. literalinclude:: ../examples/git/git-setup.conf
   :language: ini
   :linenos:
   :lineno-match:
   :start-at: # Definitions of the setup stages.
   :end-before: # Parameters for the `clean` stage.
   :emphasize-lines: 13-17

Last but not least we have to give the list of command templates as value of
parameter ``cmds_tpls`` in section ``install``:

.. literalinclude:: ../examples/git/git-setup.conf
   :language: ini
   :linenos:
   :lineno-match:
   :start-at: # Parameters for the `install` stage.
   :emphasize-lines: 4-8

With the completion of this last setup stage of our *Git* setup the content of
its setup configuration file :file:`git-setup.conf` is complete now, too.


.. _git-achieved:

What we have achieved
=====================

Now we have reproduced the complete setup configuration file
:file:`git-setup.conf` for our example *Git* setup:

.. literalinclude:: ../examples/git/git-setup.conf
   :language: ini
   :linenos:

The reader may ask what we have gained: compared to the few shell commands in
section :ref:`git-what_to_do` the setup configuration file looks rather
lengthy.  Well, first of all, we could considerably shorten it by removing the
stuff used to ease reading and understanding of :file:`git-setup.conf`, like
comments, or separate sections for parameters of different setup stages.

More importantly, using ``dusetup`` with a setup configuration file like
:file:`git-setup.conf` give us some other functionality for free.  Compared to
the lean shell commands in section :ref:`git-what_to_do`, we gain:

+ A proper commandline with options to tweak the setup process.

+ Automatic calculation of some setup parameters needed - like the version of
  the *Git* application, or the number of CPU cores that can be used in
  parallel for building.

+ Proper error handling: if any setup step fails, the setup process comes to
  halt with a proper error message.

Finally, to reap the fruits of our labor, let us use :file:`git-setup.conf` to
play a little bit with setting up Git from source.  For fun we prepare a
directory for playing with a brand-new copy the *Git* source from its Git
repository:

.. code-block:: bash
   :caption: Get a copy of the Git repository for *Git*.

   >>> mkdir ~/playtime
   >>> cd ~/playtime
   >>> git clone https://git.kernel.org/pub/scm/git/git.git git-devel
   >>> cd git-devel

For playing with ``dusetup`` we need this package and its dependencies to be
installed, and its command script with the same name resides into a directory
whose path is in the value of :envvar:`PATH`.  Alternatively, we can prepare a
suitable virtual environment for this.  First we set such an virtual
environment up, and install the dependencies of ``dusetup``:

.. code-block:: bash
   :caption: Installation of the dependencies for ``dusetup``

   >>> python3 -m venv ~/playtime/venv
   >>> source ~/playtime/venv/bin/activate
   >>> export GITLAB=git+https://gitlab.com/conlogic
   >>> pip install $GITLAB/dubasiclog.git
   >>> pip install $GITLAB/duconfigparser.git
   >>> pip install $GITLAB/duipoldict.git
   >>> pip install $GITLAB/duconfig.git

To install ``dusetup`` itself in this virtual environment, there are two
alternatives.  One, we can install ``dusetup`` like its dependencies:

.. code-block:: bash
   :caption: Plain installation of ``dusetup``

   >>> pip install $GITLAB/dusetup.git

Two, if we want to tinker with the code of ``dusetup``, we should get a clone
of its Git repository, and install ``dusetup`` editable from this clone:

.. _edit_dusetup:

.. code-block:: bash
   :caption: Editable installation of ``dusetup``

   >>> git clone https://gitlab.com/conlogic/dusetup.git ~/playtime/dusetup
   >>> pip install -e ~/playtime/dusetup

Let us copy :download:`git-setup.conf <../examples/git/git-setup.conf>` into
the :file:`~/playtime/git-devel` directory, too.  This way the setup
configuration file is automatically found by ``dusetup``.

With this preparation done, it's playtime.  Let us first explore the
``dusetup`` commandline:

.. _dusetup-help:

.. code-block:: console
   :caption: Commandline options for ``dusetup``

   >>> cd ~/playtime/git-devel
   >>> dusetup -h
       usage: dusetup [-h] [-V] [-d] [-c <cfg_fpath>] [-a <import_dpath>]
                      [-s <stages>] [-n] [-v <app_vers>] [-p <prefix_dpath>]
                      [-C [<cfg_fpath>]]

       Set up application git as defined by a setup configuration file.

       options:
         -h, --help            show this help message and exit
         -V, --version         show program's version number and exit
         -d, --debug           Enable debugging.
         -c <cfg_fpath>, --use-cfg-file <cfg_fpath>
                               Use setup configuration file with path
                               <cfg_fpath> instead of default
                               `/home/dirk/playtime/git-devel/git-setup.conf`.
         -a <import_dpath>, --add-import-path <import_dpath>
                               Add directory path <import_dpath> as additional
                               path to import Python modules or packages with
                               highest precedence (before those paths
                               configured in the used setup configuration file
                               and paths of the directories that are searched
                               for setup configuration files by default). This
                               option can be used more than once.
         -s <stages>, --stages <stages>
                               Use stages in comma-separated list <steps> to
                               set up the application (default: read from the
                               setup configuration file).
         -n, --no-perform      Do not perform setup stages but only show setup
                               parameters to be used.
         -v <app_vers>, --appl-version <app_vers>
                               Use version <app_vers> for the application
                               (default: `devel` or read from the setup
                               configuration file). An empty value is treated
                               as no version.
         -p <prefix_dpath>, --prefix <prefix_dpath>
                               Use prefix with directory path <prefix_dpath>
                               (default read from the setup configuration
                               file). A `{app_name_vers}` part (including the
                               curly braces) is handled as placeholder for
                               combined application name and version.
         -C [<cfg_fpath>], --export-cfg-file [<cfg_fpath>]
                               Export active setup configuration file as file
                               with path <cfg_fpath>, and exit. If <cfg_fpath>
                               is not given, the user setup configuration file
                               at `/home/dirk/.config/dusetup/git-setup.conf`
                               is used.

Now let us issue a complete setup for *Git* with the default parameter values.
This is as simple as:

.. code-block:: console

   >>> dusetup

Finally, let us play a little bit with the commandline options for
``dusetup``.  Maybe we want to suppress the application version component for
the installation prefix directory:

.. code-block:: console

   >>> dusetup -v ''

Or, we want to keep the application version ``<vers>``, but install into
directory with path :file:`~/git-<vers>`:

.. code-block:: console

   >>> dusetup -p ~/{app_name_vers}

And, last but not least, let us perform only the ``clean`` and ``configure``
setup stages:

.. code-block:: console

   >>> dusetup -s clean,configure

Please note that it is not strictly necessary to copy the *Git* setup
configuration file :file:`git-setup.conf` into the
:file:`~/playtime/git-devel` directory with the *Git* source code: we could
also use option ``-c`` / ``--use-cfg-file`` to specify that filepath instead.
For instance, let us assume  we use a virtual environment with an editable
installation of ``dusetup`` like described above at :ref:`edit_dusetup`.  This
installation points to a local copy of the Git repository for ``dusetup`` at,
say :file:`~/playtime/dusetup`.   We could then use :file:`git-setup.conf`
from this repository instead of a copy it in *Git* code directory.  A complete
build could the be issued by:

.. code-block:: console

   >>> dusetup -c ~/playtime/dusetup/examples/git/git-setup.conf

Please note that we could also copy :file:`git-setup.conf` into a
:file:`dusetup` of either the :file:`/etc/dusetup` or the :file:`~/.config`
directory as described in section :ref:`user_interface`.

This remark completes the tutorial for our *Git* setup example.


.. [#list_val] The form of list values also stems from package ``duconfig``:
               list values are delimited by ``[...]``, and multiple elements
               are separated by ``,``.  Please note that all lines but the
               first one of a list value have to be indented by the same
               amount - including the closing bracket, if it stands on a line
               of its own.

.. [#tmp_prefix] If we would set up Git for real we certainly would not
                 install it at a path like :file:`/tmp/git-<vers>`.  But this
                 path is a safe one to play with.

.. [#step_fct_call] You find all the glory details in the reference
                    documentation for function
                    :py:func:`dusetup.stage.perform_step`.  In particular, it
                    describes how setup step functions are called, and how a
                    setups step function call definition has to look like.

.. [#clean_fct] Please note the following syntactic details:

                + The fully-qualified setup step function name looks like
                  ``<mod_name>.<fct_name>`` for function named ``<fct_name>``
                  from module named ``<mod_name>``.

                + The fully-qualified parameter name for a parameter
                  ``<param>`` in section ``<sect>`` is ``<sect>.<param>``.

                + Additional arguments for a setup stage function call are
                  append to the function's name using ``:`` as separator.

.. [#calc_params] This is the task of function
                  :py:func:`dusetup.cmdline.calc_setup_params`.  This function
                  is called as part of the :py:func:`dusetup.cmdline.main`
                  function, and it calculates the initial setup parameters
                  from the parameters in the configuration file and the values
                  given as commandline options.  There is another function
                  :py:func:`dusetup.cmdline.add_std_install_dir_path` called
                  by :py:func:`dusetup.cmdline.main` that adds the standard
                  installation directory paths to the initial setup
                  parameters.  For details please consult the documentation of
                  the respective functions.

.. [#plh_params] To use a setup parameter value within a configuration
                 parameter value, the template value for the latter uses the
                 key of the former in setup parameter dictionaries enclosed by
                 ``{...}``.  Please note that the same mechanism is used for
                 the value of parameter ``prefix_dpath_tpl`` in configuration
                 section ``default``.

.. [#item_intp] The capability for value interpolation for configuration
                values comes from package ``duconfig``.  Here we have a
                specific form of value interpolation - the so-called *item
                interpolation*.  It uses a placeholders that are delimited by
                ``%{...}%`` to refer to the value of the configuration
                parameter whose fully qualified parameter name is enclosed.
                For details please have a look into the `duconfig repository`_
                or `duconfig documentation`_.

                The values of parameters whose values use value interpolation
                are already calculated *inside of the configuration* .
                Therefore they are *no* calculated parameter in the sense of
                point b in section :ref:`git-write_conf`.

.. [#func_intp] This is a second form of value interpolation provided by the
                ``duconfig`` package - the so-called *function call
                interpolation*.  It uses placeholders for (a form of) function
                calls that are delimited by ``%(...)%``.  Please note that the
                functions defined in module :py:mod:`dusetup.conf.interpol`
                are exactly those that can be used for function call
                interpolation in ``dusetup`` configuration files.


.. _duconfig repository: https://gitlab.com/conlogic/duconfig

.. _duconfig documentation: https://duconfig.readthedocs.io


..
 -----------------------------------------------------------------------------
 Local Variables:
 mode: rst
 ispell-local-dictionary: "en"
 End:
