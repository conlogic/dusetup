..
 =============================================================================
 Title          : Setup utilities

 Classification : reST text file

 Author         : Dirk Ullrich

 Date           : 2024-02-28

 Description    : API reference
 =============================================================================


.. _API-ref:

=============
API Reference
=============

In this section we provide a reference documentation for those Python entities
that are provided for ``dusetup`` users.  The documented entities are grouped
by the area of usage.


Configuration
=============

Package :py:mod:`dusetup.conf` deals with:

.. automodule:: dusetup.conf

It has the following attributes:

.. autodata:: dusetup.conf.APP_SEP
   :annotation:

and contains the following modules:

+ :py:mod:`dusetup.conf.config`,

+ :py:mod:`dusetup.conf.interpol`.


-------------------------------
Configuration files and objects
-------------------------------

Module in :py:mod:`dusetup.conf.config` is specialized to:

.. automodule:: dusetup.conf.config

Its functions for users are:

.. autofunction:: dusetup.conf.config.config_file_name

.. autofunction:: dusetup.conf.config.search_config_file

.. autofunction:: dusetup.conf.config.read_config_file

.. autofunction:: dusetup.conf.config.export_config_file

.. autofunction:: dusetup.conf.config.upd_config_sections


.. _cfg-interpol-ref:

------------------------------------------
Value interpolation in configuration files
------------------------------------------

Module :py:mod:`dusetup.conf.interpol` has the following purpose:

.. automodule:: dusetup.conf.interpol

It contains the following functions:

.. autofunction:: dusetup.conf.interpol.calc_app_version

.. autofunction:: dusetup.conf.interpol.calc_app_name

.. autofunction:: dusetup.conf.interpol.add_app_version

.. autofunction:: dusetup.conf.interpol.del_app_version

.. autofunction:: dusetup.conf.interpol.abspath

.. autofunction:: dusetup.conf.interpol.count_usable_cpus

All these functions are put into a list that is provided by the following
attribute:

.. autodata:: dusetup.conf.interpol.DEFAULT_CFG_FCTS
   :annotation:


.. _cmdline-ref:

Commandline
===========

Module :py:mod:`dusetup.cmdline` provides the following functionality:

.. automodule:: dusetup.cmdline

It defines the following attributes:

.. autodata:: dusetup.cmdline.STD_TOOL_DESC_TPL
   :annotation:

.. autodata:: dusetup.cmdline.SETUP_CONF_FNAME_TPL
   :annotation:

.. autodata:: dusetup.cmdline.SETUP_CONF_BASE_DPATHS
   :annotation:

.. autodata:: dusetup.cmdline.CFG_DEFAULT_SECT
   :annotation:

The functions targeted to users are:

.. autofunction:: dusetup.cmdline.main

.. autofunction:: dusetup.cmdline.calc_init_params

.. autofunction:: dusetup.cmdline.mk_cmdline_parser

.. autofunction:: dusetup.cmdline.read_config_file

.. autofunction:: dusetup.cmdline.calc_setup_params

.. autofunction:: dusetup.cmdline.add_std_install_dir_paths


Setup stages
============

Module :py:mod:`dusetup.stage` is dedicated to:

.. automodule:: dusetup.stage

Its attributes are:

.. autodata:: dusetup.stage.STAGES_CFG_SECT
   :annotation:

.. autodata:: dusetup.stage.RUN_START_TIME
   :annotation:

It defines the following functions for users:

.. autofunction:: dusetup.stage.perform_stages

.. autofunction:: dusetup.stage.perform_stage

.. autofunction:: dusetup.stage.perform_step


.. _step-function-ref:

Setup step functions
====================

Package :py:mod:`dusetup.step` is dedicated to:

.. automodule:: dusetup.step

It contains the following modules:

+ :py:mod:`dusetup.step.path`,

+ :py:mod:`dusetup.step.run`,

+ :py:mod:`dusetup.step.build`,

+ :py:mod:`dusetup.step.install`.


------------------
Path-related stuff
------------------

The purpose of module :py:mod:`dusetup.step.path` is:

.. automodule:: dusetup.step.path

The following functions are defined there:

.. autofunction:: dusetup.step.path.add_path_envvar

.. autofunction:: dusetup.step.path.ensure_clean_dir

.. autofunction:: dusetup.step.path.rm_item

.. autofunction:: dusetup.step.path.cpmv_item

.. autofunction:: dusetup.step.path.symlink_item

.. autofunction:: dusetup.step.path.symlink_items_of_dir


---------------------
Run external commands
---------------------

Module :py:mod:`dusetup.step.run` is meant for:

.. automodule:: dusetup.step.run

It provides the following functions:

.. autofunction:: dusetup.step.run.run_cmd_from_cfg

.. autofunction:: dusetup.step.run.run_cmds_from_cfg


-----------
Build items
-----------

Module :py:mod:`dusetup.step.build` is dedicated to:

.. automodule:: dusetup.step.build

It has the following attributes:

.. autodata:: dusetup.step.build.BUILD_KIND_INFO
   :annotation:

and the following functions for users:

.. autofunction:: dusetup.step.build.build_sect_items_from_cfg

.. autofunction:: dusetup.step.build.build_key_items_from_cfg


-------------
Install items
-------------

Module :py:mod:`dusetup.step.install` is dedicated to:

.. automodule:: dusetup.step.install

It has the following attributes:

.. autodata:: dusetup.step.install.INSTALL_KIND_TYPE_INFO
   :annotation:

.. autodata:: dusetup.step.install.INSTALL_KIND_ITEM_INFO
   :annotation:

and the following functions for users:

.. autofunction:: dusetup.step.install.install_sect_items_from_cfg

.. autofunction:: dusetup.step.install.install_key_items_from_cfg


Error handling
==============

The purpose of the module :py:mod:`dusetup.error` is:

.. automodule:: dusetup.error

It has the following functions:

.. autoclass:: dusetup.error.SetupError

.. autoclass:: dusetup.error.handle_errors


Generic helpers
===============

Package :py:mod:`dusetup.util` is meant for:

.. automodule:: dusetup.util

It contain the following modules:

+ :py:mod:`dusetup.util.path`,

+ :py:mod:`dusetup.util.run`,

+ :py:mod:`dusetup.util.install`,

+ :py:mod:`dusetup.util.elisp`,

+ :py:mod:`dusetup.util.misc`.


------------------
Path-related stuff
------------------

Module :py:mod:`dusetup.util.path` is dedicated to:

.. automodule:: dusetup.util.path

It provides the following functions, sorted by their areas:

+ Path components:

  .. autofunction:: dusetup.util.path.has_ext

  .. autofunction:: dusetup.util.path.add_prefix

+ Environment:

  .. autofunction:: dusetup.util.path.add_path_envvar

+ Directories:

  .. autofunction:: dusetup.util.path.ls_dir

  .. autofunction:: dusetup.util.path.ensure_dir

  .. autofunction:: dusetup.util.path.ensure_clean_dir

+ Removing:

  .. autofunction:: dusetup.util.path.rm_item

  .. autofunction:: dusetup.util.path.rm_items_of_dir

+ Copying and moving:

  .. autofunction:: dusetup.util.path.cpmv_file

  .. autofunction:: dusetup.util.path.cpmv_dir

  .. autofunction:: dusetup.util.path.cpmv_item

  .. autofunction:: dusetup.util.path.cpmv_items

  .. autofunction:: dusetup.util.path.cpmv_items_of_dir

  .. autofunction:: dusetup.util.path.cpmv_files_of_dir

+ Linking:

  .. autofunction:: dusetup.util.path.symlink_item

  .. autofunction:: dusetup.util.path.symlink_items

  .. autofunction:: dusetup.util.path.symlink_items_of_dir


---------------------
Run external commands
---------------------

Module's :py:mod:`dusetup.util.run` purpose is:

.. automodule:: dusetup.util.run

It has the following functions:

.. autofunction:: dusetup.util.run.run_cmd

.. autofunction:: dusetup.util.run.run_cmds


-----------
Build items
-----------

Module :py:mod:`dusetup.util.build` is dedicated to:

.. automodule:: dusetup.util.build

It provides functions to implement the item build mechanism:

.. autofunction:: dusetup.util.build.calc_kind_info

.. autofunction:: dusetup.util.build.build_kind_item_elements


-------------
Install items
-------------

Module :py:mod:`dusetup.util.install` is dedicated to:

.. automodule:: dusetup.util.install

It has the following functions to implement the item install mechanism:

.. autofunction:: dusetup.util.install.calc_std_install_dir_paths

.. autofunction:: dusetup.util.install.calc_kind_info

.. autofunction:: dusetup.util.install.install_kind_item_elements


-------------------
Documentation files
-------------------

Module's :py:mod:`dusetup.util.doc` purpose is:

If contains the following attributes:

.. autodata:: dusetup.util.doc.INFO_FEXT_RX
   :annotation:

.. autodata:: dusetup.util.doc.TEXI_OUT_FORMATS
   :annotation:

.. autodata:: dusetup.util.doc.INFO_DIR_FNAME
   :annotation:

.. automodule:: dusetup.util.doc

For the user it provides the following functions:

.. autofunction:: dusetup.util.doc.is_texi_file

.. autofunction:: dusetup.util.doc.is_info_file

.. autofunction:: dusetup.util.doc.texi2any_file

.. autofunction:: dusetup.util.doc.texi2any_files_of_dir

.. autofunction:: dusetup.util.doc.install_info_files_of_dir


-----------
ELisp files
-----------

The purpose of module :py:mod:`dusetup.util.elisp` is:

.. automodule:: dusetup.util.elisp

It provides the following user functions:

.. autofunction:: dusetup.util.elisp.is_elisp_src_file

.. autofunction:: dusetup.util.elisp.is_elc_file

.. autofunction:: dusetup.util.elisp.is_elisp_file

.. autofunction:: dusetup.util.elisp.mk_elc_file

.. autofunction:: dusetup.util.elisp.mk_elc_files_of_dir


------------
Python files
------------

The module :py:mod:`dusetup.util.python` is dedicated:

.. automodule:: dusetup.util.python

It defines the following functions for users:

.. autofunction:: dusetup.util.python.rel_python_site_dir

.. autofunction:: dusetup.util.python.compile_python_item


---------------------
Miscellaneous helpers
---------------------

Module :py:mod:`dusetup.util.misc` is for:

.. automodule:: dusetup.util.misc

It defines the following attributes:

.. autodata:: dusetup.util.misc.PART_SEP
   :annotation:

.. autodata:: dusetup.util.misc.NAME_VAL_SEP
   :annotation:

The following functions are available for users:

.. autofunction:: dusetup.util.misc.kind_item_elements_from_cfg_key

.. autofunction:: dusetup.util.misc.tokenize_item_element

.. autofunction:: dusetup.util.misc.tokenize_item_kind

.. autofunction:: dusetup.util.misc.tokenize_val

.. autofunction:: dusetup.util.misc.format_dict_str_vals

.. autofunction:: dusetup.util.misc.match_index

.. autofunction:: dusetup.util.misc.get_qualified_attr

.. autofunction:: dusetup.util.misc.add_import_dpaths

.. autofunction:: dusetup.util.misc.to_bool

.. autofunction:: dusetup.util.misc.to_tuple

.. autofunction:: dusetup.util.misc.to_int


..
 -----------------------------------------------------------------------------
 Local Variables:
 mode: rst
 ispell-local-dictionary: "en"
 End:
