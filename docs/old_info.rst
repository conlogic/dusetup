..
 =============================================================================
 Title          : Setup utilities

 Classification : reST text file

 Author         : Dirk Ullrich

 Date           : 2022-04-27

 Description    : Information for old versions of the distribution package.
 =============================================================================


=============================================
Information for older versions of ``dusetup``
=============================================

.. toctree::
   :maxdepth: 1

   old_changelog


..
 -----------------------------------------------------------------------------
 Local Variables:
 mode: rst
 ispell-local-dictionary: "en"
 End:
