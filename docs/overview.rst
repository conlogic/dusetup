..
 =============================================================================
 Title          : Setup utilities

 Classification : reST text file

 Author         : Dirk Ullrich

 Date           : 2022-05-30

 Description    : Overview of the distribution package.
 =============================================================================


==================================
General information on the package
==================================

.. toctree::
   :maxdepth: 1

   readme

   license

   changelog


..
 -----------------------------------------------------------------------------
 Local Variables:
 mode: rst
 ispell-local-dictionary: "en"
 End:
