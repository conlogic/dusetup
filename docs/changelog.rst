..
 =============================================================================
 Title          : Setup utilities

 Classification : reST text file

 Author         : Dirk Ullrich

 Date           : 2022-04-27

 Description    : Change log for the distribution package.
 =============================================================================


.. include:: ../CHANGELOG.rst


..
 -----------------------------------------------------------------------------
 Local Variables:
 mode: rst
 ispell-local-dictionary: "en"
 End:
