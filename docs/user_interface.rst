..
 =============================================================================
 Title          : Setup utilities

 Classification : reST text file

 Author         : Dirk Ullrich

 Date           : 2022-08-21

 Description    : User interface for the distribution package.
 =============================================================================


.. _user_interface:

==================
The user interface
==================

As already mentioned in :ref:`read-me`, there are three ways to utilize
``dusetup`` for an application's setup.  These three ways correspond to two
different ways to interact with ``dusetup``.  For each of these ways we
suppose that we want to set up an application with name, say ``<app_name>``,
and  with version ``<app_vers>``.

1. Use ``dusetup`` itself as commandline setup tool solely with an own setup
   configuration file.

   Package :py:mod:`dusetup` has to be called with the application's code as
   working directory that is properly named as :file:`<app_name>-<app_vers>`.
   [#calc_app_params]_  Furthermore an appropriate setup configuration file
   for the application's setup exists.  If its path is not given by
   commandline option ``-c`` / ``--use-cfg-file`` with an arbitrary path, the
   tool search for either a setup configuration file with name
   :file:`<app_name>-setup.conf` or, preferably,
   :file:`<app_name>-<app_vers>-setup.conf` in the following directories:

   + the current working directory,

   + below the user's configuration directory,

   + below the system configuration directory.

   Let us look at this for the *Git* example contained in the Git repository
   for ``dusetup``.  This example consists of a setup configuration file
   :download:`git-setup.conf <../examples/git/git-setup.conf>` to set up Git
   from source.  We assume that the working directory contains a copy of the
   source code for *Git* of version, say ``<vers>`` and the name of the
   directory is file:`git-<vers>`. [#app_case]_  If we now call ``dusetup``
   with arbitrary options ``<options>`` by

   .. code-block:: console

      >>> dusetup [<options>]

   or

   .. code-block:: console

      >>> python3 -m dusetup [<options>]

   the tool looks for an appropriate setup configuration file for Git at the
   following places (in that order):

   + :file:`git-<vers>-setup.conf` in the working directory,

   + :file:`git-<vers>-setup.conf` in subdirectory :file:`dusetup` of the
     user's configuration directory,

   + :file:`git-<vers>-setup.conf` in the :file:`/etc/dusetup` directory,

   + :file:`git-setup.conf` in the working directory;

   + :file:`git-setup.conf` in subdirectory :file:`dusetup` of the user's
     configuration directory,

   + :file:`git-setup.conf` in the :file:`/etc/dusetup` directory.

   This can be overwritten: if we have a copy of :file:`git-setup.conf` at,
   say :file:`/tmp/git-setup.conf` one can use, for instance:

   .. code-block:: console

      >>> dusetup -c /tmp/git-setup.conf

   To get an overview of the available ``dusetup`` options, and of the
   behavior of ``dusetup``, one can use the ``-h`` / ``--help`` option:

   .. code-block:: console

      >>> dusetup -h

   Below, at :ref:`dusetup-help`, the output of this command is shown for a
   ``dusetup`` the use case of a :file:`git-setup.conf` copy in the working
   directory.

   For details please consult section :ref:`API-ref`.  The information most
   relevant for writing setup configuration files can be found in sections
   :ref:`cfg-interpol-ref` and :ref:`step-function-ref`.

2. Use ``dusetup`` itself as commandline setup tool, with an an own setup
   configuration file, together with own Python modules or packages that can
   be imported.

   This way of usage extends way 1 from above by using own Python modules or
   packages, too, with own code used for the application's setup.  Hence
   :py:mod:`dusetup` has to be called again with the application's code as
   working directory, exactly like for way 1.  Furthermore the handling of
   the setup configuration file is identical to way 1, too.

   But what's about the own Python modules or packages?  First of all,
   ``dusetup`` can use code from any Python module or package as building
   blocks for configured setup steps if it can import this Python module or
   package.  If does such imports automatically, if the respective Python
   module or package lives in a directory whose path is a Python import path.
   Here ``dusetup`` uses a bunch of additional import paths beside the
   standard ones, with an higher priority of the former than the latter.
   These additional import paths are, sorted by increasing priority:

   + the paths of directories that are searched for setup configuration files,
     as described above for way 1;

   + a list of directories paths given as value of parameter ``import_dpaths``
     in the default section of the used configuration file, if this parameter
     is used there;

   + directory paths given by commandline option ``-a`` /
     ``--add-import-dpath``.  By the way, this option can be used multiple
     times.

   Let us demonstrate this for our *Idris 2* example formed by configuration
   file :download:`idris2-setup.conf <../examples/idris2/idris2-setup.conf>`
   together with Python module :py:mod:`idris2_setup` in file
   :download:`idris2_setup.py <../examples/idris2/idris2_setup.py>`.  It
   provides a setup for the dependently typed functional programming language
   *Idris 2* [#more_idris2]_.  To call ``dusetup`` for this example we suppose
   that the working directory contains the source code for *Idris 2* of
   version, say ``<vers>`` and the name of the directory something like
   file:`idris2-<vers>`. [#app_case]_  Calling ``dusetup`` for this *Idris 2*
   example is similar to calling ``dusetup`` for the *Git* example - with two
   differences:

   + The setup configuration file it is searched for is either
     :file:`idris2-<vers>-setup.conf` or :file:`idris2-setup.conf`, where the
     former is the preferred one.

   + We have to take care that the directory path of :file:`idris2_setup.py`
     is a Python import path for ``dusetup``.

   The latter point can be accomplished in various ways:

   + File :file:`idris2_setup.py` live in the same directory like
     :file:`idris2-setup.conf`, and :file:`idris2-setup.conf` is automatically
     be found as ``dusetup`` configuration file, or the path of the latter
     file is given as ``dusetup`` commandline option - like:

     .. code-block:: console

        >>> dusetup -c /tmp/git-setup.conf

   + File :file:`idris2_setup.py` has an arbitrary directory, and the path of
     this directory is given as ``dusetup`` commandline option - like:

     .. code-block:: console

        >>> dusetup -a /tmp

     assuming that there is a copy of :file:`idris2_setup.py` in :file:`/tmp`.
     Alternatively, one could add a parameter to :file:`idris2-setup.conf`
     like:

     .. code-block:: ini

        [default]

        import_dpaths = [/tmp]

3. Use ``dusetup`` for building blocks of a commandline setup tool:

   In this case you have to import, in the modules or package of the
   respective commandline setup tool, the ``dusetup`` modules you need.
   Section :ref:`API-ref` explains the :py:mod:`dusetup` package structure and
   contains a complete reference for all attributes, functions and classes
   provided by ``dusetup``.


.. [#calc_app_params] The special form - ``<app_name>-<app_vers>`` - of the
                      working directory containing the code of the application
                      to set up is used to automatically calculate both the
                      application's name and its version.  Equivalently, one
                      can use a special scheme for the directory path of the
                      working directory with the application's code: its
                      directory name is ``<app_vers>`` (and contains no
                      ``-``), and the name of its parent's parent directory is
                      ``<app_name>``.

.. [#app_case] Please note that you can also use uppercase letters for the
               application's name.

.. [#more_idris2] Section :ref:`read-me` has more information on *Idris 2*,
                  together with some links for additional information.


..
 -----------------------------------------------------------------------------
 Local Variables:
 mode: rst
 ispell-local-dictionary: "en"
 End:
