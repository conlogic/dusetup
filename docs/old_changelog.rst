..
 =============================================================================
 Title          : Setup utilities

 Classification : reST text file

 Author         : Dirk Ullrich

 Date           : 2022-04-27

 Description    : Old change log for the distribution package.
 =============================================================================



==========================
Changelog (older versions)
==========================


Version 0.19.dev4 (2020-04-25 - discontinued)
=============================================

Milestone dev4 for version 0.19:

- Make the calculation of the program name in :py:mod:`dusetup.cmdline` more
  robust.

- Restore the tutorial to write a ``dusetup``-based setup tool, and rework if
  completely.  Thanks to Jens Rapp for many helpful suggestions and
  discussions.


Version 0.19.dev3 (2020-04-24)
==============================

Milestone dev3 for version 0.19:

- Refactor the stuff for the :py:func:`main` function of :py:mod:`dusetup`.

- Automatically create a wrapper command script to call :py:mod:`dusetup`, and
  simplify the documentation accordingly.

- Fix found glitches, including a grave bug in the :py:func:`main` function of
  :py:mod:`dusetup` that prevents to call :py:mod:`dusetup`.


Version 0.19.dev2 (2020-04-23)
==============================

Milestone dev2 for version 0.19:

- Review the whole documentation (included documentation strings and comments
  in the code) to ensure terminological consistency, fix meanings and some
  formulations, typos, and grammatical glitches.

- Improve some terminology and naming:

  - Context manager :py:class:`handle_error` is renamed to
    :py:class:`handle_errors` and has now initialization argument
    ``catch_errors`` instead of ``raise_exception`` with inverted meaning.

  - Use "perform setup stage(s)" instead of "run setup stage(s)"; therefore
    function :py:func:`run_stages` is renamed to :py:func:`perform_stages`.

  - Attributes :py:data:`PARAM_PART_SEP` / :py:data:`PARAM_NAME_VAL_SEP` from
    module :py:mod:`dusetup.helper.misc` are renamed to py:data:`PART_SEP` /
    :py:data:`NAME_VAL_SEP`.


Version 0.19.dev1 (2020-04-22)
==============================

Milestone dev1 for version 0.19:

- Add a :py:func:`main` function for ``dusetup``-based setup tools with
  pluggable components.

- Add some standard setup stage functions:

  - :py:func:`step.function.run_command[s]` to run (a list of) command(s).

- Make package :py:mod:`dusetup` callable using a given configuration file
  that uses only standard setup stage functions.

- Add a simple configuration file example to set up Git the can be used by
  calling :py:mod:`dusetup` alone.

- Refactor function :py:func:`read_config_file` to take the path of the
  default configuration file as argument.

- Generalize function :py:func:`run_stages` (formerly: :py:func:`run_steps`)
  to pass additional arguments when calling the stage functions.

- Add module :py:mod:`dusetup.helper.misc` with unspecific helper functions to
  be used elsewhere.

- Start complete rework of the package structure.

- Add a tutorial for using ``dusetup`` itself as setup tool.

- Start to add reference documentation for those Python stuff intended for
  ``dusetup`` users.

- Change terminology: former "setup steps" become "setup stages".

- Use more Sphinx-specific markup in the documentation.


Version 0.18 (2022-04-12)
=========================

- Add a real Sphinx-based documentation, especially for the usage of the
  package.  For now the latter contains a tutorial implementation of a
  ``dusetup``-based setup tool.

- Move function :py:func:`add_version` from :py:mod:`dusetup.path` generalized
  as :py:func:`add_app_version` to :py:mod:`dusetup.config`, and use it for a
  consistent application version suffix throughout.

- Add a bunch of path functions to :py:mod:`dusetup.install`:

  - :py:func:`bin_dir_path` / :py:func:`lib_dir_path` /
    :py:func:`share_dir_path` / :py:func:`doc_dir_path` for the path of the
    binary / library / shared / documentation directory,

  - :py:func:`bin_file_path` / :py:func:`lib_item_path` /
    :py:func:`share_item_path` / :py:func:`doc_item_path` for the path of a
    binary file / library item / share item / documentation item.

- Add function :py:func:`install_lib_item` to :py:mod:`dusetup.install` to
  install a library item.

- Add function :py:func:`add_path_envvar` to :py:mod:`dusetup.config` to add
  a path to an environment variable whose value is a search path.

- Add functions :py:func:`symlink_item`, :py:func:`symlink_items` and
  :py:func:`symlink_dir_items` to :py:mod:`dusetup.path` to symlink files or
  directories.

- Add function :py:func:`run_command` to module :py:mod:`step` to run a single
  commands given in the setup configuration.

- Function :py:func:`calc_setup_params` stores combined application name and
  version, too.

- Functions :py:func:`run_cmd` and :py:func:`run_cmds` from module
  :py:mod:`command` and :py:func:`run_command` and :py:func:`run_commands`
  :py:mod:`step` return their results.

- Functions :py:func:`run_command` / :py:func:`run_commands` from module
  :py:mod:`step` pass additional keyword arguments to the used functions
  :py:func:`run_cmd` / :py:func:`run_cmds` from module :py:mod:`command`.

- Fix found glitches and typos.

- Remove leftovers from an old rework.


Version 0.17 (2022-04-03)
=========================

- Radically simplify setup step logic: use a tailored function for every
  configuration step instead of configured list of instructions.

- Changes for the generic functions in Module :py:mod:`step` to be used in
  setup steps:

  - New function :py:func:`run_commands` to run a list of commands given in
    the setup configuration.

  - New function :py:func:`rm_items` to remove a list of files or directories
    given in the setup configuration.

  - Changed arguments for :py:func:`install_items`.

  - Changed arguments for :py:func:`apply_patches`.

  - Changed arguments for :py:func:`autotools_configure`.

- Move stuff from module :py:mod:`dusetup.elisp` that is specific for Emacs
  packages to package :py:mod:`duepkgsetup` meant to be a setup tool for Emacs
  packages.

- Let error handler :py:class:`handle_error` from module
  :py:mod:`dusetup.error` ignore all non-error exceptions.

- Let package :py:mod:`dusetup` not directly export :py:exc:`SetupError`.

- Run setup steps always in their configured order.


Version 0.16.1 (2022-03-01)
===========================

- Fix for reading a non-default configuration file.


Version 0.16 (2022-03-01)
=========================

- Complete rework of the :py:mod:`install` and :py:mod:`step` modules to
  simplify both a lot.  By this tools that are based on ``dusetup`` can use
  its stuff unmodified in most cases.

- Make the structure for more heterogeneous module more explicit.

- Module :py:mod:`step` now provides the following generic functions to be
  used in step steps:

  - :py:func:`apply_patches` to apply a list of patch files.

  - :py:func:`autotools_configure` to call an Autotools-based
    :program:`configure`.

  - :py:func:`ensure_clean_destination` to ensure a clean destination
    directory for the installation.

  - :py:func:`install_items` to install configured items.

- Require version >= 3.4 for ``duconfig``.
  This allows to add ``'('`` / ``')'`` as further other begin / end
  delimiters.  This way we can use strings that represent function calls
  within list elements of configuration files, too.

- Generalize function :py:func:`add_version` to take both ``None`` and an
  empty string as no version.

- Fix parameter name for application version in function
  :py:func:`calc_default_param_vals`.

- Remove function :py:func:`join_list` since it not used at the moment.

- Add function :py:func:`compile_python_item` to byte-compile a Python module
  or package.


Version 0.15 (2022-02-19)
=========================

- New function :py:func:`upd_config_sections` to update the sections of one
  configuration by another configuration.

- An alternative configuration updates the standard configuration instead of
  overwriting it.

- Use an empty string (instead of ``None``) for a missing application version.

- Fix glitches found.


Version 0.14 (2022-02-19)
=========================

- Require version >= 3.3 for ``duconfig``.

- Pack configuration-related data in single dictionaries for function
  argument and return values.

- Move reading of the configuration file from :py:func:`get_default_params`
  into a :py:mod:`config` function :py:func:`read_config_file`, and rename
  the remaining function to :py:func:`calc_default_param_vals`.

- Clarify usage of the prefix path (template).

- Fix the calculation of the setup parameters, and read the used configuration
  only once.

- Integrate configuration export into :py:func:`calc_setup_params`, and retire
  :py:func:`export_config_file` from :py:mod:`cmdline`.


Version 0.13 (2022-02-16)
=========================

- Add an optional argument `version` for verbosity to the functions with
  removal functionality, namely:

  - :py:func:`rm_item` and :py:func:`rm_items`,

  - :py:func:`rm_files` and :py:func:`rm_dir_files`,

  - :py:func:`ensure_clean_dir`.

- Add an optional argument `version` for verbosity to the functions to run
  commands instead being always verbose, namely to:

  - :py:func:`run_cmd` and :py:func:`run_cmds`.

- Let :py:func:`run_steps_for_cfg` print essential parameters before running
  the setup steps.


Version 0.12 (2022-02-13)
=========================

- New module :py:mod:`patch` with utilities related to patches:

  - :py:func:`apply_patches` to apply a series of patches.

- Functions :py:func:`run_cmd` and :py:func:`run_cmds` print the command(s)
  they run.

- New function :py:func:`rm_items` to remove a list of items given by their
  paths.


Version 0.11 (2022-02-11)
=========================

- Add an option to :py:class:`handle_error` to re-raise an error exception
  instead of only printing it.

- Add a ``-d`` / ``--debug`` option to :py:func:`mk_cmdline_parser` to set a
  switch for debugging.

- Fix a bug in :py:func:`calc_app_version`.


Version 0.10 (2022-02-09)
=========================

- Improve function to count the number of usable CPUs, and rename it
  :py:func:`nr_of_cpus` -> :py:func:`count_usable_cpus` to reflect its proper
  purpose.

- New module :py:mod:`cmdline` with some generic functions to help creating
  commandline processing for setup tools:

  - :py:func:`get_default_params`: get default values for commandline
    parameters based on a configuration file.

  - :py:func:`mk_cmdline_parser`: create a commandline parser with typical
    options used for setup tools.

  - :py:func:`export_config_file` to export the configuration file.

  - :py:func:`calc_setup_params`: calculate the common parameters used for the
    setup steps.

- Clarify and unify the module documentation strings.


Version 0.9 (2022-02-08)
========================

- Generalize function :py:func:`calc_app_version` to use only parts of a
  calculated version.

- Generalize function :py:func:`calc_app_name` to optionally only use
  lowercase letters.

- Add  some functions meant to used for value interpolation in configuration
  files:

  - function :py:func:`join_list` to join the elements of a list to a string;

  - function :py:func:`nr_of_cpus` to count the number of CPUs.


Version 0.8 (2022-02-02)
========================

- Use term "application version" throughout; especially rename former function
  :py:func:`calc_version` to :py:func:`calc_app_version`.

- Add a function :py:func:`calc_app_name` to calculate an application's name.

- Make functions :py:func:`run_steps` and :py:func:`install_step_for_cfg` more
  generic.

- Let error handler :py:class:`handle_error` pass a :py:exc:`SystemExit`.


Version 0.7 (2022-01-31)
========================

- Add function :py:func:`run_cmds` to run a sequence of commands.

- Add generic function :py:func:`run_steps_for_cfg`, together with helper, to
  run configured setup steps.

- Add generic function :py:func:`install_step_for_cfg`, together with helpers,
  to run a configured install step.

- Unify the calling of all `install_{x}_file` / `install_{x}_item` functions.


Version 0.6.1 (2022-01-29)
==========================

- Make :py:func:`python_site_dir` more robust.

- Fix :py:func:`add_version` for paths with directories.

- Export both :py:func:`install_bash_file` and :py:func:`install_zsh_file`
  properly.


Version 0.6 (2022-01-28)
========================

- Use a consistent naming convention for the modules:

  - :py:mod:`cmds` -> :py:mod:`command`,

  - :py:mod:`conf` -> :py:mod:`config`,

  - :py:mod:`docs` -> :py:mod:`doc`,

  - :py:mod:`epkg` -> :py:mod:`elisp`,

  - :py:mod:`paths` -> :py:mod:`path`.

- Add optional arguments for permissions to the `cpmv_*` functions.

- Add functions for paths to add a prefix directory and a version suffix.

- New functions to install files or directories into standard directory paths,
  namely:

  - binary files,

  - shared files and directories,

  - documentation files and directories,

  - Python modules and packages,

  - Vim files and Emacs ELisp site files,

  - Bash and Zsh completion files.

- New function to byte-compile files, namely:

  - Emacs ELisp site files,

  - Python modules and packages.

- Use own :py:func:`run_cmd` to run external commands.

- Fix found documentation glitches.


Version 0.5.1 (2022-01-23)
==========================

- Fix error handling by :py:class:`handle_error` in :py:mod:`error`:

  - Always set its return code attribute, even if no error had occurred.

  - Print the exception name in the error message, too, if an exception has
    been raised.


Version 0.5 (2022-01-22)
========================

- Add basic error handling in :py:mod:`error` via exceptions.

- Add all remaining paths functions for single items to the API - namely:

  - :py:func:`rm_item`,

  - :py:func:`cpmv_item`,

  - :py:func:`cpmv_file`.

- Add function :py:func:`rm_files` to the API, too.

- Remove function :py:func:`rm_dir_items`.

- Add a simple function :py:func:`run_cmd` to run external commands.

- Add a simple function :py:func:`calc_version` to calculate the version of
  source code in a directory.


Version 0.4 (2022-01-18)
========================

- Add new copy/move functions for both file and directory items:

  - :py:func:`cpmv_items`, and base :py:func:`cpmv_files` on it.

  - :py:func:`cpmv_dir_items`, and base :py:func:`cpmv_dir_files` on it.

- Use :py:func:`shutil.copy2` as default file copy function for all `cpmv_*`
  functions.

- Rename the some of the removal functions, namely:

  - :py:func:`rm_all` -> :py:func:`rm_item`,

  - :py:func:`rm_dir_all` -> :py:func:`rm_dir_items`.

- Fix typos found.


Version 0.3 (2022-01-17)
========================

- Split former package ``duepkgutils`` into package ``dusetup`` with generic
  setup tools and package ``duepkgsetup`` with setup tools for Emacs packages.

- Fix typos found.


Version 0.2.2 (2022-01-11)
==========================

- Include functions :py:func:`ls_dir` and :py:func:`ls_dir_files` into the
  package's API.

- Fix formatting bugs in the ``lisp-misc-texi`` template.


Version 0.2.1 (2022-01-10)
==========================

- Fix a quoting error in the templates for the Emacs package setup tools.


Version 0.2 (2022-01-10)
========================

- Generalize a bunch of functions working with entries in or below a directory
  for a given path allow each of these cases controlled by an additional
  argument ``recursive``.  These functions are:

  - :py:func:`ls_dir`,

  - :py:func:`ls_dir_files`,

  - :py:func:`rm_dir_files`,

  - :py:func:`cpmv_dir_files`,

  - :py:func:`mk_elc_dir_files`.

- Generalize function :py:func:`paths.cpmv_files` use different destination
  directories relative to a given directory path.

- Add functions for base Texinfo support - namely:

  - :py:func:`is_texi_file`,

  - :py:func:`is_info_file`,

  - :py:func:`texi2any_file`,

  - :py:func:`texi2any_dir_files`,

  - :py:func:`install_info_dir_files`.

- Add support to set up Emacs packages of the following types:

  - ``lisp-misc-texi``:
    An Emacs package containing ELisp, miscellaneous and Texinfo files.

- Fix terminology leftovers.


Version 0.1 (2022-01-03)
========================

Initial version.  Support to set up Emacs packages of the following types:

- ``lisp``:
  An Emacs package containing ELisp files.

- ``lisp-misc``:
  An Emacs package containing ELisp and miscellaneous files.


..
 -----------------------------------------------------------------------------
 Local Variables:
 mode: rst
 ispell-local-dictionary: "en"
 End:
