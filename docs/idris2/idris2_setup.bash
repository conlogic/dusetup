#!/bin/bash

# =========================
# Set up Idris2 from source
# =========================

# Parameters
# ==========

# Application name.
APP_NAME='idris2'
# Application version.
APP_VERS="${APP_VERS:-devel}"
# Combined application namen and version.
APP_NAME_VERS="${APP_NAME}-$APP_VERS"
# Path for Idris2 installation prefix directory.
PREFIX_DPATH="${PREFIX_DPATH:-/tmp/$APP_NAME_VERS}"

# Bootstrap directory path.
BOOT_DPATH="$(pwd)/bootstrap/$APP_NAME_VERS"
# Add bootstrapped bin directory path to the value of `PATH`.
boot_bin_dpath="${BOOT_DPATH}/bin"
export PATH="$boot_bin_dpath:$PATH"
# Add bootstrapped lib directory path to the value of `LD_LIBRARY_PATH`.
boot_lib_dpath="${BOOT_DPATH}/lib"
export LD_LIBRARY_PATH="$boot_lib_dpath:$LD_LIBRARY_PATH"

# Scheme backend to be used.
SCHEME=chez

# Library directory path of installed Idris 2.
LIB_DPATH="$PREFIX_DPATH/lib/$APP_NAME_VERS"
# Relative path for Sphinx documentation directory.
REL_DOCS_DPATH='docs'
# Relative path for a Sphinx documentation directory that should exist to
# prevent a Sphinx warning.
REL_DOCS_STATIC_DPATH="$REL_DOCS_DPATH/source/_static"

# Binary directory path of installed Idris 2.
BIN_DPATH="$PREFIX_DPATH/bin"
# FHS command file path of installed Idris 2.
FHS_CMD_FPATH="$BIN_DPATH/$APP_NAME_VERS"
# Raw command file path of installed Idris 2.
RAW_CMD_FPATH="$LIB_DPATH/bin/$APP_NAME"

# A directory path to be removed of installed Idris 2.
LIB_DEL_DPATH="$LIB_DPATH/lib"
# Command to determine the raw library directory path of installed Idris 2.
RAW_LIBDIR_CMD="$FHS_CMD_FPATH --libdir"

# Documentation directory path of installed Idris 2.
DOC_DPATH="$PREFIX_DPATH/share/doc/$APP_NAME_VERS"
# Relative path for a Sphinx documentation directory with built Idris 2
# application documentation.
REL_DOCS_BUILD_DPATH="$REL_DOCS_DPATH/build/html"
# FHS application documentation directory path of installed Idris 2.
FHS_APP_DOC_DPATH="$DOC_DPATH/application"
# FHS library documentation directory path of installed Idris 2.
FHS_LIB_DOC_DPATH="$DOC_DPATH/libs"

# Functions
# =========

clean_step() {
    echo '======Clean======'
    make distclean || return 1
    rm -rf "$PREFIX_DPATH"/* || return 2
}

bootstrap_step() {
    echo '======Bootstrap======'
    echo '------Cleanup-------'
    rm -rf "$BOOT_DPATH" || return 1

    echo '------Bootstrap application and libraries------'
    make bootstrap SCHEME="$SCHEME" PREFIX="$BOOT_DPATH" || return 2

    echo '------Install bootstrapped application and libraries------'
    make install PREFIX="$BOOT_DPATH" || return 3

    return 0
}

build_step() {
    echo '======Build======'
    echo '------Cleanup-------'
    make clean || return 1

    echo '------Build application and libraries------'
    make all PREFIX="$LIB_DPATH" || return 2

    echo '------Build application documentation------'
    mkdir -p "$REL_DOCS_STATIC_DPATH" || return 3
    make -C "$REL_DOCS_DPATH" html || return 4

    return 0
}

install_app_step() {
    echo '======Install application======'
    echo '------Install application files------'
    make install-idris2 \
         PREFIX="$LIB_DPATH" IDRIS2_PREFIX="$LIB_DPATH" || return 2
    make install-support \
         PREFIX="$LIB_DPATH" IDRIS2_PREFIX="$LIB_DPATH" || return 3

    echo '------Create command link------'
    local rel_raw_cmd_fpath=$(
        realpath "$RAW_CMD_FPATH" --relative-to "$BIN_DPATH")
    mkdir -pv "$BIN_DPATH" || return 4
    ln -sfv "$rel_raw_cmd_fpath" "$FHS_CMD_FPATH" || return 5

    return 0
}

install_lib_step() {
    echo '======Install library======'
    echo '------Install library files------'
    make install-with-src-libs \
         PREFIX="$LIB_DPATH" IDRIS2_PREFIX="$LIB_DPATH" || return 1

    echo '------Install application API------'
    make install-with-src-api \
         PREFIX="$LIB_DPATH" IDRIS2_PREFIX="$LIB_DPATH" || return 2

    echo '------Clean up old shared library stuff------'
    rm -rfv "$LIB_DEL_DPATH" || return 3

    echo '------Link application libraries------'
    # Raw library directory path of installed Idris 2.
    local raw_lib_dpath=$($RAW_LIBDIR_CMD)
    # Raw OS library directory path of installed Idris 2 .
    local raw_os_lib_dpath="$raw_lib_dpath/lib"
    local l path_l rel_path_l
    for l in $(ls -1 "$raw_os_lib_dpath"); do
        path_l="$raw_os_lib_dpath/$l"
        rel_path_l=$(realpath "$path_l" --relative-to "$LIB_DPATH")
        ln -sfv "$rel_path_l" "$LIB_DPATH" || return 4
    done

    return 0
}

install_doc_step() {
    echo '======Install documentation======'
    mkdir -pv "$DOC_DPATH" || return 1
    echo '------Install application documentation------'
    cp -rv "$REL_DOCS_BUILD_DPATH" "$FHS_APP_DOC_DPATH" || return 2

    echo '------Install library documentation------'
    make install-libdocs PREFIX="$LIB_DPATH" || return 3
    # Raw library directory path of installed Idris 2.
    local raw_lib_dpath=$($RAW_LIBDIR_CMD)
    # Raw libary documentation directory path of installed Idris 2.
    local raw_lib_doc_dpath="$raw_lib_dpath/docs"
    mv -v "$raw_lib_doc_dpath" "$FHS_LIB_DOC_DPATH" || return 4

    return 0
}

# Doing
# =====

case "$1" in
    clean)
        clean_step
        ;;
    bootstrap)
        bootstrap_step
        ;;
    build)
        build_step
        ;;
    install-app)
        install_app_step
        ;;
    install-lib)
        install_lib_step
        ;;
    install-doc)
        install_doc_step
        ;;
    all)
        bootstrap_step && build_step && \
            install_app_step && install_lib_step && install_doc_step
        ;;
esac
