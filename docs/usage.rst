..
 =============================================================================
 Title          : Setup utilities

 Classification : reST text file

 Author         : Dirk Ullrich

 Date           : 2022-08-14

 Description    : Usage of the distribution package.
 =============================================================================


======================
How to use ``dusetup``
======================

.. toctree::
   :maxdepth: 2

   user_interface

   git_tutorial

   idris2_tutorial

..
 -----------------------------------------------------------------------------
 Local Variables:
 mode: rst
 ispell-local-dictionary: "en"
 End:
