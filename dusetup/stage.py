# ============================================================================
# Title          : Setup utilities
#
# Classification : Python module
#
# Author         : Dirk Ullrich
#
# Date           : 2024-02-27
#
# Description    : See documentation string below.
# ============================================================================

'''
Utilities for setup stages.
'''


import time
import sys

from .util import misc as util_misc
from . import error


# Parameters
# ==========

#: Configuration section defining the setup stage functions.
STAGES_CFG_SECT = 'stages'

#: Time (in seconds) before performing the first setup stage.
RUN_START_TIME = 3.0


# Functions
# =========


def perform_stages(init_params,
                   pre_stage='pre', post_stage='post',
                   stages_cfg_sect=STAGES_CFG_SECT):
    '''
    Perform setup stages.

    These setup stages are defined by setup parameter dictionary
    `init_params`, or simply show setup parameters from `init_params`.  For
    the content of `init_params`, please consult the documentation of
    :py:func:`dusetup.cmdline.calc_setup_params`.

    If the setup stages should be performed, this is done in the following
    order:

    a. Firstly, if dummy setup pre-stage with name `pre_stage` is defined, it
       is performed first.

    b. Every `init_params` setup stage, i.e. element of
       ``init_params['stages']``, is performed in the given order.

    c. Finally, if dummy setup post-stage with name `post_stage` is defined,
       it is performed last.

    Every setup stage is performed with a dictionary of the current setup
    parameters.  This current setup parameter dictionary is either
    `init_params` (without the ``'stages'`` and ``'perform_stages'`` items)
    for the first setup stage to perform, or the setup parameter dictionary
    returned by performing the previous setup stage.

    To perform a setup stage its definition looked up in section for the
    configuration from `init_params` that defines the setup stages, i.e. in
    ``init_params['cfg'][stages_cfg_sect]``:  The definition for setup stage
    named ``<stage>`` is expected to be the value of parameter ``<stage>`` in
    this very section.  For details how a setup stage given by its definition
    is performed please consult the documentation of function
    :py:func:`perform_stage`.

    :raise: a :py:exc:`SystemExit` exception if setup stages should not be
            performed but only initial setup parameters should be shown.
    :raise: a :py:exc:`dusetup.error.SetupError` if performing of a setup
            stage has failed.
    :raise: a :py:exc:`duconfig.ConfigError` if something is wrong with the
            `init_params` configuration.
    '''
    # If the setup stages should not be performed, simply show initial setup
    # parameters.
    stages = init_params['stages']
    app_name = init_params['app_name']
    app_vers = init_params['app_vers']
    prefix_dpath = init_params['prefix_dpath'].resolve()
    emacs_cmd = init_params['emacs_cmd']
    print(f"===Run setup stages for application `{app_name}`===")
    print(f"Setup stages          : {stages}")
    print(f"Application version   : {app_vers}")
    print(f"Prefix directory path :\n  {prefix_dpath}")
    print(f"Emacs command         : {emacs_cmd}")
    print(f"Python import paths   :\n  {sys.path}")
    perform_stages = init_params.pop('perform_stages')
    if perform_stages:
        time.sleep(RUN_START_TIME)
        # Setup stages should be performed -> do them in given order.
        _perform_stages(init_params, pre_stage, post_stage, stages_cfg_sect)
    else:
        # Setup stages should not be performed -> we are done.
        raise SystemExit(0)


def _perform_stages(init_params, pre_stage, post_stage, stages_cfg_sect):
    '''
    Variant of function :py:func:`perform_stages`.

    This function performs the setup stages as defined by setup parameter
    dictionary `init_params` for real.  For details please consult the
    documentation of :py:func:`perform_stages`.

    :raise: a :py:exc:`dusetup.error.SetupError` if performing of a setup
            stage has failed.
    :raise: a :py:exc:`duconfig.ConfigError` if something is wrong with the
            `init_params` configuration.
    '''
    # Get stages definitions from the configuration.
    stages_cfg = init_params['cfg'].get(stages_cfg_sect)
    if stages_cfg is None:
        msg = f"Missing configuration section `{stages_cfg_sect}`\n"
        msg += ' to define setup stages.'
        raise error.SetupError(msg)
    stages = init_params.pop('stages')
    params = init_params.copy()

    # If there is a pre-stage, perform it.
    pre_stage_def = stages_cfg.get(pre_stage)
    if pre_stage_def is not None:
        params = perform_stage(pre_stage, pre_stage_def, params)

    # Perform real stages in given order.
    for s in stages:
        stage_s_def = stages_cfg.get(s)
        if stage_s_def is None:
            msg = f"No setup steps are defined for setup stage `{s}`\n"
            msg += f" in configuration section `{stages_cfg_sect}`."
            raise error.SetupError(msg)
        params = perform_stage(s, stage_s_def, params)

    # If there is a post-stage, perform it.
    post_stage_def = stages_cfg.get(post_stage)
    if post_stage_def is not None:
        perform_stage(post_stage, post_stage_def, params)


def perform_stage(stage, stage_def, stage_params):
    '''
    Perform a setup stage.

    The setup stage with name `stage` and definition `stage_def` is performed
    using dictionary `stage_params` as setup parameters to start with.

    A stage definition is expected to be a list of definitions for the setup
    steps the respective setup stage consists of, and these setup steps are
    performed in their given order.

    For this every setup step ``<step>`` defined for the `stage` setup stage
    is performed with the dictionary of its setup parameters.  Here the setup
    parameter dictionary for ``<step>`` is either `stage_params` if ``<step>``
    is the first setup step defined in `stage_def`, or the setup parameter
    dictionary returned by performing the setup step defined in `stage_def`
    before ``<step>``.  For details of performing such a setup step, please
    consult the documentation of :py:func:`perform_step`.

    :return: dictionary with the setup parameters after performing the
             `stage` setup stage, i.e. returned from performing the last setup
             step defined in `stage_def`.

    :raise: a :py:exc:`dusetup.error.SetupError` if performing of a setup
            stage has failed.
    :raise: a :py:exc:`duconfig.ConfigError` if something is wrong with the
            `stage_params` configuration.
    '''
    nr_steps = len(stage_def)
    print(f"->Perform setup stage `{stage}` with {nr_steps} step(s).")
    step_params = stage_params.copy()
    for step_def in stage_def:
        step_params = perform_step(step_def, step_params)

    return step_params


def perform_step(step_def, step_params):
    '''
    Perform a setup step.

    This setup step is given by definition `step_def` using dictionary
    `step_params` as setup parameters.

    The setup step definition `step_def` is expected to define the call of the
    setup step function used to perform this setup step.  It has to be a
    string consisting of the following parts that are separated by
    :py:data:`dusetup.util.misc.PART_SEP`:

    + The first part is the name of the setup step function.

    + The next parts not representing key value pairs are taken as additional
      positional arguments for the ``<stage>`` setup step function call.

    + The remaining parts are expected to represent key value pairs where key
      and value are separated by :py:data:`dusetup.util.misc.NAME_VAL_SEP`.
      They are taken as additional keyword arguments for the setup step
      function call.

    This function call is  completed with the setup parameter dictionary
    `step_params` as first positional argument.  It is expected to return
    again a the setup parameter dictionary.  For every of these setup
    parameter dictionaries, the following parameters are at least expected:

    + key ``'cfg'`` with the setup configuration,

    + key ``'app_name'`` with the name of the application to be set up,

    + key ``'app_vers'`` with the version of the application to be set up.

    + key ``'app_name_vers'`` with the combined name and version of the
      application to be set up,

    + key ``'prefix_dpath'`` with the prefix directory path.

    :return: dictionary with setup parameters after performing the setup step.

    :raise: a :py:exc:`dusetup.error.SetupError` if performing of the setup
            step has failed.
    :raise: a :py:exc:`duconfig.ConfigError` if something is wrong with the
            `step_params` configuration.
    '''
    print(f"-->Perform step defined by\n     {step_def}")

    # Tokenize the setup step definition.
    try:
        (fct_add_posargs, add_kwargs) = util_misc.tokenize_val(step_def)
    except error.SetupError as e:
        msg = f"Tokenization of setup step definition\n  {step_def}\n"
        msg += f" has failed with error\n  {e}"
        raise error.SetupError(msg)
    if fct_add_posargs == []:
        raise error.SetupError(
            f"Missing step function in setup step definition\n  {step_def}")
    (fct_name, *add_pos_args) = fct_add_posargs

    # Get the setup step function.
    try:
        fct = util_misc.get_qualified_attr(fct_name, auto_import=True)
    except error.SetupError as e:
        msg = f"Getting the setup step function `{fct_name}`\n"
        msg += f" has failed with error\n  {e}"
        raise error.SetupError(msg)

    # Issue the setup step function call.
    msg = f"   Run function `{fct_name}`\n"
    msg += f"    with additional positional arguments\n     {add_pos_args}\n"
    msg += f"    and additional keyword arguments:\n     {add_kwargs}"
    print(msg)
    try:
        params = fct(step_params, *add_pos_args, **add_kwargs)
    except TypeError as ex:
        msg = f"Executing setup step function `{fct_name}`\n"
        msg += f"  has caused the following error:\n    {ex}"
        raise error.SetupError(msg)

    return params


# ----------------------------------------------------------------------------
# Local Variables:
# ispell-local-dictionary: "en"
# End:
