# ============================================================================
# Title          : Setup utilities
#
# Classification : Python module
#
# Author         : Dirk Ullrich
#
# Date           : 2024-02-27
#
# Description    : See documentation string below.
# ============================================================================

'''
Commandline utilities.

In particular, a modular :py:func:`main` function is provided for this package
that can also reused for other setup commandline tools.
'''


import pathlib
import sys

try:
    from duargparse import DUArgumentParser as ArgumentParser  # type: ignore
except ModuleNotFoundError:
    from argparse import ArgumentParser
import duconfig  # type: ignore

from . import version
from .conf import interpol as conf_interpol
from .conf import config as conf_config
from .util import misc as util_misc
from .util import install as util_install
from . import stage
from . import error


# Parameters
# ==========

#: Template for a standard description of a commandline setup tool with
#: placeholder ``app_name`` for the application to setup.
STD_TOOL_DESC_TPL = "Set up application `{app_name}`"
STD_TOOL_DESC_TPL += ' as defined by a setup configuration file.'

#: Template for the name of a setup configuration file.
SETUP_CONF_FNAME_TPL = '{app_name_vers}-setup.conf'

#: List of paths for base directories to be searched for setup configuration
#: files by default in addition to the current working directory.
SETUP_CONF_BASE_DPATHS = [
    duconfig.user_config_dir(),
    pathlib.Path('/etc')
]

#: Default name for the section in a setup configuration section with the
#: default values of some setup parameters.
CFG_DEFAULT_SECT = 'default'


# Functions
# =========


def calc_init_params(tool_fpath, tool_vers=version,
                     tool_desc_tpl=STD_TOOL_DESC_TPL,
                     cfg_fname_tpl=SETUP_CONF_FNAME_TPL,
                     cfg_base_dpaths=SETUP_CONF_BASE_DPATHS):
    '''
    Calculate the parameters to initialize a commandline setup tool.

    The parameters used to initialize a commandline setup tool whose command
    has full `tool_fpath`, version `tool_vers` and description according to
    template `tool_desc_tpl`.

    String `tool_desc_tpl` has a placeholder ``app_name`` for the application
    to setup.

    String `cfg_fname_tpl` is a template for the name of the setup
    configuration file, with a placeholder for combined application name and
    (possible) version.

    List `cfg_base_dpaths` is the list of paths for the base directories to be
    searched for setup configuration files by default in addition to the
    current working directory.  The real additional directories result from
    adding a subdirectory with the commandline setup tool's name.

    These parameters are collected in a dictionary that contains the following
    mandatory items:

    + key ``'app_name'`` with the name of the application to setup as value;

    + key ``'app_vers'`` with the default version of the application to setup
      as value;

    + key ``'tool_name'`` with the name of the setup tool as value;

    + key ``'tool_vers'`` with the version of the setup tool as value;

    + key ``'tool_desc'`` with a (short) description of the setup tool as
      value.

    + key ``'cfg_dpaths'`` with the paths of the directories to be searched
      for setup configuration files by default.

    + key ``'cfg_fnames'`` with the names of the setup configuration files
      searched for by default.

    + key ``'cfg_fpath'`` with the path of the default setup configuration
      file found (if such a file is available) as value.

    + key ``'out_cfg_fpath'`` with the default path of an exported setup
      configuration file,

    + key ``'emacs_cmd'`` with the default Emacs command used to byte-compile
      ELisp files.

    :return: dictionary with calculated parameters.
    '''
    init_params = {}

    # The application's name and default version is calculated from the path
    # of the current working directory.
    app_name = conf_interpol.calc_app_name()
    app_vers = conf_interpol.calc_app_version()
    init_params['app_name'] = app_name
    init_params['app_vers'] = app_vers

    # The setup tool's name we get from its command path.
    tool_fpath = pathlib.Path(tool_fpath)
    if tool_fpath.stem == '__main__':
        # Setup tool is called via ``python3 -m ...`` -> take the package
        # name.
        tool_name = tool_fpath.parent.name
    else:
        # Setup tool is called via its script -> take the filename.
        tool_name = tool_fpath.name
    init_params['tool_name'] = tool_name

    # The setup tool's version is simply stored.
    init_params['tool_vers'] = tool_vers

    # The setup tool's concrete description.
    tool_desc = tool_desc_tpl.format(app_name=app_name)
    init_params['tool_desc'] = tool_desc

    # List of directory paths to be searched for setup configuration files by
    # default.
    cfg_dpaths = [pathlib.Path.cwd()]
    add_cfg_dpaths = [d / tool_name for d in cfg_base_dpaths]
    cfg_dpaths.extend(add_cfg_dpaths)
    init_params['cfg_dpaths'] = cfg_dpaths

    # Search for a default setup configuration file.
    cfg_fpath_info = conf_config.search_config_file(
        app_name, app_vers, cfg_fname_tpl, cfg_dpaths)
    init_params['cfg_fpath'] = cfg_fpath_info['found_fpath']
    init_params['cfg_fnames'] = cfg_fpath_info['searched_fnames']

    # Default path to export a setup configuration file.
    cfg_fname = conf_config.config_file_name(
        app_name, None, cfg_fname_tpl)
    init_params['out_cfg_fpath'] = cfg_dpaths[1] / cfg_fname

    # Default Emacs command to byte-compile ELisp files.
    init_params['emacs_cmd'] = 'emacs'

    return init_params


def mk_cmdline_parser(init_params):
    '''
    Create a commandline parser for a setup commandline tool.

    For the setup commandline tool initialized by the parameters from
    dictionary `init_params` a commandline parser is created.  The expected
    content of dictionary `init_params` is described in the documentation of
    function :py:func:`calc_init_params`.

    :return: the created commandline parser.
    '''
    # Create commandline parser.
    cmdl_parser = ArgumentParser(
        prog=init_params['tool_name'],
        description=init_params['tool_desc'])

    # Add option for the tool's version.
    cmdl_parser.add_argument(
        '-V', '--version',
        action='version', version=f"%(prog)s {init_params['tool_vers']}"
    )

    # Add option for debugging.
    cmdl_parser.add_argument(
        '-d', '--debug', help='''
        Enable debugging.
        ''',
        action='store_true', default=False
    )

    # Add option for a setup configuration file path.
    default_cfg_fpath = init_params.get('cfg_fpath')
    if default_cfg_fpath is None:
        default_cfg_fpath_msg = '- must be given by this very option'
    else:
        default_cfg_fpath_msg = f"instead of default `{default_cfg_fpath}`"
    cmdl_parser.add_argument(
        '-c', '--use-cfg-file', metavar='<cfg_fpath>', help=f"""
        Use setup configuration file with path <cfg_fpath>
        {default_cfg_fpath_msg}.""",
        action='store', dest='use_cfg_fpath', default=default_cfg_fpath
    )

    # Add option for additional import paths.
    cmdl_parser.add_argument(
        '-a', '--add-import-path', metavar='<import_dpath>', help='''
        Add directory path <import_dpath> as additional path to import Python
        modules or packages with highest precedence (before those paths
        configured in the used setup configuration file and paths of the
        directories that are searched for setup configuration files by
        default).  This option can be used more than once.
        ''',
        action='append', dest='import_dpaths', default=[]
    )

    # Add option for setup steps to perform.
    read_cfg_msg = 'read from the setup configuration file'
    cmdl_parser.add_argument(
        '-s', '--stages', metavar='<stages>', help=f"""
        Use stages in comma-separated list <steps> to set up the application
        (default: {read_cfg_msg}).
        """,
        action='store', dest='stages_str', default=None
    )

    # Add option to not perform setup stages.
    cmdl_parser.add_argument(
        '-n', '--no-perform', help='''
        Do not perform setup stages but only show setup parameters to be used.
        ''',
        action='store_true', default=False
    )

    # Add option for the application's version.
    default_app_vers = init_params.get('app_vers')
    if default_app_vers is None or default_app_vers == '':
        default_app_vers_str = read_cfg_msg
    else:
        default_app_vers_str = f"`{default_app_vers}` or " + read_cfg_msg
    cmdl_parser.add_argument(
        '-v', '--appl-version', metavar='<app_vers>', help=f"""
        Use version <app_vers> for the application
        (default: {default_app_vers_str}).  An empty value is treated as no
        version.""",
        action='store', dest='app_vers', default=None
    )

    # Add option for the prefix directory path (template).
    cmdl_parser.add_argument(
        '-p', '--prefix', metavar='<prefix_dpath>', help=f"""
        Use prefix with directory path <prefix_dpath> (default
        {read_cfg_msg}).  A `{{app_name_vers}}` part (including the curly
        braces) is handled as placeholder for combined application name and
        version.""",
        action='store', dest='prefix_dpath_tpl', default=None
    )

    # Add option to export the setup configuration file.
    default_out_cfg_fpath = init_params['out_cfg_fpath']
    cmdl_parser.add_argument(
        '-C', '--export-cfg-file', metavar='<cfg_fpath>', help=f"""
        Export active setup configuration file as file with path <cfg_fpath>,
        and exit.  If <cfg_fpath> is not given, the user setup configuration
        file at `{default_out_cfg_fpath}` is used.
        """,
        action='store', nargs='?', dest='out_cfg_fpath',
        const=default_out_cfg_fpath, default=None
    )

    # Add option for the Emacs command used to byte-compile ELisp files.
    emacs_cmd = init_params['emacs_cmd']
    cmdl_parser.add_argument(
        '-e', '--emacs-command', metavar='<emacs_cmd>', help=f"""
        Use Emacs command <emacs_cmd> to byte-compile ELisp files (default
        `{emacs_cmd}`, or set in the setup configuration file).""",
        action='store', dest='emacs_cmd', default=None
    )

    return cmdl_parser


def read_config_file(init_params, args):
    '''
    Read the active setup configuration file.

    This active setup configuration file is either a default one or as given
    on the commandline via `args`, for a setup commandline tool initialized by
    the parameters from dictionary `init_params`.  The expected content of
    dictionary `init_params` is described in the documentation of function
    :py:func:`calc_init_params`.

    If wanted by `args`, the active setup configuration file is exported and
    the setup tool exits here.

    Otherwise the active setup configuration file is read.

    Furthermore the following directory paths are added as Python import paths
    in front of :py:data:`sys.path` if the respective directory
    exits (with increasing priority):

    + directory paths searched for setup configuration files by default,

    + directory path of an active setup configuration file given by the
      respective option ad commandline.

    :return: the active setup configuration.

    :raise: a :py:exc:`SystemExit` if the active setup configuration file
            should be exported.
    :raise: a :py:exc:`dusetup.error.SetupError` if an active setup
            configuration file is not given or cannot be read.
    :raise: a :py:exc:`duconfig.ConfigError` if something is wrong with the
            active setup configuration.
    '''
    # Check the active setup configuration file.
    cfg_fpath = args.use_cfg_fpath
    cfg_dpaths = init_params['cfg_dpaths']
    cfg_fnames = init_params['cfg_fnames']
    if cfg_fpath is None:
        msg = 'No active setup configuration file can be found.\n'
        msg += 'Its path must either be given via commandline option,\n'
        msg += 'or as file with one of the following names:\n'
        for n in cfg_fnames:
            msg += f"  {n}\n"
        msg += 'in one of the following directories:\n'
        for d in cfg_dpaths:
            msg += f"  {d}\n"
        raise error.SetupError(msg)
    else:
        print(f"Use setup configuration file at\n  {cfg_fpath}")

    # Export the active setup configuration file, if wanted, and exit.
    # Otherwise the active setup configuration is read from that file.
    out_cfg_fpath = args.out_cfg_fpath
    if out_cfg_fpath is not None:
        conf_config.export_config_file(cfg_fpath, out_cfg_fpath)
        raise SystemExit(0)
    else:
        cfg = conf_config.read_config_file(cfg_fpath)

    # Enhance :py:data:`sys.path`.
    # - By directory paths searched for setup configuration files by default.
    util_misc.add_import_dpaths(cfg_dpaths)
    # - By the directory path of a setup configuration file given at
    #   commandline, if the respective option is used.
    cfg_dpath = pathlib.Path(cfg_fpath).parent
    util_misc.add_import_dpaths([cfg_dpath])

    return cfg


def calc_setup_params(init_params, args, cfg,
                      cfg_default_sect=CFG_DEFAULT_SECT):
    '''
    Calculate the initial setup parameters for a setup commandline tool.

    This setup commandline tool is initialized by the parameters from
    dictionary `init_params`, together with the result `args` of parsing the
    tool's commandline and the active setup configuration `cfg`.  The expected
    content of dictionary `init_params` is described in the documentation of
    function :py:func:`calc_init_params`.

    String `default_cfg_sect` is the name of the section in the setup
    configuration with the default values of some setup parameters.

    The calculated setup parameters form a dictionary with the following
    mandatory items:

    + key ``'stages'`` with the list of the names of setup stages as value,

    + key ``'cfg'`` with the active setup configuration as value,

    + key ``'app_name'`` with the application name as value,

    + key ``'app_vers'`` with the application version as value, where an empty
      value or ``None`` represents a missing version,

    + key ``'app_name_vers'`` with the combined application name and version
      as value,

    + key ``'prefix_dpath'`` with prefix directory path as value,

    + key ``'emacs_cmd'`` with the default Emacs command used to byte-compile
      ELisp files,

    + key ``'perform_stages'`` with a switch as value to determine whether the
      setup stages will be preformed.

    Furthermore the following directory paths are added as Python import paths
    in front of :py:data:`sys.path` if the respective directory
    exits (with increasing priority):

    + directory paths configured in the default value section of `cfg` using
      parameter ``import_dpaths``,

    + directory paths given by the respective option at commandline.

    :return: dictionary with calculated initial setup parameters.

    :raise: a :py:exc:`dusetup.error.SetupError` if an active setup
            configuration file is not given or cannot be read.
    :raise: a :py:exc:`duconfig.ConfigError` if something is wrong with the
            active setup configuration.
    '''
    # Parameters immediately given as initial or commandline parameters.
    defaults = cfg[cfg_default_sect]
    app_name = init_params['app_name']
    # - The automatically calculated application version can be overwritten in
    #   the active setup configuration file, or at the commandline.
    if args.app_vers is not None:
        app_vers = args.app_vers
    elif 'app_vers' in defaults.keys():
        app_vers = defaults['app_vers']
    else:
        app_vers = init_params['app_vers']
    params = {
        'cfg': cfg,
        'app_name': app_name,
        'app_vers': app_vers
    }

    # Combined application name and version.
    app_name_vers = conf_interpol.add_app_version(app_name, app_vers)
    params['app_name_vers'] = app_name_vers

    # Prefix directory path.
    if args.prefix_dpath_tpl is not None:
        prefix_dpath_tpl = args.prefix_dpath_tpl
    else:
        prefix_dpath_tpl = defaults['prefix_dpath_tpl']
    prefix_dpath_str = prefix_dpath_tpl.format(app_name_vers=app_name_vers)
    prefix_dpath = pathlib.Path(prefix_dpath_str)
    params['prefix_dpath'] = prefix_dpath

    # The default Emacs command to byte-compile ELisp files.
    if args.emacs_cmd is not None:
        emacs_cmd = args.emacs_cmd
    elif 'emacs_cmd' in defaults.keys():
        emacs_cmd = defaults['emacs_cmd']
    else:
        emacs_cmd = init_params['emacs_cmd']
    params['emacs_cmd'] = emacs_cmd

    # Setup stages.
    all_stages = defaults['stages']
    if args.stages_str is not None:
        # Run the requested setup stages always in their configured order.
        wanted_stages = args.stages_str.split(',')
        stages = [s for s in all_stages if s in wanted_stages]
    else:
        stages = all_stages
    params['stages'] = stages

    # Perform stages switch.
    params['perform_stages'] = not args.no_perform

    # Enhance :py:data:`sys.path`.
    # - From setup configuration, if configured there.
    cfg_import_dpaths = defaults.get('import_dpaths', [])
    util_misc.add_import_dpaths(cfg_import_dpaths)
    # - Given at commandline, if the respective option is used.
    util_misc.add_import_dpaths(args.import_dpaths)

    return params


def add_std_install_dir_paths(params):
    '''
    Add standard installation directory paths to setup parameters.

    To the dictionary `params` of setup parameters the standard installation
    directory paths are added.

    Concretely, for every standard installation directory path for the
    application relative to the prefix directory path, the name of the
    respective directory path as key, and the directory path as value.  For
    details please consult the documentation of function
    :py:func:`dusetup.util.install.calc_install_dir_paths`.

    :return: dictionary with enhanced setup parameters.
    '''
    prefix_dpath = params['prefix_dpath']
    app_name_vers = params['app_name_vers']
    dst_dpaths_params = util_install.calc_std_install_dir_paths(
        prefix_dpath, app_name_vers)
    params.update(dst_dpaths_params)

    return params


def main(argv=sys.argv,
         tool_vers=version, tool_desc_tpl=STD_TOOL_DESC_TPL,
         calc_init_params_fct=calc_init_params,
         mk_cmdline_parser_fct=mk_cmdline_parser,
         read_config_file_fct=read_config_file,
         calc_setup_params_fct=calc_setup_params,
         add_std_install_dir_paths_fct=add_std_install_dir_paths):
    '''
    Main function for a commandline setup tool.

    This commandline setup tool is called with list `argv` of commandline
    arguments, that has version `tool_vers` and description according to
    template `tool_desc_tpl`.

    The functionality is provided by the following functions:

    + :py:func:`calc_init_params_fct` to calculate the parameters used to
      initialize the setup tool.

    + :py:func:`mk_cmdline_parser_fct` to create a commandline parser for the
      setup tool.

    + :py:func:`read_config_file_fct` to read the active setup configuration
      file.

    + :py:func:`calc_setup_params_fct` to calculate the initial setup
      parameters for the setup tool.

    + :py:func:`add_std_install_dir_paths_fct` to add the standard
      installation directory paths to the setup parameters.

    For a description of the arguments of these functions please see the
    documentation of the respective default functions (without their keyword
    arguments).

    :return: 0 if no error has occurred.
    '''
    with error.handle_errors(catch_errors=False) as err_hdl:

        # Initialize the setup tool.
        init_params = calc_init_params_fct(argv[0], tool_vers, tool_desc_tpl)

        # Create a commandline parser for the setup tool.
        cmdl_parser = mk_cmdline_parser(init_params)

        # Parse the commandline.
        args = cmdl_parser.parse_args(argv[1:])

        # If debugging is enabled, error exceptions are re-raised and their
        # full stack trace is printed.
        if args.debug:
            err_hdl.enable_debug()
        else:
            err_hdl.disable_debug()

        # Read the active setup configuration file - either export it or read
        # the active setup configuration from it to run the setup.
        cfg = read_config_file_fct(init_params, args)

        # Calculate the initial setup parameters for the setup tool, if
        # wanted.
        setup_params = calc_setup_params_fct(init_params, args, cfg)

        # Add the standard installation directory paths to the setup
        # parameters, if wanted.
        setup_params = add_std_install_dir_paths_fct(setup_params)

        # Perform setup stages, if wanted.
        stage.perform_stages(setup_params)

    return err_hdl.rc


# ----------------------------------------------------------------------------
# Local Variables:
# ispell-local-dictionary: "en"
# End:
