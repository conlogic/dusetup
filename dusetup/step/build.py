# ============================================================================
# Title          : Setup utilities
#
# Classification : Python module
#
# Author         : Dirk Ullrich
#
# Date           : 2023-05-04
#
# Description    : See documentation string below.
# ============================================================================

'''
Setup step functions to build various kinds of files or directories.
'''


from ..util import build as util_build
from ..util import elisp as util_elisp
from ..util import doc as util_doc
from ..util import misc as util_misc


# Parameters
# ==========


#: Associate every type of build item kinds either with ``None`` for skipping
#: build, or with dictionary that associates every item variant of a build
#: item kind with it build information dictionary that contains:
#:
#: + mandatory key ``'mk_fct'`` with the function that makes the items to be
#:   built.
#:
#: + optional key ``'mk_kwargs'`` with a dictionary of additional keyword
#:   arguments to be passed to the function that makes the items to be built.
#:   String values can contain placeholders for setup parameters.
BUILD_KIND_INFO = {
    'elisp': {
        'file': {
            'mk_fct': util_elisp.mk_elc_file,
            'mk_kwargs': {'emacs_cmd': "{emacs_cmd}"}
        },
        'files_of_dir': {
            'mk_fct': util_elisp.mk_elc_files_of_dir,
            'mk_kwargs': {'emacs_cmd': "{emacs_cmd}"}
        }
    },
    'texi': {
        'file': {'mk_fct': util_doc.texi2any_file},
        'files_of_dir': {'mk_fct': util_doc.texi2any_files_of_dir}
    },
    # The following item kind types are not built at all.
    'info': None,
    'misc': None,
    # The following item kind types are built in specific ways.
    'bin': None,
    'lib': None,
    'share': None,
    'doc': None
}


# Functions
# =========


def build_sect_items_from_cfg(params, item_sect, only_kind_types=None,
                              verbose=True, build_kind_info=BUILD_KIND_INFO):
    '''
    Build the item or items given in a configuration file section.

    This section `items_sect` is a section in the configuration from setup
    parameter dictionary `params`.

    The items in the `items_sect` section are grouped by their kind as key,
    and a single item element string or list of item elements strings of the
    respective kind.  Every such item element string defines an item to be
    built, together with optional parameters for its building.

    For a not-``None`` generalized tuple `only_kind_types` only those items
    from section `item_sect` are built whose kind types are in
    `only_kind_types`.

    If string `verbose` represents ``True``, messages for building are
    printed.

    Dictionary `build_kind_info` contains the build item kind information.

    For a description of the setup parameter dictionary please consult the
    documentation of :py:func:`dusetup.stage.perform_step`.

    A description of an item element can be found in the documentation of
    function :py:func:`dusetup.util.misc.tokenize_item_element`.

    :return: the dictionary `params` of setup parameters.

    :raise: a :py:exc:`duconfig.ConfigError` for a configuration error.
    :raise: a :py:exc:`dusetup.error.SetupError` if building of an item has
            failed.
    '''
    cfg = params['cfg']
    cfg_items_sect = cfg[item_sect]
    for k in cfg_items_sect.keys():
        _build_key_items_from_cfg(
            params, item_sect, k, verbose, build_kind_info, only_kind_types)

    return params


def build_key_items_from_cfg(params, item_sect, kind_key, verbose=True,
                             build_kind_info=BUILD_KIND_INFO):
    '''
    Build the item or items for a kind.

    This kind given by key `kind_key` from section `item_sect` in the
    configuration from setup parameter dictionary `params`.

    The value for `kind_key` is a single item element string or a list
    of item elements strings of one kind ``<kind>`` as value, where for the
    former ``<kind>`` is `kind_key` itself, and for the latter `kind_key`
    without the trailing ``'s'`` signaling a list of item element strings.
    Every such item element string of kind ``<kind>`` defines an item of
    ``<kind>``'s type to be built, together with optional parameters for
    its build function for ``<kind>``.

    If generalized switch `verbose` represents ``True``, messages for building
    are printed.

    Dictionary `build_kind_info` contains the build item kind information.

    For a description of the setup parameter dictionary please consult the
    documentation of :py:func:`dusetup.stage.perform_step`.

    A description of an item element can be found in the documentation of
    function :py:func:`dusetup.util.misc.tokenize_item_element`.

    :return: the dictionary `params` of setup parameters.

    :raise: a :py:exc:`duconfig.ConfigError` for a configuration error.
    :raise: a :py:exc:`dusetup.error.SetupError` if building of an item has
            failed.
    '''
    # Do real work.
    _build_key_items_from_cfg(
        params, item_sect, kind_key, verbose, build_kind_info)

    return params


def _build_key_items_from_cfg(params, item_sect, kind_key, verbose,
                              build_kind_info, only_kind_types=None):
    '''
    Variant of :py:func:`build_key_items_from_cfg`.

    This variant has an additional generalized tuple argument `only_types`.
    For a not-``None`` generalized tuple `only_kind_types` the item / items
    for `kind_key` in `item_sect` is / are only built if its kind type is in
    `only_kind_types`.
    '''
    # Calculate the kind and the associated item elements to be built.
    (kind, item_elems) = util_misc.kind_item_elements_from_cfg_key(
        params, item_sect, kind_key)

    # Calculate the full build kind info.
    kind_info = util_build.calc_kind_info(params, kind, build_kind_info)

    if kind_info is None:
        print(f"--->Skip build for kind `{kind}`.")
    else:
        print(f"--->Build items for kind `{kind}`.")
        util_build.build_kind_item_elements(
            params, kind_info, item_elems, verbose)


# ----------------------------------------------------------------------------
# Local Variables:
# ispell-local-dictionary: "en"
# End:
