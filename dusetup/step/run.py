# ============================================================================
# Title          : Setup utilities
#
# Classification : Python module
#
# Author         : Dirk Ullrich
#
# Date           : 2024-02-27
#
# Description    : See documentation string below.
# ============================================================================

'''
Setup step functions to run external commands.
'''


from ..util import run as util_run


def run_cmd_from_cfg(params, cmd_key, shell=True, verbose=False):
    '''
    Run a command defined by configuration value.

    Run the command whose template is given as value for key `cmd_key` in the
    configuration from setup parameter dictionary `params`.

    If generalized switch `shell` represents ``True``, the command runs in a
    shell.

    If generalized switch `verbose` represents ``True``, a message with the
    command to be run is printed.

    For a description of the setup parameter dictionary please consult the
    documentation of :py:func:`dusetup.stage.perform_step`.

    :return: the dictionary `params` of setup parameters,

    :raise: a :py:exc:`duconfig.ConfigError` for a configuration error.
    :raise: a :py:exc:`dusetup.error.SetupError` if a running of the command
            has failed.
    '''
    params = params.copy()
    cfg = params['cfg']

    # Replace in the command template setup parameter placeholders by their
    # respective values.
    cmd = cfg[cmd_key].format_map(params)

    # Run the real command.
    util_run.run_cmd(cmd, shell=shell, verbose=verbose)

    return params


def run_cmds_from_cfg(params, cmds_key, shell=True, verbose=False):
    '''
    Run a command defined by configuration list value.

    Run the commands whose templates are given as list value for key
    `cmds_key` in the configuration from setup parameter dictionary `params`.

    If generalized switch `shell` represents ``True``, the commands run in a
    shell.

    If generalized switch `verbose` represents ``True``, messages with the
    commands to be run are printed.

    For a description of the setup parameter dictionary please consult the
    documentation of :py:func:`dusetup.stage.perform_step`.

    :return: the dictionary `params` of setup parameters,

    :raise: a :py:exc:`duconfig.ConfigError` for a configuration error.
    :raise: a :py:exc:`dusetup.error.SetupError` if a running of a command has
            failed.
    '''
    params = params.copy()
    cfg = params['cfg']

    # Replace in the command templates setup parameter placeholders by their
    # respective values.
    cmds = [c.format_map(params) for c in cfg[cmds_key]]

    # Run the real commands.
    util_run.run_cmds(cmds, shell=shell, verbose=verbose)

    return params


# ----------------------------------------------------------------------------
# Local Variables:
# ispell-local-dictionary: "en"
# End:
