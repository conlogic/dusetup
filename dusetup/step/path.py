# ============================================================================
# Title          : Setup utilities
#
# Classification : Python module
#
# Author         : Dirk Ullrich
#
# Date           : 2024-04-21
#
# Description    : See documentation string below.
# ============================================================================

'''
Setup step functions related to paths of files or directories.
'''


import pathlib

from ..util import path as util_path
from ..conf import interpol


# Functions
# =========


def add_path_envvar(params, var_name, add_path_tpl, place='end',
                    keep_empty=True, verbose=True):
    '''
    Extend search path value of environment variable.

    The search path value of environment variable with name `var_name` is
    extended by additional path given by template string `add_path_tpl` using
    setup parameter dictionary `params`.`add_path`.

    String `place` controls where the additional path is added:

    + at the begin, if `place` has value ``'beg'``,

    + at the end, if `place` has value ``'end'``.

    If generalized switch switch `keep_empty` represents ``True`` even an
    empty original value of the `var_name` variable is preserved.  Please note
    that a path separator at the `place`'s end of the old value of the
    `var_name` variable is always preserved.

    If generalized switch `verbose` represents ``True``, a message with the
    new value of the environment variable is printed.

    :return: the dictionary `params` of setup parameters.
    '''
    add_path = add_path_tpl.format_map(params)

    util_path.add_path_envvar(var_name, add_path, place, keep_empty, verbose)

    return params


def rm_item(params, path_tpl, verbose=True):
    '''
    Remove an existing file or directory.

    Existing file or directory with path given by template string `path_tpl`
    using setup parameter dictionary `params` is removed.  For a directory
    its content is removed, too.

    If generalized switch `verbose` represents ``True``, a message for
    directory removal is printed.

    :return: the dictionary `params` of setup parameters.
    '''
    path = path_tpl.format_map(params)

    util_path.rm_item(path, verbose)

    return params


def ensure_clean_dir(params, dpath_tpl, verbose=True):
    '''
    Ensure a clean directory.

    For the directory path given by template string `dpath_tpl` using setup
    parameter dictionary `params` a clean directory is ensured.

    If generalized switch `verbose` represents ``True``, messages for the
    performed actions are printed.

    For a description of the setup parameter dictionary please consult the
    documentation of :py:func:`dusetup.stage.perform_step`.

    :return: the dictionary `params` of setup parameters.
    '''
    dpath = dpath_tpl.format_map(params)

    util_path.ensure_clean_dir(dpath, verbose)

    return params


def cpmv_item(params, src_path_tpl, dst_path_tpl,
              rm_src=False, fmode=None, dmode=None, verbose=True):
    '''
    Copy or move an existing item.

    Copy (recursively) or move existing file or directory with path given by
    template `src_path_tpl` to path given by template `dst_path_tpl` using
    setup parameter dictionary `params`.  The destination directory (if the
    source item is a file) or the parent directories for the destination
    directory (if the source item is a directory) will be created if they do
    not already exist.

    If generalized switch `rm_src` represents ``True`` the source file is
    removed, too.

    A not-``None`` permission specification `fmode` is used to set the
    permissions of all destination files.

    A not-``None`` permission specification `dmode` is used to set the
    permissions of all destination directories.

    If generalized switch `verbose` represents ``True`` a message about
    copying / moving is printed.

    :return: the dictionary `params` of setup parameters.
    '''
    src_path = src_path_tpl.format_map(params)
    dst_path = dst_path_tpl.format_map(params)

    util_path.cpmv_item(
        src_path, dst_path, rm_src, fmode, dmode, verbose=verbose)

    return params


def symlink_item(params, src_path_tpl, dst_path_tpl,
                 dst_as_dir=False, mk_relative=True, verbose=True):
    '''
    Symlink an item.

    Symlink file or directory with path given by template `src_path_tpl` to
    path given by template `dst_path_tpl` using setup parameter dictionary
    `params`.  The link directory will be created if they do not already
    exist.

    If generalized switch `dst_as_dir` represents ``True`` the path for
    `dst_path:tpl` is supposed to be the path of a directory, and it is used
    as path for the directory of the link instead as path of the link itself.

    If generalized switch `mk_relative` represents ``True``, the created link
    is a relative one instead of an absolute one.

    If generalized switch `verbose` represents ``True`` a message about
    symlinking is printed.

    :return: the dictionary `params` of setup parameters.
    '''
    src_path = src_path_tpl.format_map(params)
    dst_path = dst_path_tpl.format_map(params)

    util_path.symlink_item(
        src_path, dst_path, dst_as_dir, mk_relative, verbose)

    return params


def symlink_items_of_dir(params, src_dpath_tpl, dst_dpath_tpl,
                         mk_relative=True, verbose=True):
    '''
    Symlink the items in a directory.

    Symlink the files or directories in directory with path given by template
    `src_dpath_tpl` to links with same name in directory with path given by
    template `dst_dpath_tpl` using setup parameter dictionary `params`.  This
    directory will be created if they do not already exist.

    If generalized switch `mk_relative` represents ``True``, the created link
    is a relative one instead of an absolute one.

    If generalized switch `verbose` represents ``True`` a message about
    symlinking is printed.

    :return: the dictionary `params` of setup parameters.
    '''
    src_dpath = src_dpath_tpl.format_map(params)
    dst_dpath = dst_dpath_tpl.format_map(params)

    util_path.symlink_items_of_dir(
        src_dpath, dst_dpath, mk_relative=mk_relative, verbose=verbose)

    return params


def novers_symlink_items_of_dir(params, dpath_tpl, verbose=True):
    '''
    Symlink items of a directory to version-less variants.

    All files or directories with application version added in directory with
    path given by template `dpath_tpl` using setup parameter dictionary
    `params` are symlinked relatively to corresponding variants without
    version in the same directory.

    If generalized switch `verbose` represents ``True`` a message about
    symlinking is printed.

    :return: the dictionary `params` of setup parameters.
    '''
    dpath = dpath_tpl.format_map(params)
    app_vers = params['app_vers']

    dpath = pathlib.Path(dpath)
    inames = util_path.ls_dir(dpath, abspath=False)
    for iname in inames:
        iname_novers = interpol.del_app_version(iname, app_vers)
        ipath_novers = dpath / iname_novers
        ipath = dpath / iname
        if iname != iname_novers and not ipath_novers.exists():
            util_path.symlink_item(
                ipath, ipath_novers, mk_relative=True, verbose=verbose)

    return params


# ----------------------------------------------------------------------------
# Local Variables:
# ispell-local-dictionary: "en"
# End:
