# ============================================================================
# Title          : Setup utilities
#
# Classification : Python module
#
# Author         : Dirk Ullrich
#
# Date           : 2024-02-27
#
# Description    : See documentation string below.
# ============================================================================

'''
Setup step functions to install various kinds of files or directories.
'''


from ..util import path as util_path
from ..util import install as util_install
from ..util import elisp as util_elisp
from ..util import python as util_python
from ..util import doc as util_doc
from ..util import misc as util_misc


# Parameters
# ==========

#: Associate every type of install item kinds with its install information
#: dictionary that contains:
#:
#: + mandatory key ``'dst_dpath_tpl'`` with the template of the destination
#:   directory as value;
#:
#: + mandatory key ``'add_vers'`` with a switch as value whether the
#:   application version should be added to the installed item;
#:
#: + optional key ``'cp_kwargs'`` with a dictionary of additional keyword
#:   arguments to be passed to the function that copies the items to be
#:   installed.  String values can contain placeholders for setup parameters.
#:
#: + optional key ``'post_fct'`` with a function to be called after the
#:   installation of an item / a list of items of the respective kind as
#:   value.
INSTALL_KIND_TYPE_INFO = {
    'bin': {
        'dst_dpath_tpl': "{bin_dpath}",
        'add_vers': True,
        'cp_kwargs': {'fmode': 0o755}
    },
    'lib': {
        'dst_dpath_tpl': "{lib_dpath}",
        'add_vers': False
    },
    'share': {
        'dst_dpath_tpl': "{share_dpath}",
        'add_vers': False
    },
    'doc': {
        'dst_dpath_tpl': "{doc_dpath}",
        'add_vers': False
    },
    'elisp': {
        'dst_dpath_tpl': "{elisp_dpath}",
        'add_vers': True,
        'cp_kwargs': {'src_crit': util_elisp.is_elisp_file}
    },
    'python': {
        'dst_dpath_tpl': "{python_dpath}",
        'add_vers': True,
        'post_fct': util_python.compile_python_item
    },
    'info': {
        'dst_dpath_tpl': "{info_dpath}",
        'add_vers': True,
        'cp_kwargs': {'src_crit': util_doc.is_info_file},
        'post_fct': util_doc.install_info_files_of_dir
    },
    'vim': {
        'dst_dpath_tpl': "{vim_dpath}",
        'add_vers': True,
    },
    'bash': {
        'dst_dpath_tpl': "{bash_dpath}",
        'add_vers': True,
    },
    'zsh': {
        'dst_dpath_tpl': "{zsh_dpath}",
        'add_vers': True,
    }
}

#: Associate every item variant of install item kinds with its install
#: information dictionary that contains:
#:
#: + mandatory key ``'cp_fct'`` with the function that copies the items to be
#:   installed.
#:
#: + mandatory key ``'multi'`` with a switch that is ``True`` if multiple
#:   items can be installed by the install function.
INSTALL_KIND_ITEM_INFO = {
    'file': {
        'cp_fct': util_path.cpmv_file,
        'multi': False
    },
    'dir': {
        'cp_fct': util_path.cpmv_dir,
        'multi': False
    },
    'item': {
        'cp_fct': util_path.cpmv_item,
        'multi': False
    },
    'items_of_dir': {
        'cp_fct': util_path.cpmv_items_of_dir,
        'multi': True
    },
    'files_of_dir': {
        'cp_fct': util_path.cpmv_files_of_dir,
        'multi': True,
    }
}


# Functions
# =========


def install_sect_items_from_cfg(params, item_sect, only_kind_types=None,
                                verbose=True,
                                inst_kind_type_info=INSTALL_KIND_TYPE_INFO,
                                inst_kind_item_info=INSTALL_KIND_ITEM_INFO):
    '''
    Install the item(s) given in a configuration section.

    Install the item or items given in section `items_sect` in the
    configuration from setup parameter dictionary `params`.

    The items in the `items_sect` section are grouped by their kind as key,
    and a single item element string or list of item elements strings of the
    respective kind.  Every such item element string defines an item to be
    installed, together with optional parameters for its installation.

    For a not-``None`` generalized tuple `only_kind_types` only those items
    from section `item_sect` are installed whose kind types are in
    `only_kind_types`.

    If generalized switch `verbose` represents ``True``, messages for
    installations are printed.

    Dictionary `inst_kind_type_info` / `inst_kind_item_info` contains the
    install item kind information for the respective file type / item variant.

    For a description of the setup parameter dictionary please consult the
    documentation of :py:func:`dusetup.stage.perform_step`.

    A description of an item element can be found in the documentation of
    function :py:func:`dusetup.util.misc.tokenize_item_element`.

    :return: the dictionary `params` of setup parameters.

    :raise: a :py:exc:`duconfig.ConfigError` for a configuration error.
    :raise: a :py:exc:`dusetup.error.SetupError` if installation of an item
            has failed.
    '''
    cfg = params['cfg']
    cfg_items_sect = cfg[item_sect]
    for k in cfg_items_sect.keys():
        _install_key_items_from_cfg(
            params, item_sect, k, verbose,
            inst_kind_type_info, inst_kind_item_info, only_kind_types)

    return params


def install_key_items_from_cfg(params, item_sect, kind_key, verbose=True,
                               inst_kind_type_info=INSTALL_KIND_TYPE_INFO,
                               inst_kind_item_info=INSTALL_KIND_ITEM_INFO):
    '''
    Install the item(s) given by a key in a configuration section.

    Install the item or items given by key `kind_key` from section `item_sect`
    in the configuration from setup parameter dictionary `params`.

    The value for `kind_key` is a single item element string or a list
    of item elements strings of one kind ``<kind>`` as value, where for the
    former ``<kind>`` is `kind_key` itself, and for the latter `kind_key`
    without the trailing ``'s'`` signaling a list of item element strings.
    Every such item element string of kind ``<kind>`` defines an item of
    ``<kind>``'s type to be installed, together with optional parameters for
    its installation by a function determined by ``<kind>``'s item info.

    If generalized switch `verbose` represents ``True``, messages for
    installations are printed.

    Dictionary `inst_kind_type_info` / `inst_kind_item_info` contains the
    install item kind information for the respective file type / item variant.

    For a description of the setup parameter dictionary please consult the
    documentation of :py:func:`dusetup.stage.perform_step`.

    A description of an item element can be found in the documentation of
    function :py:func:`dusetup.util.misc.tokenize_item_element`.

    :return: the dictionary `params` of setup parameters.

    :raise: a :py:exc:`duconfig.ConfigError` for a configuration error.
    :raise: a :py:exc:`dusetup.error.SetupError` if installation of an item
            has failed.
    '''
    # Do real work.
    _install_key_items_from_cfg(
        params, item_sect, kind_key, verbose,
        inst_kind_type_info, inst_kind_item_info)

    return params


def _install_key_items_from_cfg(params, item_sect, kind_key, verbose,
                                inst_kind_type_info, inst_kind_item_info,
                                only_kind_types=None):
    '''
    Helper function for :py:func:`install_key_items_from_cfg`.

    Variant of :py:func:`install_key_items_from_cfg` with additional
    generalized tuple argument `only_types`.  For a not-``None`` generalized
    tuple `only_kind_types` the item / items for `kind_key` in `item_sect` is
    / are only installed if its kind type is in `only_kind_types`.
    '''
    # Calculate the kind and the associated item elements to be installed.
    (kind, item_elems) = util_misc.kind_item_elements_from_cfg_key(
        params, item_sect, kind_key)

    # Calculate the full install kind info.
    kind_info = util_install.calc_kind_info(
        params, kind,
        inst_kind_type_info, inst_kind_item_info, only_kind_types)

    # Do real installation of the list of item elements to be installed, if
    # wanted.
    if kind_info is None:
        print(f"--->Skip installation for kind `{kind}`.")
    else:
        print(f"--->Install items for kind `{kind}`.")
        util_install.install_kind_item_elements(
            params, kind_info, item_elems, verbose)


# ----------------------------------------------------------------------------
# Local Variables:
# ispell-local-dictionary: "en"
# End:
