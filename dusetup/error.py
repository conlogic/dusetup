# ============================================================================
# Title          : Setup utilities
#
# Classification : Python module
#
# Author         : Dirk Ullrich
#
# Date           : 2023-05-04
#
# Description    : See documentation string below.
# ============================================================================

'''
Error handling.
'''


import sys


# Classes
# =======


class SetupError(Exception):
    '''
    Specific exception for errors related to setup.

    Such an exception has a message `msg`, and optional location `loc` and an
    optional return code `rc`.
    '''
    def __init__(self, msg, loc=None, rc=None):
        self.msg = msg
        self.loc = loc
        self.rc = rc


    def __str__(self):
        # Since we want to have full control whether the printable string for
        # the exception ends with a ``'.'`` we ensure first that there is no
        # ``'.'`` at the end.
        if self.msg.endswith('.'):
            ex_str = self.msg[:-1]
        else:
            ex_str = self.msg

        # When a return code is given, it is added.
        if self.rc is not None:
            ex_str += f" (rc={self.rc})"

        # When a location is given, we start a new line for it, but do not
        # want a ``'.'`` at the end.  Otherwise we ensure one.
        if self.loc is not None:
            ex_str += f" at\n  {self.loc}"
        else:
            ex_str += '.'

        return ex_str


class handle_errors(object):
    '''
    Context manager to handle exceptions caused by errors.

    The errors are handled by printing an appropriate error message if such an
    exception has be raised.  It is distinguished between exceptions of type
    :py:exc:`SetupError`, and other error exceptions.  Non-error exceptions,
    i.e. exceptions not inheriting from :py:exc:`Exception`, are not handled
    at all.

    Beside the attribute :py:attr:`rc` is set to an return code <> 0 iff an
    error exception has been handled.

    If `catch_errors` has value ``True``, error exceptions are caught and
    handled as described above.
    '''
    def __init__(self, catch_errors=True):
        '''
        Initialize this context.
        '''
        self.catch_errors = catch_errors

        # Default return code is 0.
        self.rc = 0


    def __enter__(self):
        '''
        Enter this context.
        '''
        # Nothing to do when entering the context.

        return self


    def __exit__(self, exc_type, exc_obj, traceback):
        '''
        Exit this context.

        The method is called with not-``None`` type `exc_type` is the type
        of raised exception, a not-``None`` `exc_obj` is the exception raised,
        and a not-``None`` `traceback` is the associated trace back.

        Print an appropriate message to STDERR if an error exception is raised
        and will not been raised again.
        '''
        # Store whether current exception is handled by us.
        exc_handled = self.catch_errors
        if exc_type is None:
            # We cannot pass ``None`` to ``issubclass``.
            pass
        elif issubclass(exc_type, SetupError):
            # A setup error has occurred.
            msg = f"The following setup error has occurred:\n  {exc_obj}"
            self.rc = exc_obj.rc if exc_obj.rc is not None else 1
        elif issubclass(exc_type, Exception):
            # A non-setup error exception has been raised.
            exc_name = exc_type.__name__
            msg = f"An '{exc_name}' exception has raised:\n  {exc_obj}"
            self.rc = 2
        else:
            # A non-error exception should do its work.
            exc_handled = False

        if not self.rc == 0 and exc_handled:
            # Print an error message if an error exception has been raised and
            # should be handled by us.
            print(f"\n{msg}", file=sys.stderr)

        return exc_handled


    def enable_debug(self):
        '''
        Enable debugging.

        During debugging raised error exceptions will not be handled but
        raised again.
        '''
        self.catch_errors = False


    def disable_debug(self):
        '''
        Disable debugging.

        Without debugging raised error exceptions will be handled and not
        raised again.
        '''
        self.catch_errors = True


# ----------------------------------------------------------------------------
# Local Variables:
# ispell-local-dictionary: "en"
# End:
