# ============================================================================
# Title          : Setup utilities
#
# Classification : Python module
#
# Author         : Dirk Ullrich
#
# Date           : 2023-05-04
#
# Description    : See documentation string below.
# ============================================================================

'''
Helpers to build various kinds of files or directories.
'''


from .. import error
from . import misc as util_misc


# Functions
# =========


def calc_kind_info(params, kind, build_kind_info, only_kind_types=None):
    '''
    Calculate the concrete kind build information for kind `kind`.

    This calculation uses setup parameter dictionary `params` and dictionary
    `build_kind_info` with build item kind information.

    This concrete build information for `kind` consists of:

    + mandatory key ``'mk_fct'`` with the function that make the item / a
      list of items to be made as value.

    + mandatory key ``'mk_kwargs'`` with keyword arguments passed to the
      function that make the item / a list of items to be made as value.

    For a not-``None`` generalized tuple `only_kind_types` this concrete build
    information for `kind` is only calculated if the kind type of `kind` is
    `only_kind_types`.

    :return: full build information for `kind`, or ``None`` if it is not
             calculated due to `only_kind_types`.

    :raise: a :py:exc:`dusetup.error.SetupError` if calculation has failed.
    '''
    # Tokenize `kind`.
    (type_info, item_info_key) = util_misc.tokenize_item_kind(
        kind, build_kind_info, only_kind_types)

    # Calculate full kind install info, if wanted.
    if type_info is not None:
        kind_info = type_info.get(item_info_key)
        if kind_info is None:
            msg = f"Unknown build kind item type `{item_info_key}`"
            msg += f" for kind `{kind}`."
            raise error.SetupError(msg)
        kind_info['mk_kwargs'] = kind_info.get('mk_kwargs', {})
    else:
        kind_info = None

    return kind_info


def build_kind_item_elements(params, kind_info, item_elems, verbose=True):
    '''
    Build the list `item_elems` of item elements of the same kind.

    This build uses the setup parameter dictionary `params` and concrete build
    kind information `kind_info`.

    If generalized switch `verbose` represents ``True``, messages for
    installations are printed.

    For a description of the concrete kind build information please consult
    the documentation of :py:func:`calc_kind_info`.

    A description of an item element can be found in the documentation of
    function :py:func:`dusetup.util.misc.tokenize_item_element`.

    :raise: a :py:exc:`dusetup.error.SetupError` if installation of an item
            has failed.
    '''
    mk_fct = kind_info['mk_fct']
    print(f"---->Use make function `{mk_fct.__name__}`.")
    kind_mk_kwargs = kind_info['mk_kwargs']

    # Do real build of the list of item elements to be built.
    for (src_path, build_params) in item_elems:
        raw_mk_kwargs = kind_mk_kwargs.copy()
        raw_mk_kwargs.update(build_params)
        mk_kwargs = util_misc.format_dict_str_vals(raw_mk_kwargs, params)
        mk_fct(src_path, verbose=verbose, **mk_kwargs)


# ----------------------------------------------------------------------------
# Local Variables:
# ispell-local-dictionary: "en"
# End:
