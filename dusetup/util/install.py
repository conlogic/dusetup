# ============================================================================
# Title          : Setup utilities
#
# Classification : Python module
#
# Author         : Dirk Ullrich
#
# Date           : 2023-05-04
#
# Description    : See documentation string below.
# ============================================================================

'''
Helpers to install various kinds of files or directories.
'''


import pathlib

from ..conf import interpol
from .. import error
from . import misc
from . import python


# Functions
# =========


def calc_std_install_dir_paths(prefix_dpath, app_name_vers):
    '''
    Calculate the standard installation directory paths.

    For prefix directory path `prefix_dpath` the standard installation
    directory paths are calculated for application with combined name and
    version `app_name_vers`.

    The calculated paths are stored as values in a dictionary with their
    respective names as keys.  Concretely, this dictionary contains:

    + key ``'bin_dpath'`` with the binary directory path as value,

    + key ``'lib_dpath'`` with the library directory path as value,

    + key ``'share_dpath'`` with the shared directory path as value,

    + key ``'doc_dpath'`` with the documentation directory path as value,

    + key ``'man_dpath'`` with the manpage directory path as value,

    + key ``'info_dpath'`` with the info directory path as value,

    + key ``'elisp_dpath'`` with the Emacs Lisp directory path as value,

    + key ``'python_dpath'`` with the Python items directory path as value,

    + key ``'vim_dpath'`` with Vim files directory path,

    + key ``'bash_dpath'`` with Bash completion files directory path,

    + key ``'zsh_dpath'`` with Zsh completion files directory path,

    :return: the dictionary with the calculated directory paths.
    '''
    dpaths_params = {
        # Binary directory path.
        'bin_dpath': prefix_dpath / 'bin',
        # Library directory path.
        'lib_dpath': prefix_dpath / 'lib' / app_name_vers,
        # Shared directory path.
        'share_dpath': prefix_dpath / 'share' / app_name_vers,
        # Documentation directory path.
        'doc_dpath': prefix_dpath / 'share' / 'doc' / app_name_vers,
        # Info directory path.
        'info_dpath': prefix_dpath / 'share' / 'info',
        # Manpage directory path.
        'man_dpath': prefix_dpath / 'share' / 'man',
        # Emacs Lisp directory path.
        'elisp_dpath': prefix_dpath / 'share' / 'emacs' / 'site-lisp',
        # Python items directory path.
        'python_dpath': prefix_dpath / python.rel_python_site_dir(),
        # Vim file directory path.
        'vim_dpath': prefix_dpath / 'share' / 'vim' / 'vimfiles',
        # Bash completion files directory path.
        'bash_dpath': \
        prefix_dpath / 'share' / 'bash-completion' / 'completions',
        # Zsh completion files directory path.
        'zsh_dpath': prefix_dpath / 'share' / 'zsh' / 'site-functions'
    }

    return dpaths_params


def calc_kind_info(params, kind, inst_kind_type_info, inst_kind_item_info,
                   only_kind_types=None):
    '''
    Calculate the concrete kind install information.

    For kind `kind` using setup parameter dictionary `params` and dictionary
    `inst_kind_type_info` / `inst_kind_item_info` that contains the install
    item kind information for the respective file type / item variant the
    concrete kind install information is calculated.

    This concrete installation information for `kind` consists of:

    + mandatory key ``'dst_dpath'`` with the destination directory path as
      value;

    + mandatory key ``'add_vers'`` with a switch as value whether a version
      has to be added to the destination path;

    + mandatory key ``'cp_fct'`` with the function that copy the item / a
      list of items to be installed as value.

    + mandatory key ``'cp_kwargs'`` with keyword arguments passed to the
      function that copy the item / a list of items to be installed as value.

    + optional key ``'post_fct'`` with a function to be called after the
      installation of an item / a list of items of the respective kind as
      value.

    For a not-``None`` generalized tuple `only_kind_types` this concrete
    installation information for `kind` is only calculated if the kind type of
    `kind` is `only_kind_types`.

    :return: full install information for `kind`, or ``None`` if it is not
             calculated due to `only_kind_types`.

    :raise: a :py:exc:`dusetup.error.SetupError` if calculation has failed.
    '''
    # Tokenize `kind`, and info lookup.
    (type_info, item_info_key) = misc.tokenize_item_kind(
        kind, inst_kind_type_info, only_kind_types)
    item_info = inst_kind_item_info.get(item_info_key)
    if item_info is None:
        raise error.SetupError(
            f"Unknown installation kind item type `{item_info_key}`.")

    # Calculate full kind install info, if wanted.
    if type_info is not None:
        kind_info = {}
        # - Destination path.
        dst_dpath_tpl = type_info['dst_dpath_tpl']
        dst_dpath_str = dst_dpath_tpl.format_map(params)
        kind_info['dst_dpath'] = pathlib.Path(dst_dpath_str)
        # - Add application version switch.
        kind_info['add_vers'] = type_info['add_vers']
        # - Copy function.
        kind_info['cp_fct'] = item_info['cp_fct']
        # - Kind-specific keyword arguments for the copy function.
        kind_info['cp_kwargs'] = type_info.get('cp_kwargs', {})
        # - Post-install function.
        if 'post_fct' in type_info.keys():
            kind_info['post_fct'] = type_info['post_fct']
        # - Multiple items switch.
        kind_info['multi'] = item_info['multi']
    else:
        kind_info = None

    return kind_info


def install_kind_item_elements(params, kind_info, item_elems, verbose=True):
    '''
    Install a list of items of the same kind.

    The list `item_elems` of item elements of the same kind are installed
    using setup parameter dictionary `params` and with concrete install kind
    information `kind_info`.  This done by the following steps:

    1. Install every item element of `item_elem` using the install function
       from `kind_info`.

    2. If `kind_info` has a post-installation function, call it for the
       destination directory of the installed items.

    If generalized switch `verbose` represents ``True``, messages for
    installations are printed.

    For a description of the concrete kind install information please consult
    the documentation of :py:func:`calc_kind_info`.

    A description of an item element can be found in the documentation of
    function :py:func:`dusetup.util.misc.tokenize_item_element`.

    :raise: a :py:exc:`dusetup.error.SetupError` if installation of an item
            has failed.
    '''
    dst_dpath = kind_info['dst_dpath']

    # 1. Install the item elements to be installed.
    cp_fct = kind_info['cp_fct']
    print(f"---->Use copy function `{cp_fct.__name__}` to copy item(s).")
    raw_cp_kwargs = kind_info['cp_kwargs'].copy()

    for (src_path, inst_params) in item_elems:
        # Determine the version.
        if 'add_vers' in inst_params:
            # - The generalized `add_vers` switch is given as item parameter.
            add_vers = misc.to_bool(inst_params.pop('add_vers'))
        else:
            # - The value of the `add_vers` switch comes from `kind_info`.
            add_vers = kind_info['add_vers']
        app_vers = params['app_vers'] if add_vers else None

        # Add the remaining item parameters to the copy function keyword
        # arguments.
        raw_cp_kwargs.update(inst_params)
        cp_kwargs = misc.format_dict_str_vals(raw_cp_kwargs, params)

        # Call the copy function to install the item(s).
        if kind_info['multi']:
            # Install multiple items.

            # - Function to add the application version.
            def app_vers_dst_fct(p):
                return interpol.add_app_version(p, app_vers)

            # - Check an copy function argument that is not allowed.
            if 'rel_dst_path' in cp_kwargs.keys():
                raise error.SetupError(
                    'Argument `rel_dst_path` not allowed for multiple items.')

            # - Call the copy function.
            cp_fct(src_path, dst_dpath, rel_dst_path_fct=app_vers_dst_fct,
                   verbose=verbose, **cp_kwargs)
        else:
            # Install a single item.

            # - Destination item path.
            if 'rel_dst_path' in cp_kwargs.keys():
                rel_dst_path_tpl = cp_kwargs.pop('rel_dst_path')
                rel_dst_path_bare = rel_dst_path_tpl.format_map(params)
            else:
                rel_dst_path_bare = src_path.name
            rel_dst_path = interpol.add_app_version(
                rel_dst_path_bare, app_vers)
            dst_path = dst_dpath / rel_dst_path

            # - Functions to copy a single item do not have a ``src_crit``
            #   argument.  Therefore we get rid of it but apply it to the
            #   source path, too.
            if 'src_crit' in cp_kwargs.keys():
                src_crit = cp_kwargs.pop('src_crit')
            else:
                src_crit = None

            # - Call the copy function if the source path is not skipped.
            if src_crit is None or src_crit(src_path):
                cp_fct(src_path, dst_path, verbose=verbose, **cp_kwargs)
            else:
                print(f"Skip source path\n  {src_path}")

    # 2. Call the post-installation function if there is one.
    post_fct = kind_info.get('post_fct')
    if post_fct is not None:
        print(f"---->Run post function `{post_fct.__name__}`.")
        post_fct(dst_dpath)


# ----------------------------------------------------------------------------
# Local Variables:
# ispell-local-dictionary: "en"
# End:
