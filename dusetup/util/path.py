# ============================================================================
# Title          : Setup utilities
#
# Classification : Python module
#
# Author         : Dirk Ullrich
#
# Date           : 2023-05-04
#
# Description    : See documentation string below.
# ============================================================================

'''
Helpers related to paths of files or directories.
'''


import pathlib
import shutil
import os

from . import misc


# Functions
# =========


# Components of paths
# -------------------


def has_ext(fext):
    '''
    Create a predicate to test file paths for file extension `fext`.

    :return: a function ``pred`` such that ``pred(fpath)`` is ``True`` iff
             file path ``fpath`` has file extension `ext`.
    '''

    def pred(fpath):
        fpath = pathlib.Path(fpath)
        return (fpath.suffix == os.extsep + fext)

    return pred


def add_prefix(path, prefix_dpath=None):
    '''
    Add an optional directory prefix path to a path.

    :return: path `path` prefixed with prefix directory with path
             `prefix_dpath` if `prefix_dpath` is not ``None``, and `path`
             itself otherwise.
    '''
    if prefix_dpath is None:
        prefix_dpath = pathlib.Path('')
    else:
        prefix_dpath = pathlib.Path(prefix_dpath)

    prefixed_path = prefix_dpath / path

    return prefixed_path


# Environment
# -----------


def add_path_envvar(var_name, add_path, place='end', keep_empty=True,
                    verbose=True):
    '''
    Extend the search path stored in an environment variable.

    Extend search path value of environment variable with name `var_name` by
    additional path `add_path`.

    String `place` controls where the additional path is added:

    + at the begin, if `place` has value ``'beg'``,

    + at the end, if `place` has value ``'end'``.

    If generalized switch switch `keep_empty` represents ``True`` even an
    empty original value of the `var_name` variable is preserved.  Please note
    that a path separator at the `place`'s end of the old value of the
    `var_name` variable is always preserved.

    If generalized switch `verbose` represents ``True``, a message with the
    new value of the environment variable is printed.
    '''
    # Get the old paths.
    old_val = os.getenv(var_name)
    old_paths = '' if old_val is None else old_val

    # What path separators are needed?
    # - Store whether the old paths have at the `place`'s end a path separator
    #   -> this one must be ensured for the new value, too.
    ensure_place_sep = False
    # - A maybe path separator between old paths and additional path.
    maybe_sep = ''
    if len(old_paths) > 0:
        # If the old value has already a path separator at `place`'s end we do
        # not need to insert one between old and the additional path,
        # but a path separator has to be ensured at `place`'s end after the
        # additional path was added.
        if place == 'end' and old_paths.endswith(os.pathsep):
            ensure_place_sep = True
        elif place == 'beg' and old_paths.startswith(os.pathsep):
            ensure_place_sep = True
        else:
            maybe_sep = os.pathsep
    elif misc.to_bool(keep_empty):
        # If an empty old value should be kept, we need a path separator at
        # `place`'s end of the new value.
        maybe_sep = os.pathsep

    # Add the additional path at `place`'s end, and ensure a path separator
    # there, if needed.
    if place == 'end':
        new_paths = old_paths + maybe_sep + str(add_path)
        if ensure_place_sep and not new_paths.endswith(os.pathsep):
            new_paths += os.pathsep
    elif place == 'beg':
        new_paths = str(add_path) + maybe_sep + old_paths
        if ensure_place_sep and not new_paths.startswith(os.pathsep):
            new_paths = os.pathsep + new_paths

    # Set the environment variable anew.
    if misc.to_bool(verbose):
        print(f"New value of `{var_name}` is\n  {new_paths}")
    os.environ[var_name] = new_paths


# Directories
# -----------


def ls_dir(dpath, depth='min', crit=None, abspath=True):
    '''
    List the entries below directory with path `dpath`.

    The entries below this directory are listed for depth defined by string
    `depth`:

    + If `depth` has value ``'min'``, only entries below the `dpath` directory
      with minimal depth, i.e. in the `dpath` directory, are listed.

    + If `depth` has value ``'max'``, only entries below the `dpath` directory
      with maximal depth are listed.

    + If `depth` has value ``'all'``, all entries below the `dpath` directory
      are listed.

    If `crit` is a not-``None`` path predicate only those entries are listed
    whose path fulfills `crit`.

    If generalized switch `abspath` represents ``True`` the absolute paths of
    the entries are collected, and paths of the entries relative to `dpath`
    otherwise.

    :return: list of the collected entries.
    '''
    dpath = pathlib.Path(dpath)
    use_abspath = misc.to_bool(abspath)

    if crit is None:
        def crit(p):
            return True

    if depth == 'all':
        entries = _ls_dir_all_depth(dpath, crit, use_abspath)
    elif depth == 'min':
        entries = _ls_dir_min_depth(dpath, crit, use_abspath)
    elif depth == 'max':
        entries = _ls_dir_max_depth(dpath, crit, use_abspath)

    return entries


def _ls_dir_min_depth(dpath, crit, abspath=True):
    '''
    Helper function for :py:func:`ls_dir` for the minimal-depth case.

    This case is characterized by value ``'min'`` of the `depth` argument.
    '''
    entries_paths = [p for p in dpath.iterdir() if crit(p)]

    if abspath:
        entries = [p.absolute() for p in entries_paths]
    else:
        entries = [p.name for p in entries_paths]

    return entries


def _ls_dir_all_depth(dpath, crit, abspath=True):
    '''
    Helper function for :py:func:`ls_dir` for the minimal-depth case.

    This case is characterized by value ``'all'`` of the `depth` argument.
    '''
    entries_paths = []
    for (d, dnames_d, fnames_d) in os.walk(dpath):
        d = pathlib.Path(d)
        # All entry names.
        all_entries_names_d = dnames_d + fnames_d
        # Check criteria + use the variant of path wanted.
        for n in all_entries_names_d:
            path_d_n = d / n
            if crit(path_d_n):
                if abspath:
                    entry_path_d_n = path_d_n.absolute()
                else:
                    entry_path_d_n = path_d_n.relative_to(dpath)
                entries_paths.append(entry_path_d_n)

    return entries_paths


def _ls_dir_max_depth(dpath, crit, abspath=True):
    '''
    Helper function for :py:func:`ls_dir` for the minimal-depth case.

    This case is characterized by value ``'max'`` of the `depth` argument.
    '''
    entries_paths = []
    for (d, dnames_d, fnames_d) in os.walk(dpath):
        d = pathlib.Path(d)
        # Collect all max-depth paths.
        max_depth_paths_d = []
        # - A directory path has maximal depth if its directory is empty.
        if dnames_d == [] and fnames_d == []:
            max_depth_paths_d.append(d)
        # - All file paths have maximal depth.
        for n in fnames_d:
            fpath_d_n = d / n
            max_depth_paths_d.append(fpath_d_n)
        # Check criteria + use the variant of path wanted.
        for p in max_depth_paths_d:
            if crit(p):
                if abspath:
                    entry_path_d_p = p.absolute()
                else:
                    entry_path_d_p = p.relative_to(dpath)
                entries_paths.append(entry_path_d_p)

    return entries_paths


def ensure_dir(dpath, mode=None, verbose=False):
    '''
    Ensure that directory with path `dpath` exists.

    A not-``None`` permission specification `mode` is used to set the
    permissions of the directory.

    If generalized switch `verbose` represents ``True``, a message is printed
    if the directory has to be created.
    '''
    dpath = pathlib.Path(dpath)
    if not dpath.is_dir():
        if misc.to_bool(verbose):
            print(f"Create directory\n  {dpath}")
        dpath.mkdir(parents=True, exist_ok=True)
    if mode is not None:
        int_mode = misc.to_int(mode)
        os.chmod(dpath, int_mode)


def ensure_clean_dir(dpath, verbose=False):
    '''
    Ensure that directory with path `dpath` exists and is empty.

    If `verbose` is ``True`` messages for the performed actions are printed.
    '''
    dpath = pathlib.Path(dpath)
    if dpath.is_dir():
        for p in dpath.iterdir():
            rm_item(p, verbose)
    else:
        if verbose:
            print(f"Create directory\n  {dpath}")
        dpath.mkdir(parents=True)


# Removing
# --------


def rm_item(path, verbose=True):
    '''
    Remove an existing file or directory with path `path`.

    For a directory its content is removed, too.

    If generalized switch `verbose` represents ``True``, a message for
    directory removal is printed.
    '''
    path = pathlib.Path(path)

    if path.is_file():
        if misc.to_bool(verbose):
            print(f"Remove file\n  {path}")
        path.unlink()
    elif path.is_dir():
        if verbose:
            print(f"Remove  directory\n  {path}")
        shutil.rmtree(path)


def rm_items_of_dir(dpath, crit=None, recursive=False, verbose=True):
    '''
    Remove existing files or directories in directory with path `dpath`.

    If `crit` is a not-``None`` path predicate only those entries are removed
    whose path fulfills `crit`.

    If generalized switch `recursive` represents ``True`` all removal
    candidates below the `dpath` directory are removed instead of only those
    directly within the `dpath` directory.

    If generalized switch `verbose` represents ``True`` a message about
    removing is printed.
    '''
    depth = 'max' if misc.to_bool(recursive) else 'min'
    removal_paths = ls_dir(dpath, depth, crit)
    for p in removal_paths:
        rm_item(p, verbose)


# Copying and moving
# ------------------


def cpmv_file(src_fpath, dst_fpath, rm_src=False, fmode=None, dmode=None,
              cp_file_fct=None, verbose=True):
    '''
    Copy or move file with path `src_fpath` to path `dst_fpath`.

    The directory for the destination file will be created if it does not
    already exist.

    If generalized switch `rm_src` represents ``True`` the source file is
    removed, too.

    A not-``None`` permission specification `fmode` is used to set the
    permissions of the destination file.

    A not-``None`` permission specification `dmode` is used to set the
    permissions of all destination directories.

    If `cp_file_fct` is a not-``None`` function this function is used to copy
    the file instead of :py:func:`shutil.copy2`.

    If generalized switch `verbose` represents ``True`` a message about
    copying / moving is printed.
    '''
    src_fpath = pathlib.Path(src_fpath)
    dst_fpath = pathlib.Path(dst_fpath)
    src_to_rm = misc.to_bool(rm_src)

    if misc.to_bool(verbose):
        if src_to_rm:
            op_name = 'Move'
        else:
            op_name = 'Copy'
        print(f"{op_name} file\n  {src_fpath}\n to\n  {dst_fpath}")

    # We ensure that the destination directory exists.
    dst_dpath = dst_fpath.parent
    int_dmode = misc.to_int(dmode)
    ensure_dir(dst_dpath, mode=int_dmode, verbose=False)

    # Copy the source file to the destination.
    if cp_file_fct is None:
        cp_fct = shutil.copy2
    cp_fct(src_fpath, dst_fpath)

    # Set the permission of the destination, if wanted.
    if fmode is not None:
        int_fmode = misc.to_int(fmode)
        dst_fpath.chmod(int_fmode)

    # Remove the source file if wanted.
    if src_to_rm:
        src_fpath.unlink()


def cpmv_dir(src_dpath, dst_dpath, rm_src=False, fmode=None, dmode=None,
             cp_file_fct=None, verbose=True):
    '''
    Copy recursively or move a directory.

    Directory with path `src_dpath` is copied recursively or moved to path
    `dst_dpath`.  The parent directories for the destination directory will be
    created if they do not already exist.

    If generalized switch `rm_src` represents ``True`` the source file is
    removed, too.

    A not-``None`` permission specification `fmode` is used to set the
    permissions of all destination files.

    A not-``None`` permission specification `dmode` is used to set the
    permissions of all destination directories.

    If `cp_file_fct` is a not-``None`` function this function is used to copy
    the files instead of :py:func:`shutil.copy2`.

    If generalized switch `verbose` represents ``True`` a message about
    copying / moving is printed.
    '''
    src_dpath = pathlib.Path(src_dpath)
    dst_dpath = pathlib.Path(dst_dpath)
    src_to_rm = misc.to_bool(rm_src)

    if misc.to_bool(verbose):
        if src_to_rm:
            op_name = 'Move'
        else:
            op_name = 'Copy'
        print(f"{op_name} directory\n  {src_dpath}\n to\n  {dst_dpath}")

    # Ensure destination directory.
    ensure_dir(dst_dpath, dmode, verbose)

    # Copy content of source directory.
    src_paths = ls_dir(src_dpath, depth='max', abspath=False)
    for p in src_paths:
        src_path_p = src_dpath / p
        dst_path_p = dst_dpath / p
        if src_path_p.is_dir():
            ensure_dir(dst_path_p, dmode, verbose=False)
        else:
            cpmv_file(
                src_path_p, dst_path_p, rm_src, fmode, dmode, cp_file_fct,
                verbose=False)

    # Remove source directory (with remaining empty directories), if wanted.
    if src_to_rm:
        shutil.rmtree(src_dpath)


def cpmv_item(src_path, dst_path, rm_src=False, fmode=None, dmode=None,
              cp_file_fct=None, verbose=True):
    '''
    Copy (recursively) or move existing file or directory.

    Existing file or directory with path `src_path` is (recursively) copied or
    moved to path `dst_path`.  The destination directory (if the source item
    is a file) or the parent directories for the destination directory (if the
    source item is a directory) will be created if they do not already exist.

    If generalized switch `rm_src` represents ``True`` the source file is
    removed, too.

    A not-``None`` permission specification `fmode` is used to set the
    permissions of all destination files.

    A not-``None`` permission specification `dmode` is used to set the
    permissions of all destination directories.

    If `cp_file_fct` is a not-``None`` function this function is used to copy
    the files instead of :py:func:`shutil.copy2`.

    If generalized switch `verbose` represents ``True`` a message about
    copying / moving is printed.
    '''
    src_path = pathlib.Path(src_path)
    if src_path.is_file():
        cpmv_file(
            src_path, dst_path, rm_src, fmode, dmode, cp_file_fct, verbose)
    elif src_path.is_dir():
        cpmv_dir(
            src_path, dst_path, rm_src, fmode, dmode, cp_file_fct, verbose)


def cpmv_items(src_paths, dst_dpath, src_crit=None, dst_path_fct=None,
               rm_src=False, fmode=None, dmode=None, cp_file_fct=None,
               verbose=True):
    '''
    Copy (recursively) or move existing files or directories.

    Existing files or directories with paths in list `src_paths` below
    directory with path `dst_path` are recursively copied or moved.  For every
    item, the destination directory (if the source item is a file) or the
    parent directories for the destination directory (if the source item is a
    directory) will be created if they do not already exist.

    If `src_crit` is a not-``None`` predicate for paths, only those items are
    copied whose path meets `src_crit`.

    If `dst_path_fct` is a function it is used to calculate the destination
    path of every copied item.  It is expected to have two path arguments, and
    the destination path for an item to be copied is the value of calling
    `dst_path_fct` with `dst_path` as 1st and the source path of that item as
    2nd argument.  If `dst_path_fct` is ``None``, the destination path for a
    item to be copied has `dst_dpath` as directory part and the item's name as
    name part.

    If generalized switch `rm_src` represents ``True`` the source file is
    removed, too.

    A not-``None`` permission specification `fmode` is used to set the
    permissions of all destination files.

    A not-``None`` permission specification `dmode` is used to set the
    permissions of all destination directories.

    If `cp_file_fct` is a not-``None`` function this function is used to copy
    the files instead of :py:func:`shutil.copy2`.

    If generalized switch `verbose` represents ``True`` a message about
    copying / moving is printed.
    '''
    if src_crit is None:
        src_paths_todo = src_paths
    else:
        src_paths_todo = [p for p in src_paths if src_crit(p)]

    if dst_path_fct is None:
        def dst_path_fct(dpath, src_path):
            return dpath / src_path.name

    for p in src_paths_todo:
        dst_dpath_p = dst_path_fct(dst_dpath, p)
        cpmv_item(p, dst_dpath_p, rm_src, fmode, dmode, cp_file_fct, verbose)


def cpmv_items_of_dir(src_dpath, dst_dpath, src_crit=None, recursive=False,
                      rel_dst_path_fct=None, rm_src=False, fmode=None,
                      dmode=None, cp_file_fct=None, verbose=True):
    '''
    Copy or move all files or directories below a directory.

    All files or directories below directory with path `src_dpath` are copied
    or moved to an item below directory with path `dst_dpath`.

    Actually, any copied item either uses as its path relative to `dst_dpath`:

    + the relative path for the name of the respective source item, if
      generalized switch `recursive` represents ``False``, or

    + the same path of the respective source item relative to `src_dpath`, if
      generalized switch `recursive` represents ``True``.

    For a non-``None`` function `rel_dst_path_fct` for paths the value of this
    relative destination path is used instead of this path itself.

    If `src_crit` is a not-``None`` predicate for paths, only those items are
    copied whose path meets `src_crit`.

    If generalized switch `rm_src` represents ``True`` the source file is
    removed, too.

    A not-`None`` permission specification `fmode` is used to set the
    permissions of all destination files.

    A not-`None`` permission specification `dmode` is used to set the
    permissions of all destination directories.

    If `cp_file_fct` is a not-``None`` function this function is used to copy
    the files instead of :py:func:`shutil.copy2`.

    If generalized switch `verbose` represents ``True`` a message about
    copying / moving is printed.
    '''
    ls_depth = 'all' if recursive else 'min'
    src_paths_todo = ls_dir(src_dpath, ls_depth, src_crit, abspath=True)

    if rel_dst_path_fct is None:
        def rel_dst_path_fct(rel_path):
            return rel_path

    if misc.to_bool(recursive):
        def dst_path_fct(dpath, src_path):
            rel_src_path = os.path.relpath(src_path, start=src_dpath)
            rel_dst_path = rel_dst_path_fct(rel_src_path)
            return dpath / rel_dst_path
    else:
        def dst_path_fct(dpath, src_path):
            src_name = src_path.name
            dst_name = rel_dst_path_fct(src_name)
            return dpath / dst_name

    cpmv_items(
        src_paths_todo, dst_dpath, dst_path_fct=dst_path_fct, rm_src=rm_src,
        fmode=fmode, dmode=dmode, cp_file_fct=cp_file_fct, verbose=verbose)


def cpmv_files_of_dir(src_dpath, dst_dpath, src_crit=None, recursive=False,
                      rel_dst_path_fct=None, rm_src=False, fmode=None,
                      dmode=None, cp_file_fct=None, verbose=True):
    '''
    Copy or move all files below a directory.

    All files below directory with path `src_dpath` are copied or moved to an
    item below directory with path `dst_dpath`.

    Actually, any copied file either uses as its path relative to `dst_dpath`:

    + the relative path for the name of the respective source item, if
      generalized switch `recursive` represents ``False``, or

    + the same path of the respective source item relative to `src_dpath`, if
      generalized switch `recursive` represents ``True``.

    For a non-``None`` function `rel_dst_path_fct` for paths the value of this
    relative destination path is used instead of this path itself.

    If `src_crit` is a not-``None`` predicate for paths, only those files are
    copied whose path meets `src_crit`.

    If generalized switch `rm_src` represents ``True`` the source file is
    removed, too.

    A not-`None`` permission specification `fmode` is used to set the
    permissions of all destination files.

    A not-`None`` permission specification `dmode` is used to set the
    permissions of all destination directories.

    If `cp_file_fct` is a not-``None`` function this function is used to copy
    the files instead of :py:func:`shutil.copy2`.

    If generalized switch `verbose` represents ``True`` a message about
    copying / moving is printed.
    '''
    if src_crit is not None:
        def is_candidate(p):
            return (src_crit(p) and p.is_file())
    else:
        def is_candidate(p):
            return p.is_file()

    cpmv_items_of_dir(
        src_dpath, dst_dpath, is_candidate, recursive, rel_dst_path_fct,
        rm_src, fmode, dmode, cp_file_fct, verbose)


# Linking
# -------


def symlink_item(src_path, dst_path, dst_as_dir=False, mk_relative=True,
                 verbose=True):
    '''
    Symlink a file or directory.

    File or directory with path `src_path` is symlinked to path `dst_path`.

    The link directory will be created if they do not already exist.

    If generalized switch `dst_as_dir` represents ``True`` path `dst_path` is
    supposed to be the path of a directory, and `dst_path` is used as path for
    the directory of the link instead as path of the link itself.

    If generalized switch `mk_relative` represents ``True``, the created link
    is a relative one instead of an absolute one.

    If generalized switch `verbose` represents ``True`` a message about
    copying / moving is printed.
    '''
    src_path = pathlib.Path(src_path)
    dst_path = pathlib.Path(dst_path)

    # Calculate the proper link path and its directory path.
    if misc.to_bool(dst_as_dir):
        src_name = src_path.name
        lnk_path = dst_path / src_name
        lnk_dpath = dst_path
    else:
        lnk_path = dst_path
        lnk_dpath = dst_path.parent

    # We ensure that the link's directory exists.
    lnk_dpath.mkdir(parents=True, exist_ok=True)

    # Use the right variant for the link target path.
    if misc.to_bool(mk_relative):
        tgt_path = os.path.relpath(src_path, start=lnk_dpath)
    else:
        tgt_path = lnk_path.absolute()

    # Create the symlink.
    if misc.to_bool(verbose):
        print(f"Symlink\n  {tgt_path}\n to\n  {lnk_path}")
    lnk_path.symlink_to(tgt_path)


def symlink_items(src_paths, dst_dpath, src_crit=None, mk_relative=True,
                  verbose=True):
    '''
    Symlink files or directories given by a list of paths.

    The files or directories with paths in list `src_paths` are symlinked to
    links with same name in directory with path `dst_dpath`.  This directory
    will be created if they do not already exist.

    If `src_crit` is a not-``None`` predicate for paths, only those items are
    linked whose path meets `src_crit`.

    If generalized switch `mk_relative` represents ``True``, the created link
    is a relative one instead of an absolute one.

    If generalized switch `verbose` represents ``True`` a message about
    copying / moving is printed.
    '''
    for p in src_paths:
        if src_crit is None or src_crit(p):
            symlink_item(
                p, dst_dpath, dst_as_dir=True,
                mk_relative=mk_relative, verbose=verbose)


def symlink_items_of_dir(src_dpath, dst_dpath, src_crit=None,
                         mk_relative=True, verbose=True):
    '''
    Symlink files or directories in a directory.

    The files or directories in directory with path `src_dpath` are symlinked
    to links with same name in directory with path `dst_dpath`.  This
    directory will be created if they do not already exist.

    If `src_crit` is a not-``None`` predicate for paths, only those items are
    linked whose path meets `src_crit`.

    If generalized switch `mk_relative` represents ``True``, the created link
    is a relative one instead of an absolute one.

    If generalized switch `verbose` represents ``True`` a message about
    copying / moving is printed.
    '''
    src_paths = ls_dir(src_dpath, depth='min', crit=src_crit)
    symlink_items(
        src_paths, dst_dpath, mk_relative=mk_relative, verbose=verbose)


# ----------------------------------------------------------------------------
# Local Variables:
# ispell-local-dictionary: "en"
# End:
