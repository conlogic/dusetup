# ============================================================================
# Title          : Setup utilities
#
# Classification : Python module
#
# Author         : Dirk Ullrich
#
# Date           : 2023-05-04
#
# Description    : See documentation string below.
# ============================================================================

'''
Helpers for Python files.
'''


import pathlib
import site
import compileall


def rel_python_site_dir():
    '''
    Relative path of the Python package site directory.

    :return: path for Python package site directory relative to the prefix
             directory path.
    '''
    # Calculate the relative site directory path.
    # - User's site directory path.
    user_site_dpath = pathlib.Path(site.USER_SITE)
    # - User's base directory path.
    user_base_dpath = site.USER_BASE
    # Remove the user's base directory path
    rel_site_dpath = user_site_dpath.relative_to(user_base_dpath)

    return rel_site_dpath


def compile_python_item(path):
    '''
    Byte-compile a Python module or package.

    The Python module (i.e. file) or package (i.e. directory) with path `path`
    is byte-compiled.
    '''
    path = pathlib.Path(path)

    if path.is_file():
        compileall.compile_file(path)
    elif path.is_dir():
        compileall.compile_dir(path)


# ----------------------------------------------------------------------------
# Local Variables:
# ispell-local-dictionary: "en"
# End:
