# ============================================================================
# Title          : Setup utilities
#
# Classification : Python module
#
# Author         : Dirk Ullrich
#
# Date           : 2023-05-04
#
# Description    : See documentation string below.
# ============================================================================

'''
Utilities for documentation files.
'''


import re
import os
import pathlib

from . import path
from . import run
from . import misc
from .. import error


# Parameters
# ==========

#: Regular expression to match a numbered INFO file extension.
INFO_FEXT_RX = re.escape(os.extsep + 'info') + r'(-\d+)?$'

#: List of known output formats Texinfo files can transformed into.
TEXI_OUT_FORMATS = ['info', 'pdf', 'html']

#: Default name of the Info directory file.
INFO_DIR_FNAME = 'dir'


# Functions
# =========


def is_texi_file(fpath):
    '''
    Test for Texinfo files.

    :return: ``True`` if `fpath` is a Texinfo file path.
    '''
    fpath = pathlib.Path(fpath)

    is_texi = False
    if fpath.is_file():
        if path.has_ext('texi')(fpath):
            is_texi = True

        elif path.has_ext('texinfo')(fpath):
            is_texi = True

    return is_texi


def is_info_file(fpath):
    '''
    Test for Info files.

    :return: ``True`` if `fpath` is an Info file path.
    '''
    fpath = pathlib.Path(fpath)

    is_info = False
    if fpath.is_file():
        # INFO files can have file extension ``.info-N`` where `N` is a
        # number.
        info_matcher = re.search(INFO_FEXT_RX, str(fpath))
        is_info = info_matcher is not None

    return is_info


def texi2any_file(texi_fpath, add_opts=[], out_format='info', out_dpath=None,
                  verbose=True):
    '''
    Transform a Texinfo file.

    Texinfo file with path `texi_fpath` is transformed to file(s) with format
    `out_format`.

    The options in list `add_opts` are passed to the transformation program.

    If `out_dpath` is a not-``None`` path, the directory with this path is
    used to store the output file(s) instead of the directory for
    `texi_fpath`.

    If generalized switch `verbose` represents ``True``, messages for
    installations are printed.

    :raise: a :py:exc:`dusetup.error.SetupError` if transformation has failed.
    '''
    texi_fpath = pathlib.Path(texi_fpath)
    if out_dpath is None:
        out_dpath = texi_fpath.parent
    else:
        out_dpath = pathlib.Path(out_dpath)

    if out_format not in TEXI_OUT_FORMATS:
        raise error.SetupError(
            f"Unknown Texinfo output format `{out_format}`.")

    cmd = ['texi2any', f"--{out_format}", '--no-split']
    cmd.extend(add_opts)
    texi_fbase = texi_fpath.stem
    out_fname = texi_fbase + os.extsep + out_format
    out_fpath = out_dpath / out_fname
    if misc.to_bool(verbose):
        msg = f"Transform Texinfo file at\n  {texi_fpath}\n"
        msg += f"to `{out_format}` format."
        print(msg)
    cmd.extend([f"--output={out_fpath}", texi_fpath])
    out_dpath.mkdir(parents=True, exist_ok=True)
    run.run_cmd(cmd, verbose=False)


def texi2any_files_of_dir(texi_dpath, crit=None, add_opts=[],
                          out_format='info', out_dpath=None, verbose=True):
    '''
    Transform all Texinfo files in a directory.

    All Texinfo files in directory with path <texi_dpath> are transformed to
    files with format `out_format`.

    If `crit` is a not-``None`` path predicate only those Texinfo files are
    transformed whose path fulfills `crit`.

    The options in list `add_opts` are passed to the transformation program.

    If `out_dpath` is a not-``None`` path, the directory with this path is
    used to store the output file(s) instead of the `texi_dpath` directory.

    If generalized switch `verbose` represents ``True``, messages for
    installations are printed.

    :raise: a :py:exc:`dusetup.error.SetupError` if transformation has failed.
    '''
    # Extend the ELisp file criteria if needed.
    if crit is not None:
        def is_candidate(f):
            return (crit(f) and is_texi_file(f))
    else:
        def is_candidate(f):
            return is_texi_file(f)

    if out_dpath is None:
        out_dpath = texi_dpath

    for f in path.ls_dir(
            texi_dpath, depth='min', crit=is_candidate, abspath=True):
        texi2any_file(f, add_opts, out_format, out_dpath, verbose)


def install_info_files_of_dir(info_dpath, info_dir_fname=INFO_DIR_FNAME,
                              add_opts=[], verbose=True):
    '''
    Install all Info files from a directory.

    All Info files in directory with path `info_dpath` are installed in Info
    directory file with name `info_dir_fname`.

    The options in list `add_opts` are additionally used for installation.

    If generalized switch `verbose` represents ``True``, messages for
    installations are printed.

    :raise: a :py:exc:`dusetup.error.SetupError` if installation has failed.
    '''
    info_fpaths = path.ls_dir(
        info_dpath, depth='min', crit=is_info_file, abspath=True)

    for f in info_fpaths:
        if misc.to_bool(verbose):
            print(f"Install Info file at\n  {f}")
        cmd_f = ['install-info']
        cmd_f.extend(add_opts)
        cmd_f.extend([str(f), info_dir_fname])
        # We want relative entries for the installed Info files in the Info
        # directory file.
        run.run_cmd(cmd_f, verbose=False, shell=False, cwd=info_dpath)


# ----------------------------------------------------------------------------
# Local Variables:
# ispell-local-dictionary: "en"
# End:
