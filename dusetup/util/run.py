# ============================================================================
# Title          : Setup utilities
#
# Classification : Python module
#
# Author         : Dirk Ullrich
#
# Date           : 2022-05-24
#
# Description    : See documentation string below.
# ============================================================================

'''
Helpers to run external commands.
'''


import subprocess
import shlex

from .. import error
from . import misc


# Functions
# =========


def run_cmd(cmd, error_rc=None, verbose=True, **kwargs):
    '''
    Run command given by string or list `cmd`.

    The keyword arguments in dictionary `kwargs` are passed to
    :py:func:`subprocess.run`.

    A not-``None`` number `error_rc` is used as return code in a raised error
    exception instead of the original one from the run result.

    If generalized switch `verbose` represents ``True`` a message about
    copying / moving is printed.

    :return: the result of the command.

    :raise: a :py:exc:`dusetup.error.SetupError` if running of the command has
            failed.
    '''
    # Put the command into the proper format.
    use_shell = misc.to_bool(kwargs.get('shell', False))
    cmd_is_str = isinstance(cmd, str)
    if use_shell and not cmd_is_str:
        # Using a shell and getting the command as list it must be converted
        # into a string.
        used_cmd = ' '.join([shlex.quote(n) for n in cmd])
    elif not use_shell and cmd_is_str:
        # Using no shell and getting the command as string it must be
        # converted into a list.
        used_cmd = shlex.split(cmd)
    else:
        used_cmd = cmd

    # Try to run the command.
    error_exc = None
    try:
        if misc.to_bool(verbose):
            print(f"Run command\n  {used_cmd}")
        # Pass a proper Boolean for ``shell`` to :py:func:`subprocess.run`.
        kwargs['shell'] = use_shell
        result = subprocess.run(used_cmd, check=True, **kwargs)
    except subprocess.CalledProcessError as e:
        error_exc = e
        error_rc = e.returncode if error_rc is None else error_rc
    except OSError as e:
        error_exc = e
        error_rc = e.errno if error_rc is None else error_rc
    if error_exc is not None:
        raise error.SetupError(
            f"Command\n  {used_cmd}\n has failed with error\n  {error_exc}",
            rc=error_rc)

    return result


def run_cmds(cmds, first_error_rc=None, verbose=True, **kwargs):
    '''
    Run commands given by list `cmds`.

    The keyword arguments in dictionary `kwargs` are passed to
    :py:func:`subprocess.run`.

    For a not-``None`` number `first_error_rc` a number `first_error_rc + n`
    is used as return code in an error exception raised by the command
    given by ``n``-th element in `cmds` instead of the original one from the
    run result of this command.

    If generalized switch `verbose` represents ``True`` messages with the
    commands to be run are printed.

    :return: a list with the results of the commands.

    :raise: a :py:exc:`dusetup.error.SetupError` if running of a command has
            failed.
    '''
    error_rc = first_error_rc
    results = []
    for c in cmds:
        result = run_cmd(c, error_rc=error_rc, verbose=verbose, **kwargs)
        results.append(result)
        if error_rc is not None:
            error_rc += 1

    return results


# ----------------------------------------------------------------------------
# Local Variables:
# ispell-local-dictionary: "en"
# End:
