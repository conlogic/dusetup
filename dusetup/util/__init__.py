# ============================================================================
# Title          : Setup utilities
#
# Classification : Python module
#
# Author         : Dirk Ullrich
#
# Date           : 2023-05-04
#
# Description    : See documentation string below.
# ============================================================================

'''
Helpers used elsewhere in the :py:mod`dusetup` package

These helpers are especially as building blocks of setup step functions.
'''

# ----------------------------------------------------------------------------
# Local Variables:
# ispell-local-dictionary: "en"
# End:
