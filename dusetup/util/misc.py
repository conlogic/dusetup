# ============================================================================
# Title          : Setup utilities
#
# Classification : Python module
#
# Author         : Dirk Ullrich
#
# Date           : 2023-05-04
#
# Description    : See documentation string below.
# ============================================================================

'''
Unspecific helpers to be used elsewhere in :py:mod:`dusetup`.
'''


import pathlib
import re
import sys
import importlib
import collections as colls

from .. import error


# Parameters
# ==========

#: Separator for parts of value strings.
PART_SEP = ':'

#: Separator between name and value in value string parts that are name value
#: pairs.
NAME_VAL_SEP = '='


# Functions
# =========


def kind_item_elements_from_cfg_key(params, sect, kind_key,
                                    part_sep=PART_SEP,
                                    name_val_sep=NAME_VAL_SEP):
    '''
    Get the item element or item elements for a kind.

    For a kind given by key `kind_key` the item element or item elements are
    gotten from section `sect` in the configuration from setup parameter
    dictionary `params`.  The item element or the item elements are given as
    string or list of strings value of key `kind_key` that represents a kind
    ``<kind>``; in the former case ``<kind>`` is `kind_key` itself, but in the
    latter case ``<kind>`` is `kind_key` without the trailing ``'s'``
    signaling a list of strings.

    For the tokenization of item element strings the separators `part_sep` and
    `name_val_sep` are used.  For a description of item elements and their
    tokenization please consult the documentation of function
    :py:func:`tokenize_item_element`.

    :return: a pair with ``<kind>`` and the list of item elements associated
             with ``<kind>``.

    :raise: a :py:exc:`duconfig.ConfigError` for a configuration error.
    :raise: a :py:exc:`dusetup.error.SetupError` if installation of an item
            has failed.
    '''
    # Get the kind's value.
    cfg = params['cfg']
    cfg_items_sect = cfg[sect]
    kind_val = cfg_items_sect[kind_key]

    # Get the kind and the list of elements for it.
    if kind_key.endswith('s'):
        # List of element strings.
        kind = kind_key[:-1]
        elems = [
            tokenize_item_element(params, i, part_sep, name_val_sep)
            for i in kind_val
        ]
    else:
        # Single element string.
        kind = kind_key
        elems = [
            tokenize_item_element(params, kind_val, part_sep, name_val_sep)]

    return (kind, elems)


def tokenize_item_element(params, elem_str,
                          part_sep=PART_SEP, name_val_sep=NAME_VAL_SEP):
    '''
    Tokenize string `elem_str` representing an item element.

    Such an item element consists of:

    + A mandatory 1st part that is the path template of an item, i.e. a file
      or directory.  The placeholders in the path template are filled from
      setup parameter dictionary `params`.

    + Optional further parts that are parameters given as key value pairs.
      String values can also be templates; hence they are filled from setup
      parameter dictionary `params`, too.

    String `part_sep` is used as separator between the parts in `elem_str`,
    and string `name_val_sep` to separate keys and values in the parameter
    parts of `elem_str`.

    :return: pair of the item path and a dictionary with the parameters.

    :raise: a :py:exc:`dusetup.error.SetupError` if the tokenization fails.
    '''
    # Tokenize the element string.
    try:
        (elem_str_hd, raw_elem_params) = tokenize_val(
            elem_str, split_nr=1,
            part_sep=part_sep, name_val_sep=name_val_sep)
    except error.SetupError as e:
        msg = f"Tokenization of element string\n  {elem_str}\n"
        msg += f" has failed with error\n  {e}"
        raise error.SetupError(msg)

    # Get the real item path.
    elem_path_tpl = elem_str_hd[0]
    elem_path = pathlib.Path(elem_path_tpl.format_map(params))

    # Fill possible setup parameter placeholders in parameter string values.
    elem_params = format_dict_str_vals(raw_elem_params, params)

    return (elem_path, elem_params)


def tokenize_item_kind(kind, kind_type_info, only_kind_types=None):
    '''
    Tokenize item kind `kind` into it type and item variant.

    The item kind type is looked up in information dictionary `kind_type_info`
    - but only if `only_kind_types` is either ``None`` or a generalized tuple
    that contains the kind type of `kind`.  Otherwise ``None`` is used as item
    kind information.

    :return: a pair of item kind type information and item kind item variant.

    :raise: a :py:exc:`dusetup.error.SetupError` if lookup has failed,
    '''
    kind_idx = kind.find('_')
    if kind_idx == -1:
        msg = f"Malformed item kind `{kind}`.\n"
        msg += '  It must have the form `<type>_<item(s)>`.'
        raise error.SetupError(msg)
    type_info_key = kind[:kind_idx]
    item_info_key = kind[kind_idx + 1:]

    # Get the type info for `kind`, if wanted.
    if only_kind_types is None or type_info_key in to_tuple(only_kind_types):
        if type_info_key not in kind_type_info.keys():
            raise error.SetupError(
                f"Unknown item kind type `{type_info_key}`.")
        type_info = kind_type_info[type_info_key]
    else:
        type_info = None

    return (type_info, item_info_key)


def tokenize_val(val_str, split_nr=None,
                 part_sep=PART_SEP, name_val_sep=NAME_VAL_SEP):
    '''
    Tokenize value string `val_str`.

    In this tokenization the parts are split at separator `part_sep`.  These
    value parts are divided into head and tail parts:

    + If `split_nr` is an integer, then maximal `part_sep` many parts from the
      head are taken as head parts, and the remaining parts as tail parts.

    + If `split_nr` is ``None``, all parts from the beginning before the first
      part containing `name_val_sep` are taken as head parts, and the
      remaining parts as tail parts.

    The tail parts are always supposed to be name value pairs with name and
    value are separated by `name_val_sep`.

    :return: a pair with the list of the tail parts of `val_str`, and a
             dictionary with the name value items for the tail parts of
             `val_str`.

    :raise: a :py:exc:`dusetup.error.SetupError` if the tokenization fails.
    '''
    # Get the value parts.
    val_parts = val_str.split(part_sep)

    # Take the head value parts away.
    if split_nr is None:
        name_val_sep_rx = f".*{re.escape(name_val_sep)}.*"
        split_nr = match_index(name_val_sep_rx, val_parts)
        split_nr = len(val_parts) if split_nr is None else split_nr
    hd_parts = val_parts[:split_nr]

    # Split the tail value parts into names and values.
    tl_parts = val_parts[split_nr:]
    tl_names_vals = {}
    for s in tl_parts:
        components_s = s.split(name_val_sep)
        # A tail part should exactly consist of 2 components - namely of its
        # name and its value.
        if not len(components_s) == 2:
            raise error.SetupError(f"Malformed name value part `{s}`")
        (name_s, val_s) = components_s
        tl_names_vals[name_s] = val_s

    return (hd_parts, tl_names_vals)


def format_dict_str_vals(dct, repls):
    '''
    Replace placeholders in string values of a dictionary.

    :return: dictionary `dct` in whose string values the placeholders are
             replaced according to map `repls`.
    '''
    result_dct = {}
    for (k, v) in dct.items():
        result_v = v.format_map(repls) if isinstance(v, str) else v
        result_dct[k] = result_v

    return result_dct


def match_index(rx, strs_list):
    '''
    Search for matching strings in a list.

    :return: index of the first element in list `strs_list` of strings
             matching regexp `rx` if there is such an element, and ``None``
             otherwise.
    '''
    rx = re.compile(rx)
    match_idxs = [
        e[0] for e in enumerate(strs_list)
        if re.match(rx, e[1]) is not None]
    if match_idxs == []:
        match_idx = None
    else:
        match_idx = match_idxs[0]

    return match_idx


def get_qualified_attr(qual_attr_name, auto_import=False):
    '''
    Get the attribute for a fully-qualified attribute name.

    For the fully-qualified attribute name `qual_attr_name` the attribute is
    gotten.  A fully-qualified attribute name has the form
    ``<pkg_or_mod_name>.<attr_name>`` where ``<pkg_or_mod_name>`` is the
    name of a package or module such that attribute with name ``<attr_name>``
    is an attribute of that package or module.

    For a ``True`` switch `auto_import` the package or module named in
    `qual_attr_name` is tried to be imported first before getting the
    attribute.  Otherwise it is assumed that this package or module is already
    imported.

    :return: attribute got for `qual_attr_name`.

    :raise: a :py:exc:`dusetup.error.SetupError` if getting of the attribute
            fails for whatever reason.
    '''
    # Split `qual_attr_name` into package / module and pure attribute names.
    split_idx = qual_attr_name.rfind('.')
    if split_idx == -1:
        raise error.SetupError(
            f"String `{qual_attr_name}` is no fully qualified attribute.")
    pkg_or_mod_name = qual_attr_name[:split_idx]
    attr_name = qual_attr_name[split_idx + 1:]

    # Get the package / module from `qual_attr_name`.
    if pkg_or_mod_name in sys.modules:
        pkg_or_mod = sys.modules[pkg_or_mod_name]
    elif auto_import:
        try:
            importlib.import_module(pkg_or_mod_name)
        except ModuleNotFoundError:
            raise error.SetupError(
                f"Module / package `{pkg_or_mod_name}` cannot be found.")
        pkg_or_mod = sys.modules[pkg_or_mod_name]
    else:
        raise error.SetupError(
            f"There is no imported module / package `{pkg_or_mod_name}`.")

    # Get the attribute form `qual_attr_name` from its package / mdoule there.
    try:
        attr = getattr(pkg_or_mod, attr_name)
    except AttributeError:
        msg = f"Module / package `{pkg_or_mod_name}`\n"
        msg += f" has no attribute `{attr_name}`."
        raise error.SetupError(msg)

    return attr


def add_import_dpaths(dpaths):
    '''
    Add paths to the Python import path.

    The paths for existing directories from list `dpaths` are added as Python
    import paths in from of :py:data:`sys.path` in the given order.
    '''
    dpaths_left = dpaths.copy()
    while len(dpaths_left) > 0:
        # To keep the paths' order traverse their list backwards.
        d = pathlib.Path(dpaths_left.pop(-1))
        # Only paths for existing directories are considered.
        if not d.is_dir():
            continue
        normed_d = str(d.resolve())
        # Remove former occurrences of the current directory path.
        while normed_d in sys.path:
            sys.path.remove(normed_d)
        # Add current directory path in front of :py:data:`sys.path`.
        sys.path.insert(0, normed_d)


def to_bool(val):
    '''
    Try to convert value `val` into a Boolean, if possible.

    If `val` is already ``True`` / ``False``, or if it is string that is a
    variant of `true` or `yes` / `false` or `no`, it is converted to ``True``
    / ``False``.

    :return: the Boolean result of conversion, or ``None`` if conversion is
             not possible.
    '''
    bool_val = None
    if isinstance(val, bool):
        bool_val = val
    elif isinstance(val, str):
        lower_val_str = val.lower()
        if lower_val_str in ['true', 'yes']:
            bool_val = True
        elif lower_val_str in ['false', 'no']:
            bool_val = False

    return bool_val


def to_tuple(val, sep=';'):
    '''
    Try to convert value `val` into a tuple.

    If `val` is a string, it is split at separator `sep`, or if `val` is
    another sequence it is converted to a tuple.

    :return: the tuple result of conversion, or ``None`` if conversion is not
             possible.
    '''
    if isinstance(val, colls.abc.Sequence):
        # A string is treated special.
        if isinstance(val, str):
            seq_val = val.split(sep)
        else:
            seq_val = val
        tuple_val = tuple(seq_val)
    else:
        tuple_val = None

    return tuple_val


def to_int(val):
    '''
    Try to convert value `val` into an integer, if possible.

    :return: the integer result of conversion, or ``None`` if conversion is
             not possible.
    '''
    int_val = None
    if isinstance(val, str):
        try:
            # For strings, `int` initialization can guess the correct base for
            # values representing integer literals.
            int_val = int(val, base=0)
        except ValueError:
            pass
    else:
        try:
            # For other types try bar `int` initialization.
            int_val = int(val)
        except (ValueError, TypeError):
            pass

    return int_val


# ----------------------------------------------------------------------------
# Local Variables:
# ispell-local-dictionary: "en"
# End:
