# ============================================================================
# Title          : Setup utilities
#
# Classification : Python module
#
# Author         : Dirk Ullrich
#
# Date           : 2023-05-04
#
# Description    : See documentation string below.
# ============================================================================

'''
Helpers for ELisp files.
'''


import pathlib

from . import path
from . import run
from . import misc


# Functions
# =========


def is_elisp_src_file(fpath):
    '''
    Test for ELisp source files.

    :return: ``True`` if `fpath` is the path of a ELisp source file.
    '''
    is_el = path.has_ext('el')(fpath)

    return is_el


def is_elc_file(fpath):
    '''
    Test for byte-compiled ELisp files.

    :return: ``True`` if `fpath` is the path of a byte-compiled ELisp file.
    '''
    is_elc = path.has_ext('elc')(fpath)

    return is_elc


def is_elisp_file(fpath, with_elc=True):
    '''
    Test for an ELisp file.

    If generalized switch `with_elc` represents ``True``, byte-compiled ELisp
    files are considered as ELisp files. too.

    :return: ``True`` if `fpath` is an ELisp file path.
    '''
    fpath = pathlib.Path(fpath)

    is_elisp = False
    if fpath.is_file():
        if is_elisp_src_file(fpath):
            is_elisp = True
        else:
            is_elisp = misc.to_bool(with_elc) and is_elc_file(fpath)

    return is_elisp


def mk_elc_file(el_fpath, emacs_cmd='emacs', add_emacs_opts=[],
                force_recompile=False, verbose=True):
    '''
    Byte-compile an ELisp source file.

    ELisp source file with path `el_fpath` is byte-compiled using Emacs called
    by `emacs_cmd`.

    The elements of list `add_emacs_opts` are passed as additional options to
    the Emacs command.

    For a generalized switch `force_recompile` that represents ``True`` the
    file is even byte-compiled again if it was already done.

    If generalized switch `verbose` represents ``True``, messages for
    installations are printed.

    :raise: a :py:exc:`dusetup.error.SetupError` if byte-compilation has
            failed.
    '''
    cmd = [emacs_cmd]
    cmd.extend(add_emacs_opts)
    if misc.to_bool(force_recompile):
        compile_fct = 'batch-byte-compile'
        compile_mode = 'forced'
    else:
        compile_fct = 'batch-byte-compile-if-not-done'
        compile_mode = 'if needed'
    cmd.extend(['--no-site-file', '--batch', '-f', compile_fct])
    cmd.append(str(el_fpath))

    if misc.to_bool(verbose):
        msg = f"Use `{emacs_cmd}` to byte-compile ({compile_mode})\n"
        msg += f"  {el_fpath}"
        print(msg)
    run.run_cmd(cmd, verbose=False, shell=False)


def mk_elc_files_of_dir(el_dpath, recursive=False, crit=None,
                        emacs_cmd='emacs', add_emacs_opts=[],
                        force_recompile=False, verbose=True):
    '''
    Byte-compile ELisp source files in or below a directory.

    All ELisp source file in (if `recursive` is ``False``) or below (if
    `recursive` is ``True``) directory with path `el_dpath` are byte-compiled
    by Emacs called a `emacs_cmd`.

    If `crit` is a not-``None`` path predicate only those ELisp source files
    are byte-compiled whose path fulfills `crit`.

    The elements of list `add_emacs_opts` are passed as additional options to
    the Emacs command.

    For a generalized switch `force_recompile` that represents ``True`` the
    file is even byte-compiled again if it was already done.

    If generalized switch `verbose` represents ``True``, messages for
    installations are printed.

    :raise: a :py:exc:`dusetup.error.SetupError` if byte-compilation has
            failed.
    '''
    # Extend the ELisp file criteria if needed.
    if crit is not None:
        def is_candidate(f):
            return (crit(f) and is_elisp_file(f, with_elc=False))
    else:
        def is_candidate(f):
            return is_elisp_file(f, with_elc=False)

    # Since often the directory `el_dpath` contains more than one Elisp file
    # and some of these files may include others from `el_dpath` we add
    # `el_dpath` as additional load directory by default.
    add_emacs_opts.extend(['-L', str(el_dpath)])

    depth = 'max' if misc.to_bool(recursive) else 'min'
    for f in path.ls_dir(
            el_dpath, depth, is_candidate, abspath=True):
        mk_elc_file(f, emacs_cmd, add_emacs_opts, force_recompile, verbose)


# ----------------------------------------------------------------------------
# Local Variables:
# ispell-local-dictionary: "en"
# End:
