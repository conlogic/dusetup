# ============================================================================
# Title          : Setup utilities
#
# Classification : Python module
#
# Author         : Dirk Ullrich
#
# Date           : 2024-04-21
#
# Description    : See documentation string below.
# ============================================================================

'''
Main package for distribution package ``dusetup``.

Beside this, the setup step functions will imported into its namespace to make
them available with shorter fully-qualified names to configure setup stages.
'''


from .step.path import (
    add_path_envvar, ensure_clean_dir, rm_item, cpmv_item,
    symlink_item, symlink_items_of_dir, novers_symlink_items_of_dir
)
from .step.run import (
    run_cmd_from_cfg, run_cmds_from_cfg
)
from .step.build import (
    build_sect_items_from_cfg, build_key_items_from_cfg
)
from .step.install import (
    install_sect_items_from_cfg, install_key_items_from_cfg
)


# Parameters
# ==========

# Version of this package.
version = '0.20.dev15'


# Provided Functions
# ==================

__all__ = [
    # Setup step functions from :py:mod:`dusetup.step.path`.
    "add_path_envvar", "ensure_clean_dir", "rm_item", "cpmv_item",
    "symlink_item", "symlink_items_of_dir", "novers_symlink_items_of_dir",
    # Setup step functions from :py:mod:`dusetup.step.run`.
    "run_cmd_from_cfg", "run_cmds_from_cfg",
    # Setup step functions from :py:mod:`dusetup.step.build`.
    "build_sect_items_from_cfg", "build_key_items_from_cfg",
    # Setup step functions from :py:mod:`dusetup.step.install`.
    "install_sect_items_from_cfg", "install_key_items_from_cfg"
]


# ----------------------------------------------------------------------------
# Local Variables:
# ispell-local-dictionary: "en"
# End:
