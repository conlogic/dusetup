# ============================================================================
# Title          : Setup utilities
#
# Classification : Python module
#
# Author         : Dirk Ullrich
#
# Date           : 2024-02-27
#
# Description    : See documentation string below.
# ============================================================================

'''
Utilities for configuration files and objects.
'''


import pathlib
import shutil

import duconfig  # type: ignore

from .. import error
from . import interpol


# Functions
# =========


def config_file_name(app_name, app_vers, cfg_fname_tpl):
    '''
    Name of the setup configuration file for an application.

    :return: name of the setup configuration file for application with name
             `app_name` and version `app_vers`, where string `cfg_fname_tpl`
             is a template for the name of the setup configuration file, with
             a placeholder for combined application name and (possible)
             version.
    '''
    app_name_vers = interpol.add_app_version(app_name, app_vers)
    cfg_fname = cfg_fname_tpl.format(app_name_vers=app_name_vers)

    return cfg_fname


def search_config_file(app_name, app_vers, cfg_fname_tpl, cfg_dpaths,
                       vers_fallback=True):
    '''
    Search for a setup configuration file for an application.

    For application with name `app_name` and version `app_vers` the
    configuration file is searched in the directories with paths in list
    `cfg_dpaths`.

    String `cfg_fname_tpl` is a template for the name of the configuration
    file, with a placeholder for combined application name and (possible)
    version.

    If switch `vers_fallback` is ``True`` it is also searched for a setup
    configuration file without application version if no one with application
    version exists.

    :return: info dictionary about setup configuration file paths:

             + key ``'searched_fnames'`` with the list of filenames searched
               for an existing setup configuration file,

             + key ``'found_fpath'`` with the path of an existing setup
               configuration file, if one was found, and ``None`` otherwise.
    '''
    # Search with application version first.
    cfg_fname = config_file_name(app_name, app_vers, cfg_fname_tpl)
    searched_cfg_fnames = [cfg_fname]

    cfg_fpath = duconfig.search_config_file(cfg_fname, cfg_dpaths)

    # If fallback is wanted, search for version-less setup configuration file,
    # too, if no setup configuration file with version was found.
    if cfg_fpath is None and vers_fallback:
        cfg_fname = config_file_name(app_name, None, cfg_fname_tpl)
        searched_cfg_fnames.append(cfg_fname)
        cfg_fpath = duconfig.search_config_file(cfg_fname, cfg_dpaths)

    cfg_fpath_info = {
        'searched_fnames': searched_cfg_fnames,
        'found_fpath': cfg_fpath
    }

    return cfg_fpath_info


def read_config_file(cfg_fpath, cfg_fcts=interpol.DEFAULT_CFG_FCTS):
    '''
    Read setup configuration file given by path `cfg_fpath`.

    List `cfg_fcts` contains the functions available for value interpolation
    in the setup configuration file.

    :return: the configuration read.

    :raise: a :py:exc:`dusetup.error.SetupError` if a suitable setup
            configuration file does not exist.
    :raise: a :py:exc:`duconfig.ConfigError` if something is wrong with the
            setup configuration read from the setup configuration file.
    '''
    cfg_fpath = pathlib.Path(cfg_fpath)

    if cfg_fpath.is_file():
        # A suitable setup configuration file exists -> read it.
        cfg = duconfig.Config(fcts=cfg_fcts, raw=False)
        cfg.read_file(cfg_fpath)
    else:
        # No suitable setup configuration exists -> error.
        raise error.SetupError(
            f"Missing configuration file at\n  {cfg_fpath}")

    return cfg


def export_config_file(cfg_fpath, out_fpath):
    '''
    Export setup configuration file with path `cfg_fpath`.

    The setup configuration file is exported as file with path `out_fpath`.

    :raise: a :py:exc:`dusetup.error.SetupError` if the both file paths are
            the same.
    '''
    cfg_fpath = pathlib.Path(cfg_fpath)
    out_fpath = pathlib.Path(out_fpath)
    if out_fpath.is_file() and cfg_fpath.samefile(out_fpath):
        msg = 'Paths for active and exported setup configuration files'
        msg += ' must be different.'
        raise error.SetupError(msg)
    else:
        print(f"Active setup configuration file exported to\n  {out_fpath}")
        shutil.copy(cfg_fpath, out_fpath)


def upd_config_sections(old_cfg, new_cfg):
    '''
    Update the sections in setup configuration `old_cfg`.

    These sections are updated with sections in setup configuration `new_cfg`.

    :return: updated setup configuration.
    '''
    # To work around a ``duipoldict`` / ``duconfig`` bug, we work with a
    # dictionary for `old_cfg`.
    old_data = old_cfg.write_dict()
    new_data = new_cfg.write_dict()
    # Update sections.
    data = old_data.copy()
    for s in new_data.keys():
        new_sect_s = new_data[s]
        if s in data.keys():
            data[s].update(new_sect_s)
        else:
            data[s] = new_sect_s
    # Create the updated setup configuration.
    # - We also merge the interpolation functions to be used.
    old_fcts = set(old_cfg.fcts)
    new_fcts = set(new_cfg.fcts)
    fcts = list(old_fcts.union(new_fcts))
    # - Create the setup configuration object.
    cfg = duconfig.Config(fcts, raw=old_cfg.raw)
    cfg.read_dict(data)

    return cfg


# ----------------------------------------------------------------------------
# Local Variables:
# ispell-local-dictionary: "en"
# End:
