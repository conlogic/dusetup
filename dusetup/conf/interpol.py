# ============================================================================
# Title          : Setup utilities
#
# Classification : Python module
#
# Author         : Dirk Ullrich
#
# Date           : 2024-02-28
#
# Description    : See documentation string below.
# ============================================================================


'''
Functions usable for value interpolation in configuration files.

Due to current type limitation for value interpolation based on the
``duipoldict`` package, such functions either return a string, or a dictionary
or list with string values.
'''


import pathlib
import os

from . import APP_SEP


# Functions
# =========


def calc_app_name(dpath=None, lowercase_only=True, sep=APP_SEP):
    '''
    Calculate the name for the application in a directory.

    This directory is given by path `dpath`.  If `dpath` is ``None``, the path
    of the current working directory is used.

    For a ``True`` switch `lowercase_only` the calculated application name
    uses lowercase letters only.

    String `sep` is the separator between the application's name and its
    version in the name of `dpath`.  If the name of `dpath` does not contain a
    separator, the name of the parent directory is used as application name.

    :return: calculated application name.
    '''
    if dpath is None:
        dpath = pathlib.Path.cwd()
    else:
        dpath = pathlib.Path(dpath)
    dname = dpath.name

    sep_idx = dname.rfind(sep)
    if sep_idx < 0:
        # Name version separator was not found -> take name of the parent's
        # parent as application name.
        app_dpath = dpath.parent.parent
        app_name_str = app_dpath.name
    else:
        # Name version separator was found -> application name is the part
        # before the separator.
        app_name_str = dname[:sep_idx]

    if lowercase_only:
        app_name = app_name_str.lower()
    else:
        app_name = app_name_str

    return app_name


def calc_app_version(nr_parts=None, dpath=None, part_sep='.', sep=APP_SEP):
    '''
    Calculate the version for the application in a directory.

    This directory is given by path `dpath`.  If `dpath` is ``None``, the path
    of the current working directory is used.

    For a not-``None`` value `nr_parts` that can be converted into a positive
    integer, maximal `nr_parts` many parts of the calculated version are used,
    where the parts of a version are separated by string `part_sep`.

    If `dpath` is ``None``, the path of the current working directory is used.

    String `sep` is the separator between the application's name and its
    version in the name of `dpath`.  If the name of `dpath` does not contain a
    separator, the whole name is used as application version.

    :return: calculated application version.
    '''
    if dpath is None:
        dpath = pathlib.Path.cwd()
    else:
        dpath = pathlib.Path(dpath)
    dname = dpath.name

    sep_idx = dname.rfind(sep)
    if sep_idx < 0:
        # Name version separator was not found -> take the whole directory
        # name as application version.
        app_vers_str = dname
    else:
        # Name version separator was found -> application version is rightmost
        # part after the separator.
        app_vers_str = dname[sep_idx + 1:]

    if nr_parts is not None:
        app_vers_parts = app_vers_str.split(part_sep)
        app_vers_used_parts = app_vers_parts[:int(nr_parts)]
        app_vers = part_sep.join(app_vers_used_parts)
    else:
        app_vers = app_vers_str

    return app_vers


def add_app_version(name, vers=None, sep=APP_SEP):
    '''
    Append a version suffix to an application name.

    For application version `vers` a version suffix is appended to application
    name `name`, if `vers` is neither ``None`` nor empty, and use only `name`
    itself otherwise.

    String `sep` is used as separator between the application name and the
    version, i.e. the version suffix to be appended consists of `sep` followed
    by `vers`.

    :return: name `name` with version suffix maybe added.
    '''
    if vers is None or vers == '':
        name_vers = name
    else:
        # We add the version to the basename, i.e. before all file extensions.
        name = pathlib.Path(name)
        base_vers = f"{name.stem}{sep}{vers}"
        name_vers = str(name.with_stem(base_vers))

    return name_vers


def del_app_version(name, vers=None, sep=APP_SEP):
    '''
    Delete a possible version suffix from an application name.

    If application name `name` has a version suffix for version `vers`, and if
    `vers` is neither ``None`` nor empty, this version suffix is removed.
    Otherwise `name` remains unchanged.

    String `sep` is used as separator between the application name and the
    version, i.e. the version suffix to be appended consists of `sep` followed
    by `vers`.

    :return: name `name` with version suffix maybe deleted.
    '''
    if vers is None or vers == '':
        name_novers = name
    else:
        name = pathlib.Path(name)
        name_noext = name.stem
        vers_suffix = f"{sep}{vers}"
        if name_noext.endswith(vers_suffix):
            name_novers_noext = name_noext[:-len(vers_suffix)]
            name_novers = str(name.with_stem(name_novers_noext))
        else:
            name_novers = name

    return name_novers


def abspath(path):
    '''
    Calculate an absolute path.

    :return: absolute variant of path `path` as string.
    '''
    path = pathlib.Path(path)
    abs_path = path.absolute()

    return str(abs_path)


def count_usable_cpus():
    '''
    Count number of available CPUs or CPU cores.

    :return: number of CPUs or CPU cores the current process can use in
             parallel, represented as string.
    '''
    # Set of CPUs the current process is restricted to.
    cpu_set = os.sched_getaffinity(0)
    # Number of CPUs the current process is restricted to.
    nr_cpus = len(cpu_set)

    return str(nr_cpus)


#: The functions provided for value interpolation.
DEFAULT_CFG_FCTS = [
    calc_app_name, calc_app_version, add_app_version, del_app_version,
    abspath, count_usable_cpus
]


# ----------------------------------------------------------------------------
# Local Variables:
# ispell-local-dictionary: "en"
# End:
