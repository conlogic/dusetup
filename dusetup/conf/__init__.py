# ============================================================================
# Title          : Setup utilities
#
# Classification : Python module
#
# Author         : Dirk Ullrich
#
# Date           : 2022-04-28
#
# Description    : See documentation string below.
# ============================================================================

'''
Handling of configurations and configuration files.
'''


# Parameters
# ==========

#: Separator between a name and application version.
APP_SEP = '-'


# ----------------------------------------------------------------------------
# Local Variables:
# ispell-local-dictionary: "en"
# End:
