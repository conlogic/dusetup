# ============================================================================
# Title          : Setup utilities
#
# Classification : Python module
#
# Author         : Dirk Ullrich
#
# Date           : 2022-05-02
#
# Description    : See documentation string below.
# ============================================================================

'''
Main module to call package :py:mod:`dusetup`.
'''


from . import cmdline


# Doing
# =====

if __name__ == '__main__':
    cmdline.main()


# ----------------------------------------------------------------------------
# Local Variables:
# ispell-local-dictionary: "en"
# End:
