..
 =============================================================================
 Title          : Setup utilities

 Classification : reST text file

 Author         : Dirk Ullrich

 Date           : 2024-04-21

 Description    : Change log for the distribution package.
 =============================================================================


=========
Changelog
=========


Version 0.20.dev15 (2024-04-21)
===============================

- Make style checker and type checker happy.

- Make remaining doc strings :pep:`257`-conform.

- Fix other errors and typos in some docstrings.

- Fix for :py:func:`dusetup.conf.interpol.add_app_version`: it always returns
  a string.

- New function :py:func:`dusetup.conf.interpol.del_app_version` to remove a
  version suffix from an appending name.

- New function :py:func:`novers_symlink_items_of_dir` to create no-version
  symlinks for the items of a directory.


Version 0.20.dev14 (2023-08-26)
===============================

- Make the doc strings more :pep:`257`-conform.

- Fix a typo in the :file:``CHANGELOG.rst`` file.

- Complete the doc string of function
  :py:func:`dusetup.conf.interpol.add_app_version`.

- Sync the tutorial about the *Idris 2* setup with the updated real-world
  *Idris 2* setup.  Moreover all found  glitches in this tutorial are fixed.

- Tighten some formulations in the tutorial based on the *Git* setup.


Version 0.20.dev13 (2023-01-22)
===============================

- Fix a bug in :py:func:`dusetup.util.elisp.mk_elc_files_of_dir` with the
  path parameter of the ``-L`` option.

- Fix year in change log dates.


Version 0.20.dev12 (2022-10-31)
===============================

- Fix a bug in function :py:func:`dusetup.util.install.calc_kind_info` for
  passing additional keyword arguments to the copy function.


Version 0.20.dev11 (2022-10-10)
===============================

- Use terminology for build / installation item kinds more consistently.

- Refactor the item build mechanism: use a new module
  :py:mod:`dusetup.util.build` to modularize a helper function for setup steps
  to build items.  This resembles similar helpers for installing items in
  module :py:mod:`dusetup.util.install`.  For now :py:mod:`dusetup.util.build`
  provides the following functions:

  - :py:func:`dusetup.util.build.calc_kind_info` to calculate the kind build
    information,

  - :py:func:`dusetup.util.build.build_kind_item_elements` to build item
    elements of a kind.

- Refactor the item build mechanism: do not cope with the differences between
  the copy functions for a single item and a multiple items by creating
  wrapper functions with the same API on the fly, but deal with these
  differences when calling the respective copy function.  This makes the
  implementation more concrete an error messages from calling the respective
  copy function more useful.  The refactoring moves the respective logic from
  a helper function of function :py:func:`dusetup.util.install.calc_kind_info`
  into function :py:func:`dusetup.util.install.install_kind_item_elements`.

- Allow more install item parameters, namely:

  - generalized switch ``add_vers`` to control whether the application version
    is added;

  - Arguments ``dmode`` / ``fmode`` to set the mode for directories / files.
    Their values must represent suitable integers (interpreted by function
    :py:func:`dusetup.util.misc.to_int`), and will be passed to the copy
    function used.

- Generalize build and install item parameters by allowing setup parameter
  placeholders in all these item parameters.

- New helper function :py:func:`dusetup.util.misc.to_int` to interpret strings
  as integer literals.

- Show details if a setup step function could not be called.

- Fix the build steps in module :py:mod:`dusetup.step.build` module to
  recognize the ``only_kind_types`` argument.

- Fix typos found.


Version 0.20.dev10 (2022-08-24)
===============================

- New helper function :py:func:`dusetup.util.misc.format_dict_str_vals` to
  replace placeholders in string values of dictionaries.

- Function :py:func:`dusetup.util.misc.tokenize_item_element` now also fills
  string values in item element parameters by setup parameters.

- Optional keyword arguments in the build / install information for build /
  install item kinds are generalized is a similar way: string values for such
  keyword arguments are filled by setup parameters, too.

- New commandline option ``-e`` / ``--emacs-command`` for the Emacs command to
  be used for the byte-compilation of ELisp files.  It can also be set as
  configuration parameter named ``emacs_cmd`` in the ``default`` section of
  setup configuration files, and is stored as initial setup parameter with
  same name.

- Helper functions :py:func:`dusetup.util.elisp.mk_elc_file` and
  :py:func:`dusetup.util.elisp.mk_elc_files_of_dir` get a new keyword
  argument to choose the Emacs command to be used for ELisp byte-compilation.
  This Emacs command is also shown in their message for verbosity.

- Use the Emacs command set by commandline option or configured in the setup
  configuration file to byte-compile ELisp files.

- Install byte-compiled ELisp files, too.

- Fix a test in function :py:func:`dusetup.cmdline.calc_setup_params`.

- Fix typos found.


Version 0.20.dev9 (2022-08-21)
==============================

- Rework the mechanism to search for setup configuration files:

  - Move the configuration for the setup configuration filename template from
    module :py:mod:`duetup.conf.config` into :py:mod:`dusetup.cmdline`.  This
    also adds a keyword argument for this template to function
    :py:func:`dusetup.cmdline.calc_init_params` and makes this very argument
    mandatory for the :py:func:`dusetup.conf.config.config_file_name` and
    :py:func:`dusetup.conf.config.search_config_file` functions.

  - Add a parameter to module :py:mod:`dusetup.cmdline` for the list of base
    directory paths to be searched for setup configuration files by default in
    addition to the current working directory.  To use these additional
    directories function :py:func:`dusetup.conf.config.search_config_file`
    gets an additional argument.  It calculates the real directory list, and
    passes it in turn to the :py:func:`dusetup.cmdline.calc_init_params`
    function by a new argument instead of hard-wiring the directory list into
    the former function.

  - Addition of directory paths as Python import paths is now done either in
    the :py:func:`dusetup.cmdline.read_config_file` or in the
    :py:func:`dusetup.cmdline.calc_setup_params` function.

  - Additional directories to be searched for setup configuration files by
    default results from the respective base directory by adding a
    subdirectory with the setup tool's name.

  - Use :file:`/etc` directory as base directory to be searched for setup
    configuration files by default, too.

  - Add a message with the path of the active setup configuration file, and
    improve the message if no setup configuration file can be found.  For this
    function :py:func:`dusetup.conf.config.search_config_file` now also return
    information for searched setup configuration files.

  - Now a :py:exc:`dusetup.error.SetupError` is raised if one try to export
    the active setup configuration file to a file with the same path.

- The dictionary with the parameters used to initialize the setup tool now
  stores additional parameters:

  - key ``cfg_dpaths`` with the list of paths for the directories to be
    searched for setup configuration files by default as value,

  - key ``searched_cfg_fnames`` with the names of the setup configuration
    files searched for by default.

  These new parameters are utilized, for instance, for the rework described
  above.

- The function to read the active setup configuration file like the
  :py:func:`dusetup.cmdline.read_config_file` one, gets the dictionary used
  for the setup tool initialization as argument, too.  This is utilized, for
  instance, for the rework described above.

- Use a consistent terminology - especially for the stuff related to setup
  configurations and their files.

- Update the Sphinx documentation and the examples for all these changes.

- Remove some redundant information from the change log.

- Fix typos found.


Version 0.20.dev8 (2022-08-14)
==============================

- Python helper function :py:func:`dusetup.util.python.python_site_dir` now
  returns the Python site directory path relative to the prefix directory
  path.  Thus it has no argument anymore, and is renamed to
  :py:func:`dusetup.util.python.rel_python_site_dir`.

- Factor from function :py:func:`dusetup.cmdline.calc_setup_params` the
  calculation of standard installation directory paths out into own function
  :py:func:`dusetup.util.install.calc_std_install_dir_paths`.  Adding these
  directory paths to the setup parameters is now done by an own function
  :py:func:`dusetup.cmdline.add_std_install_dir_paths`.  Accordingly, function
  :py:func:`dusetup.cmdline.main` gets a new parameter for this function.

- Split usage documentation file into multiple ones.


Version 0.20.dev7 (2022-08-13)
==============================

- New path helper function :py:func:`dusetup.util.path.rm_items_of_dir` to
  remove items in / below a directory.

- New path predicates :py:func:`dusetup.util.elisp.is_elc_file` for ELisp
  source files and :py:func:`dusetup.util.elisp.is_elc_file` for byte-compiled
  ELisp files.

- The installation of ELisp files is narrowed: only ELisp source files are
  installed by default.


Version 0.20.dev6 (2022-08-10)
==============================

- Print more diagnostic messages during build / installation.

- Change logic for default output directory path when building Texinfo files
  by functions :py:func:`dusetup.util.doc.texi2any_file` and
  :py:func:`dusetup.util.doc.texi2any_files_of_dir`.

- Create Info directory file after installing Info file(s).


Version 0.20.dev5 (2022-08-09)
==============================

- Start new module :py:mod:`dusetup.util.python` with Python helpers.  At the
  moment it provides for users:

  - :py:func:`dusetup.util.python.python_site_dir` to calculate the Python
    package site directory path,

  - :py:func:`dusetup.util.python.compile_python_item` to byte-compile a
    Python module or package.

- Function :py:func:`dusetup.cmdline.calc_setup_params` set more installation
  destination path directories:

  - ``python_dpath`` for Python site items directory path.

- Add more install item kind types:

  - ``python_dpath`` for Python site items.

- Correct typos found.


Version 0.20.dev4 (2022-08-09)
==============================

- New path helper function :py:func:`dusetup.util.path.add_prefix` to prefix a
  path with another prefix directory path.

- Let function :py:func:`dusetup.cmdline.calc_setup_params` set more
  installation destination path directories:

  - ``elisp_dpath`` for the Emacs Lisp directory path,

  - ``vim_dpath`` for the Vim files directory path,

  - ``bash_dpath`` for the Bash completion files directory path,

  - ``zsh_dpath`` for the Zsh completion files directory path.

- Add more install item kind types:

  - ``vim`` for the Vim files,

  - ``bash`` for the Bash completion files,

  - ``zsh`` for the Zsh completion files.

- Allow optional parameter ``rel_dst_path`` for install items of single kinds.

- Fix :py:func:`dusetup.cmdline.main` to let it really use function from
  argument `calc_setup_params_fct`.

- Better error message if configuration file is missing.


Version 0.20.dev3 (2022-08-04)
==============================

- Depends on ``duconfig`` with a version >= 5.0.

- The application version can now be overwritten in the configuration file.

- Add a mechanism to enable ``dusetup`` to import Python modules / packages
  from directories outside of ``dusetup``, and document its usage:

  - New helper function :py:func:`dusetup.helper.misc.add_import_dpaths` to
    add a list of directory paths as Python import paths.

  - Add the paths of the standard directories searched for setup configuration
    files, and the directory path of the used setup configuration file as
    Python import paths.

  - Add directory paths configured using parameter ``import_dpaths`` in the
    default section of the used setup configuration file as Python import
    paths.

  - Add directory paths given by new option ``-a`` / ``--add-import-path`` as
    Python import paths.

- Reorganize imports of ``dusetup``'s packages and modules:

  - Rename package :py:mod:`dusetup.cfg` to :py:mod:`dusetup.conf`.

  - Rename package :py:mod:`dusetup.helper` to :py:mod:`dusetup.util`.

  - Import modules from the packages :py:mod:`dusetup.conf` and
    :py:mod:`dusetup.util` using unique names.

- Enhance the standard initial setup parameters calculated by function
  :py:func:`dusetup.cmdline.calc_setup_params`:

  - ``bin_dpath`` for the binary directory path,

  - ``lib_dpath`` for the library directory path,

  - ``share_dpath`` for the shared directory path,

  - ``doc_dpath`` for the documentation directory path,

  - ``info_dpath`` for the info directory path,

  - ``man_dpath`` for the  manpage directory path.

  All but the last one are adapted from the respective ``'dst_dpath_tpl'``
  values in :py:data:`dusetup.step.install.INSTALL_KIND_TYPE_INFO`, and the
  latter are now based on (placeholders of) the new setup parameters.

- New or changed setup stage / step functions:

  - New setup step function :py:func:`dusetup.step.path.add_path_envvar` to
    add a path to the search path value of an environment variable.

  - New setup step function :py:func:`dusetup.step.path.rm_item` to remove a
    file or directory.

  - New setup step function :py:func:`dusetup.step.path.cpmv_item` to copy or
    move a file or directory.

  - New setup step function :py:func:`dusetup.step.path.symlink_item` to
    create a symbolic link for a file or directory.

  - New setup step function :py:func:`dusetup.step.path.symlink_items_of_dir`
    to create a symbolic links for files or directories in a directory.

  - Fix a default value bug in setup step function
    :py:func:`dusetup.step.path.run_cmds_from_cfg`.

  - Fix a return value bug in function :py:func:`dusetup.stage.perform_stage`.

- New or changed helper functions:

  - Now function :py:func:`dusetup.util.path.add_path_envvar` allows a
    generalized switch argument for `keep_empty` and has an argument to
    control verbosity now.

  - Functions :py:func:`dusetup.util.path.symlink_item`,
    :py:func:`dusetup.util.path.symlink_items` and
    :py:func:`dusetup.util.path.symlink_items_of_dir` (this is former
    function :py:func:`dusetup.path.symlink_dir_items`) now can use
    generalized switch arguments throughout.

- New function :py:func:`dusetup.conf.interpol.abspath` for function
  interpolation in configurations to calculate the absolute variant of a path.

- Fix configuration file export.

- Use a *Idris 2* setup as example again, together with a tutorial to
  create this example.

- Clarify documentation of various functions.

- Fix some documentation glitches.


Version 0.20.dev2 (2022-05-30)
==============================

- Simplify the mechanism to handle the commandline:

  - Function :py:func:`dusetup.cmdline.calc_init_params` only needs an
    argument for a tool description template.

  - Function :py:func:`dusetup.cmdline.main` can get the tool's version and
    description via arguments.

  - Reading the configuration file is split away from function
    :py:func:`dusetup.cmdline.calc_setup_params` into a new function
    :py:func:`dusetup.cmdline.read_config_file` that is separately called by
    :py:func:`dusetup.cmdline.main`.

- Add a generic mechanism to read item elements of a certain kind from the
  configuration.  It is implemented by the functions
  :py:func:`dusetup.helper.misc.tokenize_item_element`,
  :py:func:`dusetup.helper.misc.tokenize_item_kind` and
  :py:func:`dusetup.helper.misc.kind_item_elements_from_cfg_key`.

- Add a mechanism to build items.  It uses the generic mechanism for item
  elements of certain kind mentioned above.  For now the following setup step
  functions are implemented for it:

  - Function :py:func:`dusetup.step.build.build_sect_items_from_cfg` to build
    items of all kinds given in a configuration section.

  - Function :py:func:`dusetup.step.build.build_key_items_from_cfg` to build
    items of one given kind.

- The mechanism to install items given in a dedicated configuration section is
  completely reworked and added again:

  - The install information for the known kinds of items is enhanced and uses
    path templates for the destination directory path.  It uses the generic
    mechanism for item elements of certain kind mentioned above.

  - A new function :py:func:`dusetup.helper.install.calc_kind_info` calculates
    from this enhanced installation information for kinds the installation
    helper function and destination information needed by the used
    installation helper functions for concrete kinds.

  - New function :py:func:`dusetup.helper.install.install_kind_item_elements`
    to call the used installation helper functions for concrete kinds.

  - New setup step function
    :py:func:`dusetup.step.install.install_sect_items_from_cfg` to install
    items of all kinds given in a configuration section.  It replaces a former
    setup step function with a similar purpose.

  - New setup step function
    :py:func:`dusetup.step.install.install_key_items_from_cfg` to install
    items of one given kind.

  - Redo more path-related generic helper functions in module
    :py:mod:`dusetup.helper.path` that are needed to implement this install
    mechanism.

- Further new or changed path-related helper functions:

  - New path-related generic helper function
    :py:mod:`dusetup.helper.path.ensure_dir` to ensure a directory.

  - Generalize function :py:func:`dusetup.helper.path.ls_dir` to a 3rd case
    that only entries with maximal depth are listed.

  - Simplify function :py:mod:`dusetup.helper.path.cpmv_dir` a lot by using
    the generalized function :py:func:`dusetup.helper.path.ls_dir`.

  - Generalize function :py:func:`dusetup.helper.path.cpmv_item` to allow a
    function to calculate the destination path for every item.

  - Function :py:func:`dusetup.helper.path.cpmv_items_of_dir` is former
    function :py:func:`dusetup.helper.path.cpmv_dir_items` generalized to
    allow a function to calculate the relative destination path for every
    item; similar for :py:func:`dusetup.helper.path.cpmv_files_of_dir`.

- Further new or changed some helper functions to run external commands:

  - Both functions :py:func:`dusetup.helper.run.run_cmd` and
    :py:func:`dusetup.helper.run.run_cmds` now can handle commands given as
    list, too.

- Further new or changes file type specific helper functions:

  - Function :py:func:`dusetup.helper.elisp.is_elisp_file` tests that the
    tested path belongs to a file.  This function has now also an additional
    optional argument to control its behavior for byte-compiled files.

  - Functions :py:func:`dusetup.helper.elisp.is_texi_file` and
    :py:func:`dusetup.helper.elisp.is_info_file` test that its path belongs to
    a file, too.

  - All functions :py:func:`dusetup.helper.elisp.mk_elc_file`,
    :py:func:`dusetup.helper.elisp.mk_elc_files_of_dir`,
    :py:func:`dusetup.helper.doc.texi2any_file`,
    :py:func:`dusetup.helper.doc.texi2any_files_of_dir`,
    :py:func:`dusetup.helper.doc.install_info_files_of_dir` got an additional
    argument to control verbosity.

- New idempotent type conversion helper functions:

  - Helper function :py:func:`dusetup.helper.str2bool` is generalized to
    :py:func:`dusetup.helper.misc.to_bool` that accepts Booleans, too.  It is
    used for all former switch arguments in helper functions to get
    generalized switch arguments.

  - New helper function :py:func:`dusetup.helper.misc.to_tuple` to split
    strings into tuples but directly converts other sequences.  It can be used
    for generalized tuple arguments for functions.

- Tweak setup stage messages a bit.

- Migrate the API documentation into an own reST file with a separate index
  entry.

- Various fixes for the documentation from proofreading by Jens Rapp.  Thanks,
  Jens!

- Fix various glitches.


Version 0.20.dev1 (2022-05-06)
==============================


- Summary : this is a complete rework of the whole package to make, first of
  all, the configuration of setup stages more powerful and flexible.

- Depends on ``duconfig`` with a version >= 4.0.

- New option ``-n`` / ``--no-perform`` to only show setup parameters instead
  of performing the setup stages.

- Improved mechanism for setup configuration files:

  - Search automatically for a suitable setup configuration file for the
    application to be set up.  For this the default component functions
    called by :py:func:`dusetup.cmdline.main` are completely reworked.

  - Let :py:mod:`dusetup` import setup step functions directly into its
    namespace to make them available with shorter fully-qualified names to
    configure setup stages.

- Improved mechanism for setup stages:

  - Every setup stage consists of a list of setup steps to perform, and there
    are additional optional dummy setup stage for pre- and post-processing.

  - The mechanism to specify a setup step function is now completely general
    and generic: the function name is given fully-qualified, and the
    respective package / module must be importable.

- New or changed setup step functions:

  - Rename all setup step functions (i.e. in package :py:mod:`dusetup.step`)
    that gets its main parameter(s) from values of (a) configuration
    parameter(s) to a ``<fct>_from_cfg`` form.

  - New function :py:func:`dusetup.step.path.ensure_clean_dir` to ensure a
    clean directory directly given by its template.  This is a generalization
    of the former :py:func:`dusetup.step.path.ensure_clean_destination`.

- New or changed configuration-related functions:

  - New function :py:func:`dusetup.cfg.config.config_file_name` to calculate
    the standard configuration file name for an application.

  - Function :py:func:`dusetup.cfg.config.read_config_file` now simply returns
    the configuration read, since the other configuration data are no longer
    necessary.

- New or changed helper functions:

  - New helper function :py:func:`dusetup.helper.misc.get_qualified_attr` to
    get an attribute from a package or module for a fully-qualified attribute
    name.

  - Helper function :py:func:`dusetup.helper.run.run_cmd` and friends can now
    also run commands without a shell.  This inherited by the setup step
    functions to run commands in module :py:mod:`dusetup.step.run`.

  - Generalize helper function :py:func:`dusetup.helper.path.ensure_clean_dir`
    to have a keyword argument for verbosity.

- Fix methods to control debugging for error handler
  :py:class:`dusetup.error.handle_errors`.

- Use a simple *Git* setup as example again, together with a tutorial to
  create this example.


..
 -----------------------------------------------------------------------------
 Local Variables:
 mode: rst
 ispell-local-dictionary: "en"
 End:

..  LocalWords:  docstrings
