..
 =============================================================================
 Title          : Setup utilities

 Classification : reST text file

 Author         : Dirk Ullrich

 Date           : 2022-06-21

 Description    : Reade me for the distribution package.
 =============================================================================


.. _read-me:

============================
``dusetup``: Setup utilities
============================

This package provides generic utilities to set up applications. [#app_term]_
The *setup of an application* means the process to create variants of the
respective application ready to be used for its purpose(s).  Normally the
setup of an application is done in stages.  Typical setup stages to set up,
for instance, an application from source are configuring, building the
configured application and installing the built application.

Package ``dusetup`` can be used in two ways for application setup:

1. Use ``dusetup`` itself as commandline setup tool, solely with an own setup
   configuration file:

   Here package :py:mod:`dusetup` itself is called and an appropriate setup
   configuration file is fed to set up an application.  This way supposes that
   the needed setup stages can fully configured based on the building blocks
   provided by ``dusetup``.

   There is a simple example setup configuration file :file:`git-setup.conf`
   that can be used to try this usage.  This setup configuration file defines
   a  simple setup to build the `Git`_ version control system - including its
   documentation - from source.

2. Use ``dusetup`` itself as commandline setup tool, with an own setup
   configuration file, together with own Python modules or packages that can
   be imported:

   This way uses :py:mod:`dusetup` itself as commandline tool, too.  But
   beside an appropriate setup configuration file own Python modules or
   packages are used that can be imported.  These Python modules or packages
   provide, for instance, additional building blocks for needed setup stages.

   Again, a example is provided, consisting of the setup configuration file
   :file:`idris2-setup.conf` together with Python module file
   :file:`idris2_setup.py`.  They define a setup of the dependently typed
   functional programming language `Idris`_, actually its 2nd version *Idris
   2* [#idris2]_, from source.

3. Use the the ``dusetup`` modules or packages as building blocks to create
   own commandline setup tools for applications.  This way of usage suits for
   very complex or exotic application setups.  To ease the adaption of the
   ``dusetup`` functionality to own purposes, its components are made as
   modular and flexible as possible.


.. [#app_term] The term *application* is used here in a broad sense: beside
               programs that can be run, it should comprise any bundle of code
               serving concrete purposes.  Therefore, for instance, libraries
               are also applications in this wider sense.

.. [#idris2] *Idris 2* is interesting for several reasons.

             Firstly, there is its expressive power that results from being a
             dependently typed functional programming language.  Typical for
             modern statically typed functional programming languages, like
             `Haskell`_, is their powerful type system that allows to express
             many expected properties of the code and to check them already at
             compile time.  The availability of dependent types takes this a
             big step further to allow to encode even whole formal
             specifications solely by appropriate types.

             Secondly, another interesting features of *Idris 2* is that it is
             self-hosting, i.e. completely implemented in itself.  For details
             you can consult the `Idris 2 GitHub`_ repository and the
             corresponding `Idris 2 Wiki`_.

.. _Git: https://git-scm.org

.. _Idris: https://www.idris-lang.org

.. _Haskell: https://www.haskell.org

.. _Idris 2 Github: https://github.com/idris-lang/Idris2

.. _Idris 2 Wiki: https://github.com/idris-lang/Idris2/wiki


..
 -----------------------------------------------------------------------------
 Local Variables:
 mode: rst
 ispell-local-dictionary: "en"
 End:
