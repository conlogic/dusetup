# ============================================================================
# Title          : Setup utilities
#
# Classification : Python module
#
# Author         : Dirk Ullrich
#
# Date           : 2022-08-01
#
# Description    : See documentation string below.
# ============================================================================

'''
Example package for distribution package ``dusetup``.
'''


# ----------------------------------------------------------------------------
# Local Variables:
# ispell-local-dictionary: "en"
# End:
