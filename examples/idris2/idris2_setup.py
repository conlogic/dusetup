'''
Supplements for Idris 2 setup.
'''

import pathlib
import subprocess
from dusetup.util import run

def add_idris2_raw_lib_dpath(params):
    '''
    Special setup step function for Idris 2.

    This setup step function to ensure that the raw library directory path of
    installed Idris 2 is added to dictionary `params` with setup parameters.
    It uses key ``'raw_lib_dpath'``.

    :return: the dictionary with all setup parameters.

    :raise: a :py:exc:`duconfig.ConfigError` if there is something wrong with
            the setup configuration.

    :raise: a :py:exc:`dusetup.error.SetupError` if running of the command to
            determine the raw library directory path fo installed Idris 2 has
            failed.
    '''
    # Do nothing if the respective setup parameter is already stored.
    if params.get('raw_lib_dpath') is None:
        # Get the setup configuration from the setup parameter dictionary.
        cfg = params['cfg']
        # Get the template for the command to determine the raw library
        # directory path of installed Idris 2.
        raw_libdir_cmd_tpl = cfg['install-lib.raw_libdir_cmd_tpl']
        # Fill this command template to get the respective command.
        raw_libdir_cmd = raw_libdir_cmd_tpl.format_map(params)
        # Run the command to determine the raw library directory path of
        # installed Idris 2, and catch its output.
        result = run.run_cmd(
            raw_libdir_cmd, verbose=False, stdout=subprocess.PIPE, text=True)
        # Remove a newline from the output's end.
        result_stdout = result.stdout.rstrip()
        # Store the raw library directory path of installed Idris 2 as setup
        # parameter.
        params['raw_lib_dpath'] = pathlib.Path(result_stdout).resolve()

    return params
